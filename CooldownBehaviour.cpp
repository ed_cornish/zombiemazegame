#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"

State * cooldownState(TypeData & data, float elapsed_s, float cooldown_s)
{
    if (CheckTimerExpire(data, elapsed_s, cooldown_s))
    {
        /* Return to scan state when the node comes out of cooldown */
        return new State(nullptr, bind(scanFrameB, _1, _2, GetScanDegsPerSec(data.id), GetPreferredTarget(data)));
    }
    return nullptr;
}
