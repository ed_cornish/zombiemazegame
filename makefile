
IDIR =./include
CC=gcc
CFLAGS=-I$(IDIR)

LDIR =./lib

LIBS=-lm

_DEPS = $(wildcard, *.h) $(wildcard, *.hpp)

DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

OBJ = Behaviours.o BulletBehaviour.o CloseAttackBehaviour.o CollisionShapes.o CooldownBehaviour.o DetectedBehaviour.o Effect.o EntityData.o GridNode.o HuntBehaviour.o IdleBehaviour.o main.o MapLoader.o Maze.o micropather.o MovementData.o Node.o PlayerBehaviour.o PortBehaviour.o RangedAttackBehaviour.o Ray.o ScanBehaviour.o Shadow.o SpawnBehaviour.o StateMachine.o Steering.o Type.o

DungeonGame: $(OBJ)
	$(CC) $(wildcard, *.cpp) $(CFLAGS) $(LIBS)

%.o: %.c $(DEPS)
	$(CC) -c $(input) $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o *~ core $(IDIR)/*~ 
