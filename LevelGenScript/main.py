import getopt
import sys
from time import ctime
from random import randint

WALL_TILE = '#'
FLOOR_TILE = '.'


class LevelGen:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.grid = []
        self.wall_count = 0
        self.floor_count = 0

        #Create a grid of wall tiles
        for x in xrange(width):
            self.grid.append([])
            for y in xrange(height):
                self.grid[x].append(FLOOR_TILE)

    def PutTile(self, x, y, tile):
        self.grid[x][y] = tile

    def PutWall(self, x, y):
        self.PutTile(x, y, WALL_TILE)

    def PutFloor(self, x, y):
        self.PutTile(x, y, FLOOR_TILE )

    def PutBlock(self, left, top, width, height, tile):
        for x in xrange(left, left + width):
            for y in xrange(top, top + height):
                self.PutTile(x, y, tile)
                #print("{}, {}".format(x, y))

    def Border(self):
        self.PutBlock(0, 0, self.width-1, 1, WALL_TILE)
        self.PutBlock(0, 0, 1, self.height-1, WALL_TILE)
        self.PutBlock(0, self.height-1, self.width, 1, WALL_TILE)
        self.PutBlock(self.width-1, 0, 1, self.height, WALL_TILE)
        print("{} wide, {} high".format(self.width, self.height))

    def RandomBlock(self, tile):
        top = randint(1, self.height-1)
        left = randint(1, self.width-1)
        width = randint(1, self.width / 10)
        height = randint(1, self.height / 10)
        if ((left + width) < self.width and (top + height) < self.height):
            self.PutBlock(left, top, width, height, tile)

    def Tally(self):
        self.floor_count = 0
        self.wall_count = 0
        for x in xrange(self.width):
            for y in xrange(self.height):
                ch = self.grid[x][y]
                if ch == WALL_TILE:
                    self.wall_count += 1
                else:
                    self.floor_count += 1

    def Strew(self, iterations):
        iterations += self.width
        for i in xrange(iterations):
            self.Tally()
            if (self.wall_count - (self.width + self.height) * 2) > self.floor_count:
                self.RandomBlock(FLOOR_TILE)
            else:
                self.RandomBlock(WALL_TILE)

    def DebugPrint(self):
        for y in xrange(self.height):
            ln = '{:<4} - '.format(y)
            for x in xrange(self.width):
                ln = ln + self.grid[x][y]
            print ln
    def DumpToFile(self):
        with open("LevelGenOutput - {}.map".format(ctime()), 'w') as f:
            f.write("\"\n")
            f.write("g{}\n".format(self.width))
            f.write("c60\n")
            for y in xrange(self.height):
                for x in xrange(self.width):
                    f.write(self.grid[x][y])
                f.write("\"\n")


def main(argv):
    width = int(argv[0])



    gen = LevelGen(width, width)

    #file_name = "LevelGenOutput_{}.map".format(ctime())
    #with open(file_name, "w") as f:
    gen.Border()
    gen.Strew(int(argv[1]))
    gen.DebugPrint()
    gen.DumpToFile()

if __name__ == "__main__":
   main(sys.argv[1:])
