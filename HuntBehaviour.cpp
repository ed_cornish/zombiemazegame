#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"



void huntEnter(TypeData & data, float elapsed_s, fec2 last_known)
{

    data.target_pos = last_known;
    GetPathToTarget(data);
    if (MoveDataCheck(data))
    {
        data.move_data->nav.weights = monsterSearchWeights;
    }
}

State * huntTarget(TypeData & data, float elapsed_s, float timeout_s)
{
    /* Ray cast to last known position of target */
    CastLOSRay(data);

    if (CheckDetectedTypeId(data, GetPreferredTarget(data)))
    {
        /* We can see the target, go to detected state */
        data.target_pos = data.detected_ptr->world_pos;
        return new State(nullptr, bind(detectedTarget, _1, _2, data.detected_ptr->instance_id));
    }
    else if (CheckTimerExpire(data, elapsed_s, timeout_s))
    {
        /* Go back to IDLE state */
        return new State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, ENEMY_SCAN_INTERVAL));
    }
    else
    {
        /* Cannot see the target, path to it */
        FollowPath(data);
        //data.target_pos = Type::GetPlayerPos();
    }
    npcMoveUpdate(data, elapsed_s);

    return nullptr;
}
