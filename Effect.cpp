#include "Effect.h"
#include "Type.h"



typedef function<void(TypeData&, int)> effect_spec;
using namespace std::placeholders;
const effect_spec effect_table[MAX_EFFECT_ID] = {
    bind(effRegenHealth, _1, _2),//IMPACT_EFFECT,
    bind(effRegenHealth, _1, _2),//FIRE_EFFECT,
    bind(effRegenHealth, _1, _2),//ICE_EFFECT,
    bind(effRegenHealth, _1, _2),//CONFUSION_EFFECT,
    bind(effRegenHealth, _1, _2),//STASIS_EFFECT,
    bind(effRegenHealth, _1, _2),//STUN_EFFECT,
    bind(effRegenHealth, _1, _2),//SHOCK_EFFECT,
    bind(effRegenHealth, _1, _2),//TELEPORT_EFFECT,
    bind(effRegenHealth, _1, _2),//POLYMORPH_EFFECT,
    bind(effRegenHealth, _1, _2),//HEAL_EFFECT,
};

void effOnFire(TypeData &target, int amount)
{
    target.hp -= amount;
    //cout << target.hp << " left" << endl;
}

void effRegenHealth(TypeData & data, int amount)
{
    data.hp += amount;
    cout << "Healed " << data.type_string.toAnsiString() << " " << amount << " points!" << endl;
}

void effHeal(TypeData & data, int amount)
{
    data.hp += amount;
}

void effResistEffect(Effect & eff, effect_type_id type, int amount)
{
    if (eff.GetType() == type && eff.GetAmount() >= 0)
    {
         eff.ChangeAmount(amount);
        //cout << "Resisted effect id " << type << endl;
    }
}

/* This is set up so that an amount of 2 will double the amount of the target effect */
void effWeaknessEffect(Effect & eff, effect_type_id type, int amount)
{
    if (eff.GetType() == type && eff.GetAmount() >= 0)
    {
        int amount_delta = (eff.GetAmount() * amount) - eff.GetAmount();
        eff.ChangeAmount(amount_delta);
        //cout << "Weak to effect id " << type << endl;
    }
}

Effect GetEffect(effect_type_id id, TypeData & target, int amount, float timeout)
{
    Effect ret_effect(id, amount, timeout);
    ret_effect.RegisterExec(effect_table[id]);
    ret_effect.RegisterTarget(target);
    return ret_effect;
}


Effect::Effect(effect_type_id type, int amount, float time_out) : type(type), amount(amount), time_out_seconds(time_out), timed_out(false)
{
}


void Effect::RegisterTarget(TypeData & target)
{
    target_ptr = &target;
}


void Effect::RegisterExec(function<void(TypeData&, int)> func)
{
    exec_f = func;
}


void Effect::RegisterFilt(function<void(Effect&, effect_type_id, int)> func)
{
    filt_f = func;
}


void Effect::Apply(float elapsed_secs)
{
    if (time_out_seconds >= 0.f)
    {
        time_out_seconds -= elapsed_secs;
        if (time_out_seconds < 0.f)
        {
            timed_out = true;
            GAME_DEBUG_LOG("Effect has timed out !")
        }
    }
    if (target_ptr != nullptr && exec_f != nullptr)
    {
        exec_f(*target_ptr, amount);
        GAME_DEBUG_LOG("Applied effect with id " << type << " to entity.");
    }
}


void Effect::Filter(Effect & eff)
{
    if (filt_f != nullptr)
    {
        filt_f(eff, type, amount);
    }
}


bool Effect::TimedOut()
{
    return timed_out;
}


effect_type_id Effect::GetType()
{
    return type;
}


int Effect::GetAmount()
{
    return amount;
}


void Effect::ChangeAmount(int delta)
{
    amount += delta;
    if (amount <= 0)
    {
        amount = 0;
    }
}


Effect::~Effect()
{
}
