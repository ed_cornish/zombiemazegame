#include "Maze.h"
Maze::Maze()// unsigned int rows, unsigned int cols, unsigned int res)
{
	/*min_span = res;

	if ((rows > MAX_DIM) || (cols > MAX_DIM))
	{
		std::cout << "Failed to init Maze, rows/cols exceeds limit of " << MAX_DIM << std::endl;
	}

	for (unsigned int i = 0; i < rows; i++)
	{
		for (unsigned int j = 0; j < cols; j++)
		{
			map_v.push_back(1);
		}

	}
	maze_rows = rows;
	maze_cols = cols;
	recursion = 0;
	Generate();*/
    path_p = new MicroPather(this);
    walkable_count = 0;
}

Maze::~Maze()
{
    delete path_p;
}

void Maze::Config(unsigned int rows, unsigned int cols, unsigned int res)
{
    min_span = res;

    if ((rows > MAX_DIM) || (cols > MAX_DIM))
    {
        std::cout << "Failed to init Maze, rows/cols exceeds limit of " << MAX_DIM << std::endl;
    }
    map_v.clear();
    for (unsigned int i = 0; i < rows; i++)
    {
        for (unsigned int j = 0; j < cols; j++)
        {
            map_v.push_back(1);
        }

    }
    maze_rows = rows;
    maze_cols = cols;
    recursion = 0;
    Generate();
}

void Maze::WallGen(unsigned int top, unsigned int left, unsigned int bottom, unsigned int right, unsigned int last_door = 0)
{
	unsigned int wall_pos;
	unsigned int door_pos;
	recursion++;
	//Reseed rand
	srand((unsigned int)time(NULL));

	//Debug prints
	//for (auto r = 0; r < maze_rows; r++)
	//{
	//	std::cout << "|";
	//	for (auto c = 0; c < maze_cols; c++)
	//	{
	//		if (MAP_ACCESS(c,r))
	//		{
	//			std::cout << "#";
	//		}
	//		else
	//		{
	//			std::cout << " ";
	//		}
	//	}
	//	std::cout << "|" << std::endl;
	//}
	//std::cout << "---------------------------------------------------------" << std::endl;

	if ((bottom == top) || (right == left))
	{
		return;
	}

	if (((bottom - top) <= min_span) || ((right - left) <= min_span))
	{
		return;
	}

	if ((right < left) || (bottom < top))
	{
		return;
	}

	if ((bottom - top) > (right - left))
	{
		do
		{
			wall_pos = (rand() % (bottom - top)) + (top);
		} while ((wall_pos == last_door) || (wall_pos == top) || (wall_pos == bottom));

		door_pos = (rand() % (right - left)) + (left);

		//std::cout << "HWALL: Door Pos: " << door_pos << " Wall Pos: " << wall_pos << std::endl;
		for (auto i = left; i < right; i++)
		{
			if (i != door_pos)
			{
                MAP_ACCESS(i, wall_pos) = 0;
			}
		}

		if ((wall_pos == bottom) || (wall_pos == top))
		{
			return;
		}

		WallGen(wall_pos+1, left, bottom, right, door_pos);
		WallGen(top, left, wall_pos-1, right, door_pos);
	}
	else
	{
		do
		{
			wall_pos = (rand() % (right - left)) + (left);
		} while ((wall_pos == last_door) || (wall_pos == right) || (wall_pos == left));

		door_pos = (rand() % (bottom - top)) + (top);

		for (auto i = top; i < bottom; i++)
		{
			if (i != door_pos)
			{
				MAP_ACCESS(wall_pos, i) = 0;
			}
		}
		if ((wall_pos == left) || (wall_pos == right))
		{
			return;
		}
		//std::cout << "VWALL: Door Pos: " << door_pos << " Wall Pos: " << wall_pos << std::endl;
		WallGen(top, wall_pos+1, bottom, right, door_pos);
		WallGen(top, left, bottom, wall_pos-1, door_pos);
	}

	return;
}

void Maze::Generate()
{
	WallGen(0, 0, maze_rows, maze_cols);
	calc_mask();
	//for (auto r = 0; r < maze_rows; r++)
	//{
	//	std::cout << "|";
	//	for (auto c = 0; c < maze_cols; c++)
	//	{
	//		if (MAP_ACCESS(c,r))
	//		{
	//			std::cout << "#";
	//		}
	//		else
	//		{
	//			std::cout << " ";
	//		}
	//	}
	//	std::cout << "|" << std::endl;
	//}
	//std::cout << "---------------------------------------------------------" << std::endl;
}

/* This function creates the maze from a pattern of bools, such as would be loaded from a file */
bool Maze::LoadPattern(std::vector<bool>& pattern, unsigned int pattern_width)
{
    if (pattern.size() > (pattern_width * pattern_width))
    {
        return false;
    }
    else
    {
        if (pattern_width > MAX_DIM)
        {
            std::cout << "Failed to init Maze, rows/cols exceeds limit of " << MAX_DIM << std::endl;
        }
        map_v.clear();
        std::cout << "Cleared Level data!" << std::endl;
        for (unsigned int i = 0; i < pattern.size(); i++)
        {
            if((i % pattern_width) == 0)
            {
                std::cout << std::endl;
            }
            map_v.push_back(pattern[i]);
            if(pattern[i] == 0)
            {
                std::cout << '#';
            }
            else
            {
                std::cout << ' ';
            }

        }
        maze_cols = pattern_width;
        maze_rows = pattern_width;

        calc_mask();
        return true;
    }
}

void Maze::Path(MPVector<void*>& path, unsigned int start_idx, unsigned int end_idx)
{
    float totalCost;
    int result = 0;
    if(end_idx >= maze_cols * maze_rows)
    {
        std::cout << "ERROR: Pathfinding is off grid! Idx :" << end_idx << std::endl;
        return;
    }
    if (path_p != nullptr)
    {
        result = path_p->Solve((void*)start_idx, (void*)end_idx, &path, &totalCost);
    }
    if (result != MicroPather::SOLVED)
    {
        std::cout << "Did not solve path! Result: " << result << std::endl;
    }
    //else
    //{
    //    std::cout << "Path solved, cost: " << totalCost << std::endl;
    //}
}

bool Maze::calc_mask()
{
	for (unsigned int row = 0; row < maze_rows; row++)
	{
		for (unsigned int col = 0; col < maze_cols; col++)
		{
			if (MAP_ACCESS(col, row) == 1)
			{
				MAP_ACCESS(col, row) |= FLOOR_BIT_MASK;
				MAP_ACCESS(col, row) |= CEILI_BIT_MASK;
				if ((col == 0) || (MAP_ACCESS(col-1, row) == 0))
				{
					MAP_ACCESS(col, row) |= WWALL_BIT_MASK;
				}
				if ((col == (maze_cols - 1)) || (MAP_ACCESS(col+1, row) == 0))
				{
					MAP_ACCESS(col, row) |= EWALL_BIT_MASK;
				}
				if ((row == 0) || (MAP_ACCESS(col, row-1) == 0))
				{
					MAP_ACCESS(col, row) |= NWALL_BIT_MASK;
				}
				if ((row == (maze_rows - 1)) || (MAP_ACCESS(col, row+1) == 0))
				{
					MAP_ACCESS(col, row) |= SWALL_BIT_MASK;
				}
			}
			//std::cout << std::hex << MAP_ACCESS(col, row);
		}
		//std::cout << "\n";
	}
	return true;
}

unsigned int Maze::map_access(unsigned int x, unsigned int y)
{
    //return 0;
    return MAP_ACCESS(x,y);
}


void Maze::map_index(unsigned int idx, unsigned int & x, unsigned int & y)
{
    if (idx < map_v.size())
    {
        x = idx % maze_cols;
        y = idx / maze_cols;
    }
    else
    {
        x = 0;
        y = 0;
    }
}

unsigned int Maze::map_idx_from_cart(unsigned int x, unsigned int y)
{
    if (x >= maze_cols)
    {
        x = maze_cols - 1;
    }
    if (y >= maze_rows)
    {
        y = maze_rows - 1;
    }

    return (y * maze_cols) + x;
}

/* Provides Micropather with shortest distance between two points */
float Maze::LeastCostEstimate(void * nodeStart, void * nodeEnd)
{
    unsigned int sx, sy, ex, ey;
    float delta;
    map_index((unsigned int)(MP_UPTR)nodeStart, sx, sy);
    map_index((unsigned int)(MP_UPTR)nodeEnd, ex, ey);
    delta = sqrtf((float)(sx-sy)*(sx-sy) + (float)(ey - sy)*(ey - sy));
    return delta;
}

#define PATH_BASE_COST 1.f
#define PATH_CORNER_COST (PATH_BASE_COST * 1.f)
#define PATH_CORNER_DENY_FACTOR 3.f

/* Supplies Micropather with adjacency cost for pathfinding */
void Maze::AdjacentCost(void * node, MPVector<StateCost>* neighbours)
{
    const int dx[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };
    const int dy[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };
    float cost[8] = { PATH_BASE_COST, PATH_CORNER_COST, PATH_BASE_COST, PATH_CORNER_COST, PATH_BASE_COST, PATH_CORNER_COST, PATH_BASE_COST, PATH_CORNER_COST };
    unsigned int x, y;
    map_index((unsigned int)(MP_UPTR)node, x, y);

    unsigned int enclosed_count = 0;

    /* Do an initial pass to see if there is a NSEW blockage and raise corner cost accordingly */
    for (int i = 0; i < 8; i += 2)
    {
        int nx = x + dx[i];
        int ny = y + dy[i];
        int nbr_a;
        int nbr_b;

        if (i == 0)
        {
            nbr_a = 7;
            nbr_b = 1;
        }
        else
        {
            nbr_a = i - 1;
            nbr_b = i + 1;
        }

        if (nx >= 0 && ny >= 0 && nx < maze_cols && ny < maze_rows && map_access(nx, ny) == 0)
        {
            cost[nbr_a] *= PATH_CORNER_DENY_FACTOR;
            cost[nbr_b] *= PATH_CORNER_DENY_FACTOR;
        }
    }

    /* Populate the neighbour cost accounting for blocked cells and the results of the corner sweep */
    for (int i = 0; i < 8; ++i)
    {
        int nx = x + dx[i];
        int ny = y + dy[i];
        if (nx >= 0 && ny >= 0 && nx < maze_cols && ny < maze_rows)
        {
            unsigned int blocked = map_access(nx, ny);
            StateCost nodeCost = { (void*)map_idx_from_cart(nx, ny), cost[i] };

            if (blocked == 0)
            {
                nodeCost.cost = FLT_MAX;
            }

            neighbours->push_back(nodeCost);
        }
    }
}

/* Debug method for Micropather */
void Maze::PrintStateInfo(void * node)
{
   /* Maze& cell = GetCell((int)node);
    cout << "Cell x: " << cell.x << ", y: " << cell.y;*/
}

void Maze::PutBlock(sf::Rect<unsigned int> dims, bool walkable)
{
    unsigned int col_extent = (dims.left + dims.width);
    unsigned int row_extent = (dims.top + dims.height);
    if(col_extent >= maze_cols)
    {
        dims.left -= (col_extent - maze_cols);
    }
    if(row_extent >= maze_rows)
    {
        dims.top -= (row_extent - maze_rows);
    }

    for (auto y = 0; y < dims.height; y++)
    {
        for(auto x = 0; x < dims.width; x++)
        {
            MAP_ACCESS(dims.left + x, dims.top + y) = (walkable ? 1 : 0);
            (walkable ? walkable_count++ : walkable_count--);
        }
    }

}

void Maze::PutBorders()
{
    sf::Rect<unsigned int> border;

    border.left = 0;
    border.top = 0;
    border.width = maze_cols;
    border.height = 1;

    PutBlock(border);

    border.top = maze_rows - 1;

    PutBlock(border);

    border.width = 1;
    border.height = maze_rows;
    border.top = 0;

    PutBlock(border);

    border.left = maze_cols-1;

    PutBlock(border);
}


void Maze::RandomBlock(bool walkable)
{
    sf::Rect<unsigned int> dims;
    dims.left = (rand() % (maze_cols-1));
    dims.top = (rand() % (maze_rows-1));
    dims.width = (rand() % (unsigned int)(maze_cols/10));
    dims.height = (rand() % (unsigned int)(maze_rows/10));

    PutBlock(dims, walkable);
}

void Maze::Strew(unsigned int iterations)
{
    iterations += maze_cols;
    unsigned int total_tiles = maze_cols * maze_rows;

    for(auto i = 0 ; i < iterations ; i++ )
    {
        if(walkable_count - (maze_cols + maze_rows)*2 > (total_tiles - walkable_count))
        {
            RandomBlock(true);
        }
        else
        {
            RandomBlock(false);
        }
    }
}

void Maze::PlacePlayer()
{
    unsigned int row = (rand() % maze_rows-2);
    bool placed = false;
    unsigned int col = 0;
    while(placed == false)
    {
        if(MAP_ACCESS(col, row))
        {
            /* Place player */
        }
    }

}

//void Maze::PlaceExit();
//void Maze::PlaceEnemies();

