#include "Node.h"
#include "GridNode.h"
#include "Ray.h"
#include "Type.h"
int Node::nodeCount = 0;
fec2 Node::player_pos;
RenderWindow * Node::window_ptr = nullptr;
float Node::player_last_shot = 0.f;

int Node::player_score = 0;
Font* Node::font_ptr = nullptr;

bool Node::debugDrawOn = true;

/* Forward declarations for behaviour functions */
void player_behaviour(Node *node_p);
void zombie_behaviour(Node *node_p);
void health_behaviour(Node *node_p);
void treasure_behaviour(Node *node_p);
void default_trigger_behaviour(Node *node_p);

const collider_type_e collider_type_table[MAX_TYPE_ID] = {
    PLAYER_NODE, //PLAYER_TYPE,                  1
    MOVER_NODE, //DISINFECTOR_TYPE,             2
    MOVER_NODE, //INTERRUPTOR_TYPE,             3
    TRIGGER_NODE,  //    PORT_TYPE,             4
    MOVER_NODE,  //    GARBAGECOLLECTOR_TYPE,   5
    STATIC_BLOCK_NODE,  //    WATCHDOG_TYPE,    6    
    TRIGGER_NODE,  //    FRAGMENT_TYPE,         7
    STATIC_BLOCK_NODE,  //    fIREWALL_TYPE,    8
    MOVER_NODE,  //    LOGfILE_TYPE,            9
    MOVER_NODE,  //    EXEfILE_TYPE,            10
    MOVER_NODE,  //    SOURCEfILE_TYPE,         11
    MOVER_NODE,  //    BUffER_TYPE,             12
    MOVER_NODE,  //    VIRUS_TYPE,              13
    MOVER_NODE,  //    WORM_TYPE,               14
    MOVER_NODE,  //    TROJAN_TYPE,             15
    MOVER_NODE,  //    LOGICBOMB_TYPE,          16
    TRIGGER_NODE,  //    BULLET_TYPE,           17
    MOVER_NODE,  //    NO_TYPE_ID,
};
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ---- NODE functions ---- */

void Node::RecalcBounds()
{
    bounds = FloatRect(position - Vector2f(radius, radius), Vector2f(radius * 2, radius * 2));
    radius_squared = radius*radius;
}

float Node::DistanceSquared(Node &rh_node)
{
    return ((rh_node.position.x - position.x)*(rh_node.position.x - position.x) + (rh_node.position.y - position.y)*(rh_node.position.y - position.y));
}

/* Check if two nodes are overlapping by radius (Mover to Mover collisions)*/
bool Node::RadiusOverlap(Node &rh_node, Vector2f &seperation_vector)
{
    if (collider.overlap(rh_node.collider, seperation_vector))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Construct a node with a specified type, position, radius and velocity
Node::Node(collider_type_e collider, Vector2f new_pos, float new_radius, Vector2f vel) :
    Text(), collider_type(collider), position(new_pos), radius(new_radius), id(++nodeCount), collider(new_pos, new_radius, debugDrawOn)
{
    RecalcBounds();
    dead = false;
    cout << "New Node added! id: " << id << "type: " << collider_type << " pos: " << position.x << ", " << position.y << " radius: " << radius << endl;
    //Get the node to stay where it is
    resultant_pos = position;
    if (isnan(position.x))
    {
        cout << "nan found!" << endl;
    }
    type_ptr = nullptr;
}

Node::Node(String node_char, collider_type_e node_type, Vector2f new_pos, float new_radius, Vector2f vel):
    Text(node_char, *font_ptr, 20), collider_type(node_type), position(new_pos), radius(new_radius), id(++nodeCount), collider(new_pos, new_radius, debugDrawOn)
{
    RecalcBounds();
    dead = false;
    //cout << "New Node added! id: " << id << "type: " << type << " pos: " << position.x << ", " << position.y << " radius: " << radius << endl;
    //Get the node to stay where it is
    resultant_pos = position;
    setOrigin(getLocalBounds().width/2, getLocalBounds().height);
    if (collider_type == MOVER_NODE)
    {
        setColor(Color(160, 255, 180, 255));
    }

    if (isnan(position.x))
    {
        cout << "nan found!" << endl;
    }
    type_ptr = nullptr;
}

Node::Node(Type * node_type_ptr, collider_type_e collider, Vector2f new_pos, float new_radius) :
    type_ptr(node_type_ptr), collider_type(collider), position(new_pos), radius(new_radius), id(++nodeCount), collider(new_pos, new_radius, debugDrawOn)
{
    RecalcBounds();
    dead = false;
    type_ptr->SetPos(new_pos);
    //type_ptr->SetGoalPos(new_pos);
    resultant_pos = position;
    setOrigin(getLocalBounds().width / 2, getLocalBounds().height);
    //cout << type_ptr.use_count() << " references in use" << endl;
    if (collider_type == MOVER_NODE)
    {
        setColor(Color(160, 255, 180, 255));
    }

    if (isnan(position.x))
    {
        cout << "nan found!" << endl;
    }
}

Node::~Node()
{
    // cout << "Destroying node " << id << endl;
}

//Render the node to the static render target
void Node::Render()
{
    setPosition(position);


    if (collider_type == PLAYER_NODE)
    {
        collider.SetDrawColour(Color::Magenta);
    }
    else if (collider_type == STATIC_BLOCK_NODE)
    {
        collider.SetDrawColour(Color::Blue);
    }
    else if (collider_type == TRIGGER_NODE)
    {
        collider.SetDrawColour(Color::Yellow);
    }
    else if (collider_type == MOVER_NODE)
    {
        collider.SetDrawColour(Color::Red);
        //cout << getRotation() << endl;
    }

    collider.draw_me();
    if (type_ptr != nullptr)
    {
       //type_ptr->Render();
    }
}

//Update the processing for the node, based on the time since last update (passed in from host)
bool Node::Update(float secs_since_last)
{
    Type * spawns_ptr = nullptr;
    float player_move = PLAYER_MOVE_SPEED * secs_since_last;
    float monster_move = MONSTER_MOVE_SPEED * secs_since_last;
    type_ptr->SetPos(position);
    fec2 to_player = player_pos - position;
    bool ret_val = false;

    type_ptr->SyncNode(this);
    /* Run the nodes update loop */
    type_ptr->runUpdate();    
    if (type_ptr->GetTypeDataPtr()->id == BULLET_TYPE)
    {
        int i = 0;
        i++;
    }
    if (isnan(type_ptr->GetPos().x))
    {
        cout << "nan found!" << endl;
    }
    /* Update the new position from the loop, will be corrected in collision resolution step */
    position = GridNode::BoundsCheck(type_ptr->GetPos());

    /* Update static player pos from player type */
    player_pos = Type::GetPlayerPos();
    /* Flag if the node has expired and should be removed */
    dead = type_ptr->isDead();

    if (dead)
    {
        type_ptr->SpawnFragments();
    }

    ret_val = true;

    collider.centre = position;
    resultant_pos = position;
    if (isnan(position.x))
    {
        cout << "nan found!" << endl;
    }
    return ret_val;
}

//Placeholder function to use for trigger nodes
void Node::Trigger(Node & triggered, fec2 &collide_resultant)
{
    if (collider_type == TRIGGER_NODE)
    {
        type_ptr->hitBy(*(triggered.type_ptr->GetTypeDataPtr()));
    }
}

String Node::GetScoreString()
{
    ostringstream stream;
    String ret_string;
    if (player_score < 0)
    {
        stream << "GAME OVER!";
    }
    else
    {
        stream << "Score: " << player_score;
    }
    ret_string = stream.str();
    return ret_string;
}

void Node::SetFont(Font & font)
{
    font_ptr = &font;
}

fec2 Node::GetPlayerPos()
{
    return player_pos;
}

void Node::ConfigDraw(bool debug_draw)
{
    debugDrawOn = debug_draw;
}

void Node::AssignWindow(RenderWindow & window)
{
    window_ptr = &window;
}


//Resolve a collision for a trigger node
bool Node::HandleTriggerCollision(Node & rhs, fec2 & resultant)
{
    if (rhs.collider_type == TRIGGER_NODE && collider_type != TRIGGER_NODE)
    {
        rhs.Trigger(*this, resultant);
        type_ptr->RegisterContact(*rhs.type_ptr.get());
        return true;
    }
    return false;
}

//Resolve collisions for a static node
bool Node::HandleStaticCollisions(Node & rhs, fec2 & resultant)
{
    if (rhs.collider_type == STATIC_BLOCK_NODE)
    {
        if (collider_type != STATIC_BLOCK_NODE)
        {
            resultant_pos += resultant;
            type_ptr->hitBy(*(rhs.type_ptr->GetTypeDataPtr()));
            type_ptr->RegisterContact(*rhs.type_ptr.get());
        }
        return true;
    }
    return false;
}

//Resolve collisions for a mover node
bool Node::HandleMoverCollision(Node & rhs, fec2 & resultant)
{
    if (rhs.collider_type == MOVER_NODE || rhs.collider_type == PLAYER_NODE)
    {
        if (collider_type == TRIGGER_NODE)
        {
            Trigger(rhs, resultant);
        }
        else if (collider_type != STATIC_BLOCK_NODE)
        {
            resultant_pos += fec2(resultant.x / 2, resultant.y / 2);
            rhs.resultant_pos -= fec2(resultant.x / 2, resultant.y / 2);
            Trigger(rhs, resultant);
        }
        type_ptr->RegisterContact(*rhs.type_ptr.get());
        return true;
    }
    return false;
}

//Resolve collisions for a player node
bool Node::HandlePlayerCollision(Node & rhs, fec2 & resultant)
{
    fec2 seperation_v = fec2(resultant.x / 2.f, resultant.y / 2.f);
    if (rhs.collider_type == PLAYER_NODE)
    {
        if (collider_type != STATIC_BLOCK_NODE && collider_type != TRIGGER_NODE)
        {
            rhs.resultant_pos -= seperation_v;
            resultant_pos += seperation_v;
            Trigger(rhs, resultant);
        }
        else if (collider_type == STATIC_BLOCK_NODE)
        {
            rhs.resultant_pos -= resultant;
        }
        type_ptr->RegisterContact(*rhs.type_ptr.get());
        return true;
    }
    return false;
}

void Node::HasHitWall(edge & wall)
{
    type_ptr->RegisterContact(wall);
}

void Node::AlertTo(TypeData& type_detected)
{
    type_ptr->AlertTo(type_detected);
}

Type * Node::GetTypePtr()
{
    return type_ptr.get();
}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ---- GRIDNODE Functions ---- */

collider_type_e GetColliderType(node_type_id id)
{
    return collider_type_table[id];
}
