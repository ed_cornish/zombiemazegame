#include "Type.h"
#include "GridNode.h"
#include "Ray.h"
#include "Node.h"
#include <functional>
#include "EntityData.h"
#include "Behaviours.h"
#include "Thor/Math.hpp"
#include "Explosion.h"
#include "SoundFX.h"
///* Static member variables */
using namespace std::placeholders;

/* Used for elapsed time between frames */
float Type::time_elapsed_s = 0.f;
/* Font for character rendering */
Font *Type::font_ptr = nullptr;
/* Debug Draw flag */
bool Type::debugDraw = true;

unsigned int TypeData::instance_count = 0;

/* Window pointer for rendering */
RenderWindow *Type::window_ptr = nullptr;

/* Player position for use by other entities */
fec2 Type::player_pos = fec2(0, 0);

///* Base TYPE class methods */

fec2 Type::GetPlayerPos()
{
    return player_pos;
}

void Type::UpdatePlayerPos(fec2 pos)
{
    player_pos = pos;
}

/* Set the positional origin of the text object to the centre */
void Type::CentreText()
{
    FloatRect text_rect;
    text_rect = data.render_text.getLocalBounds();
    data.render_text.setOrigin(text_rect.left + text_rect.width / 2, text_rect.top + text_rect.height / 2);
}

/* Base method for handling contacts between objects */
void Type::handleHit()
{
    if (data.hit_node != nullptr)
    {
        /* Call the hitBy method of the node we hit, passing ourselves in */
        data.hit_node->hitBy(*GetTypeDataPtr());
        data.hit_node = nullptr;
    }
}

/* Apply any active effects */
void Type::ApplyEffects()
{
    float elapsed_s = Type::GetSecsElapsed();

    for (auto & effect : data.effs)
    {
        effect.Apply(elapsed_s);
    }
    /* Remove any effects that have timed out */
    data.effs.erase(remove_if(data.effs.begin(), data.effs.end(), [](Effect & effect) { return effect.TimedOut(); }), data.effs.end());
}


void Type::InitEmitter()
{
    emitter.setEmissionRate(25);
    emitter.setParticleLifetime(thor::Distributions::uniform(sf::seconds(0.1f), sf::seconds(0.3f)));
    emitter.setParticleScale(thor::Distributions::rect(fec2(0.6f, 0.6f), fec2(0.4f, 0.4f)));
    Color part_colour = data.render_colour;
    part_colour.a = 100;
    emitter.setParticleColor(part_colour);
//    emitter_p->setParticlePosition(thor::Distributions::circle(data.world_pos, data.host_node_ptr->collider.radius));
    auto_conn_p.reset(new thor::ScopedConnection(GFXMgr::RegisterEmitter(emitter)));
}

void Type::SyncNode(Node * node_ptr)
{
    if (node_ptr)
    {
        data.host_node_ptr = node_ptr;
    }
}

TypeData * Type::GetTypeDataPtr()
{
    return &data;
}

/* Base class constructor, sets the position of the object and nothing else.*/
Type::Type(fec2 pos, node_type_id id, State * initial_state, node_type_id * parent_id_p)
{
    data.id = id;
    /* Setup defaults */
    //type_id = NO_TYPE_ID;
    //data.hit_node = nullptr;
    InitTypeData(data);

    data.world_pos = pos;
    //data.detect_range = 0.f;
    data.timer = 0.f;

    //Setup the initial state
    if (initial_state)
    {
        state_m.initState(initial_state);
    }
    else
    {
        state_m.initState(new State(nullptr, bind(initStateFrameB, _1, _2)));
    }

    /* Check that a static window object has been assigned - needed for rendering purposes and mouse input */
    if(window_ptr == nullptr)
    {
        std::cout << "ERROR: Window not initialised for Type creation!" << std::endl;
    }
    data.render_colour = GetColor(id, parent_id_p);
    InitEmitter();
    /* Initialise as mover node so that overlap is seperated */
    Node add_node(this, GetColliderType(id), data.world_pos, GetColliderRadius(data));
    GridNode::AddNode(add_node);

    CentreText();
    GFXMgr::RegisterChar(*this);
}

Type::Type(const Type &)
{

}

Type::~Type()
{
    if (data.host_node_ptr && data.hp <= 0)
    {
       //thor::Distribution<Vector2f> pos_offset = thor::Distributions::rect(data.world_pos, fec2(data.host_node_ptr->radius, data.host_node_ptr->radius));
        Explosion death_burst(data.world_pos, GetMaxHitPoints(data.id) / (float)GetMaxHitPoints(BULLET_TYPE), &(data.render_colour) );
        if(data.id == PLAYER_TYPE)
        {
            SoundFX::play(PLAYER_DIE_SND);
        }
        else
        {
            SoundFX::play(ENEMY_DIE_SND);
        }
    }
}

void Type::runUpdate()
{
    float elapsed_s = Type::GetSecsElapsed();
    ApplyEffects();
    state_m.runUpdate(data, elapsed_s);
    data.render_text.setPosition(data.world_pos);
    data.contact_list.clear();
    data.geom_contact_list.clear();
    emitter.setParticlePosition(thor::Distributions::circle(data.world_pos, data.host_node_ptr->collider.radius * 1.4f));
}

void Type::hitBy(TypeData & data_that_hit)
{
    data.has_hit_ptr = &data_that_hit;
}

void Type::RegisterContact(Type & contacting_object)
{

    data.contact_list.push_back(contacting_object.shared_from_this());
    contacting_object.data.contact_list.push_back(shared_from_this());
}

void Type::RegisterContact(edge & contacting_edge)
{
    data.geom_contact_list.push_back(&contacting_edge);
}

/* Notify the entity that it has been hit by another node */
void Type::registerHit(Type & node_hit)
{
    data.hit_node = &node_hit;
}

/* Indicate if the node should be removed from the world */
bool Type::isDead()
{
    return (data.hp <= 0);
}

bool Type::isPlayer()
{
    if (data.id == PLAYER_TYPE)
    {
        return true;
    }
    return false;
}

int Type::GetHP()
{
    return data.hp;
}

/* Setup the font for character drawing */
void Type::SetFont(Font &font)
{
    font_ptr = &font;
}

/* Retrieve the font */
Font * Type::GetFontPtr()
{
    return font_ptr;
}

/* Assign the window for rendering, mouse position etcetera */
void Type::InitWindow(RenderWindow &window)
{
    window_ptr = &window;
}

RenderWindow* Type::GetWindowPtr()
{
    return window_ptr;
}

/* Overall frame tick for all types, call once per frame */
void Type::TickFrame(float secs_elapsed)
{
    time_elapsed_s = secs_elapsed;
}

/* Get the time elapsed this frame */
float Type::GetSecsElapsed()
{
    return time_elapsed_s;
}

/* Get a sf::String representation of the object */
String Type::GetTypeString()
{
    return data.type_string;
}

/* Set the HP to 0 */
void Type::Kill()
{
    data.hp = 0;
}

/* Increase the HP */
void Type::Heal(unsigned int heal_amount)
{
    data.hp += heal_amount;
}

void Type::AddEffect(Effect eff)
{
    // Filter any new effects against current effect
    for (auto &curr_eff : data.effs)
    {
        curr_eff.Filter(eff);
    }
    data.effs.push_back(eff);
}

fec2 Type::GetPos()
{
    return data.world_pos;
}

void Type::SetPos(fec2 pos)
{
    data.world_pos = pos;
}

void Type::SetGoalPos(fec2 pos)
{
    data.move_data->goal_pos = pos;
}

fec2 Type::GetTargetPos()
{
    return data.target_pos;
}

/* Render to the build in window_ptr */
void Type::Render()
{//TODO: Deprecate
    if (window_ptr != nullptr)
    {
        window_ptr->draw(data.render_text);
    }
}
#define OUTLINE_COLOUR_DELTA 50
#define OUTLINE_COLOUR_BOOST(c) ((c > (OUTLINE_COLOUR_DELTA) ? c -= OUTLINE_COLOUR_DELTA : c += 0 ))
void Type::Render(RenderWindow & window, bool debug_mode)
{
    static bool shape_init = false;
    static RectangleShape core_rect;
    static CircleShape    core_circle;

    if (shape_init == false)
    {
        core_circle.setOutlineThickness(1.4f);
        shape_init = true;
        core_rect.setOutlineThickness(1.4f);
    }
    Color outline_colour = data.render_colour;
    OUTLINE_COLOUR_BOOST(outline_colour.r);
    OUTLINE_COLOUR_BOOST(outline_colour.g);
    OUTLINE_COLOUR_BOOST(outline_colour.b);

    if (IsTypeID(PLAYER_TYPE))
    {
        core_circle.setFillColor(data.render_colour);
        core_circle.setPosition(data.world_pos);
        core_circle.setRadius(GetColliderRadius(data));
        core_circle.setOrigin(fec2(core_circle.getRadius(), core_circle.getRadius()));
        core_circle.setOutlineColor(outline_colour);
        window.draw(core_circle);
    }
    else
    {
        float width = GetColliderRadius(data);
        core_rect.setFillColor(data.render_colour);
        core_rect.setPosition(data.world_pos);
        core_rect.setSize(fec2(width*2.f, width*2.f));
        core_rect.setOrigin(fec2(width, width));
        core_rect.setOutlineColor(outline_colour);
        window.draw(core_rect);
    }

    //if(debug_mode)
    //{
    //    data.render_text.setPosition(data.world_pos);
    //    data.render_text.setColor(data.render_colour);
    //    window.draw(data.render_text);
    //}

    /* Don't draw health bars for the player, bullets, or ports */
    if (!IsTypeID(PLAYER_TYPE) && !IsTypeID(BULLET_TYPE) && !IsTypeID(PORT_TYPE))
    {
        RectangleShape health_bar;
        float hp_fraction = GetCurrHPFraction(data);
        float radius = GetColliderRadius(data);
        health_bar.setSize(Vector2f(hp_fraction  * radius * 2.f, 5.f));
        health_bar.setPosition(data.world_pos - Vector2f(radius, radius));
        health_bar.setFillColor(Color(255 - 255 * hp_fraction, 255 * hp_fraction, 0, 255));
        window.draw(health_bar);
    }
}

bool Type::IsTypeID(node_type_id id)
{
    if (data.id == id)
    {
        return true;
    }
    return false;
}

void Type::SpawnFragments()
{
    thor::Distribution<Vector2f> offset_dist = thor::Distributions::rect(data.world_pos, fec2(15.f, 15.f));
    switch (data.id)
    {
    case DISINFECTOR_TYPE:
    case INTERRUPTOR_TYPE:
    case WATCHDOG_TYPE:
    case GARBAGECOLLECTOR_TYPE:
        new Type(offset_dist(), FRAGMENT_TYPE);
        break;
    default:
        break;
    }
}

/* Another entity is alerting this to the presence of an entity (i.e. the player) */
void Type::AlertTo(TypeData & alert_to_type)
{
    data.detected_ptr = &(alert_to_type);
}

/* Set the debug draw flag */
void Type::ConfigDraw(bool debug_draw)
{
    debugDraw = debug_draw;
}

TypeData::TypeData()
{
    id = NO_TYPE_ID;
    hit_node = nullptr;
    host_node_ptr = nullptr;
    has_hit_ptr = nullptr;
    detected_ptr = nullptr;
    instance_count++;
    instance_id = instance_count;
}

/* Initialise default values for MoverData */
MoverData::MoverData()
{
    max_speed = MONSTER_MOVE_SPEED;
}
