#include "Behaviours.h"
#include "Type.h"
#include "Ray.h"
#include "EntityData.h"
#include "Explosion.h"
#include "SoundFX.h"

State * playerFrameB(TypeData & data, float elapsed_s)
{
    static sf::Clock hp_degrade_clk;
    static sf::Clock warp_reset_clk;
    static sf::Clock fwall_reset_clk;
    float player_move = GetCurrentMaxSpeedPerSecond(data) * elapsed_s;
    fec2 vel;

    //TODO: This is a cheat so we can test without dying
    if (data.hp < 100)
    {
       // data.hp = 100;
    }


    if (data.hp > 300)
    {
        data.hp = 300;
    }
    if (hp_degrade_clk.getElapsedTime().asSeconds() > (1 / GetCurrHPFraction(data)))
    {
        hp_degrade_clk.restart();
        if (data.hp > 100)
        {
            data.hp -= 1;
        }
    }

    Type::UpdatePlayerPos(data.world_pos);

    RenderWindow* window_ptr = Type::GetWindowPtr();
    if (window_ptr)
    {
        data.target_pos = window_ptr->mapPixelToCoords(Mouse::getPosition(*window_ptr));
    }

    data.render_text.setPosition(data.world_pos);

    if (Keyboard::isKeyPressed(Keyboard::A))
    {
        vel.x -= player_move;
    }
    else if (Keyboard::isKeyPressed(Keyboard::D))
    {
        vel.x += player_move;
    }
    if (Keyboard::isKeyPressed(Keyboard::W))
    {
        vel.y -= player_move;
    }
    else if (Keyboard::isKeyPressed(Keyboard::S))
    {
        vel.y += player_move;
    }

    CastLOSRay(data);

    fec2 aim_pt = data.target_pos - data.world_pos;
    fec2 offset = data.world_pos + aim_pt.norm() * (data.host_node_ptr->radius * 2.4f);
    if (Mouse::isButtonPressed(Mouse::Left) && data.timer > GetBaseAttackRate(data.id))
    {
        GAME_DEBUG_LOG("Bang!")
        /* Reset the counter */
        data.timer = 0.f;
        SoundFX::play(PLAYER_SHOOT_SND);
        State * bullet_state = new State(bind(bulletLaunchEnterB, _1, _2, aim_pt, GetCurrHPFraction(data) * GetBulletDamage(data.id)), bind(bulletLaunchB, _1, _2, data.instance_id, aim_pt));
        new Type(offset, BULLET_TYPE, bullet_state, &(data.id));// bind(bulletLaunchEnterB, _1, _2, data.target_pos), bind(bulletLaunchB, _1, _2, &data)));
    }
    else if (Keyboard::isKeyPressed(Keyboard::Space) && (warp_reset_clk.getElapsedTime().asSeconds() > PLAYER_WARP_INTERVAL))
    {
        Color warp_colour = Color::Red;
        GAME_DEBUG_LOG("Teleport");
        Ray warp_ray(*(data.host_node_ptr), data.target_pos, true);
        warp_ray.RayCastGrid();
        if (GridNode::GetCell(warp_ray.GetTerminationPoint()).CanWalk())
        {
            data.world_pos = warp_ray.GetTerminationPoint();
        }

        Explosion warp_burst_b(data.world_pos, 2.f);
        warp_ray.draw_me(*window_ptr);
        SoundFX::play(WARP_SND);
        warp_reset_clk.restart();
        //data.timer = 0.f;
    }
    else if (Keyboard::isKeyPressed(Keyboard::F) && (fwall_reset_clk.getElapsedTime().asSeconds() > PLAYER_FWALL_INTERVAL))
    {
        GAME_DEBUG_LOG("Firewall");

        new Type(offset, FIREWALL_TYPE);

        fwall_reset_clk.restart();
    }

    data.timer += elapsed_s;

    data.world_pos += vel;
    data.move_data->vel = fec2();

    return nullptr;
}
