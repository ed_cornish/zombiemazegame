#include "MapLoader.h"
#include <stdlib.h>
#include <cctype>
#include <algorithm>
#ifdef _MSC_VER
#include <experimental\filesystem>
#else
#include <dirent.h>
#endif

/* Prefix chars */
#define COMMENT_CHAR '"'
#define WALL_CHAR '#'
#define FLOOR_CHAR '.'
#define GRIDWIDTH_CHAR 'g'
#define CELLWIDTH_CHAR 'c'
#define NODEADD_CHAR '!'
#define RANDSPAWN_CHAR '*'
/* Add further char defines here */

/* Line buffer size */
#define LINE_LIMIT 64


int LevelList::completed_lvl = 0;
vector<diskLevel> LevelList::level_v;
bool LevelList::load_new = false;

void diskLevel::Process()
{
    if (fs_p && fs_p->is_open())
    {
        while (ProcessLine())
        {
            //cout << "Processing...\n";
        }
    }
    else
    {
        cout << "ERROR: filestream is not open!\n";
    }
}

bool diskLevel::ProcessInt(unsigned int & val, char & buff)
{
    char* endP;
    long int tval = strtol(&buff, &endP, 10);
    if (tval || buff == '0')
    {
        val = (unsigned int)tval;
        //cout << "Extracted value of " << val << "\n";
        /* Jump over the converted characters */
        return true;
    }
    else
    {
        /* Failed to convert */
        success_flag &= false;
        return false;
    }
}

bool diskLevel::ProcessLine()
{
    char buff[LINE_LIMIT];
    int c_count = 0;

    if (fs_p && *fs_p)
    {
        if (fs_p->getline(buff, LINE_LIMIT))
        {
            c_count = fs_p->gcount();
            for (int i = 0; i < c_count; i++)
            {
                switch (buff[i])
                {
                case COMMENT_CHAR:
                    //cout << buff + i + 1 << "\n";
                    i = c_count;
                    /* Discard the rest of the line as this is a comment */
                    break;
                case FLOOR_CHAR:
                    level_data.maze_pattern.push_back(true);
                    break;
                case WALL_CHAR:
                    level_data.maze_pattern.push_back(false);
                    break;
                case GRIDWIDTH_CHAR:
                    if (ProcessInt(level_data.grid_w, buff[i + 1]))
                    {
                        //cout << "Extracted grid width\n";
                        i = c_count;
                    }
                    break;
                case CELLWIDTH_CHAR:
                    if (ProcessInt(level_data.cell_w, buff[i + 1]))
                    {
                       // cout << "Extracted cell width\n";
                        i = c_count;
                    }
                    break;
                /*case NODEADD_CHAR:
                    unsigned int id;
                    if (ProcessInt(id, buff[i + 1], i))
                    {
                        cout << "Extracted node id " << id << "\n";
                        level_data.maze_pattern.push_back(true);
                        ProcessNode(id, level_data.maze_pattern.size()-1);
                    }
                    break;*/
                case RANDSPAWN_CHAR:
                    {
                        unsigned int node_id = PORT_TYPE;
                        do
                        {
                            node_id = DISINFECTOR_TYPE + (rand() % (WATCHDOG_TYPE - DISINFECTOR_TYPE + 1));
                        }while(node_id == PORT_TYPE);

                        //cout << "Extracted node id " << id << "\n";
                        ProcessNode(node_id, level_data.maze_pattern.size() - 1);
                        cout << "Added Random Node: " << node_id << endl;
                    }
                    level_data.maze_pattern.push_back(true);
                    break;
                default:
                    if (isdigit(buff[i]))
                    {
                        unsigned int id;
                        if (ProcessInt(id, buff[i]))
                        {
                            //cout << "Extracted node id " << id << "\n";
                            level_data.maze_pattern.push_back(true);
                            ProcessNode(id, level_data.maze_pattern.size() - 1);
                        }
                    }
                    else if(isalpha(buff[i]) || ispunct(buff[i]))
                    {
                        //Handle letters in the level - 'glyphs'
                        level_data.maze_pattern.push_back(true);
                        cout << "Glyph: " << buff[i] << endl;
                        ProcessGlyph(buff[i], level_data.maze_pattern.size() - 1);
                    }

                    break;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        cout << "ERROR: Could filestream not valid!\n";
        success_flag &= false;
        return false;
    }

}

void diskLevel::ProcessNode(unsigned int node_id, unsigned int pattern_pos)
{
    spawnData node;
    if (pattern_pos >= level_data.maze_pattern.size())
    {
        cout << "ERROR: Attemping to add node at position " << pattern_pos << " outside of current map range: " << level_data.maze_pattern.size() << endl;
        success_flag = false;
    }
    else
    {
        if (node_id > MAX_TYPE_ID)
        {
            cout << "ERROR: Found node id > MAX_TYPE_ID " << MAX_TYPE_ID << endl;
        }
        else
        {
            node.id = (node_type_id)node_id;
            node.x = pattern_pos % level_data.grid_w;
            node.y = pattern_pos / level_data.grid_w;
            level_data.spawns.push_back(node);
        }

    }
}

void diskLevel::ProcessGlyph(char letter, unsigned int pattern_pos)
{
    glyphData glyph;

    if (pattern_pos >= level_data.maze_pattern.size())
    {
        cout << "ERROR: Attemping to add glyph at position " << pattern_pos << " outside of current map range: " << level_data.maze_pattern.size() << endl;
        success_flag = false;
    }
    else
    {
        glyph.letter = letter;
        glyph.x = pattern_pos % level_data.grid_w;
        glyph.y = pattern_pos / level_data.grid_w;
        level_data.glyphs.push_back(glyph);
    }
}


diskLevel::diskLevel(string fname)
{
    string test_path("./Levels/hub.map");
    fstream test;
    test.open(test_path);
    cout << test.is_open() << " should be opening " << fname.c_str() << endl;
    success_flag = false;
    test.close();
    fstream fs;
    cout << fname << endl;
    fs.open(fname);
    success_flag |= fs.is_open();
    cout << "State: " << fs.is_open() << "\n";
    fs_p = &fs;
    Process();
    cout << endl;
    fs.close();
    fs_p = nullptr;
//    {
//        sf::Clock time_mark;
//
//        srand(time_mark.getElapsedTime().asMicroseconds());
//    }
}

diskLevel::~diskLevel()
{
    int i = 0;
  /*  for (auto c : level_data.maze_pattern)
    {
        if (c)
        {
            cout << '#';
        }
        else
        {
            cout << '.';
        }
        i++;
        if (i >= level_data.grid_w)
        {
            i = 0;
            cout << endl;
        }
    }
    cout << endl;*/
    if (fs_p && fs_p->is_open())
    {
        fs_p->close();
    }
}

bool diskLevel::GetSuccess() const
{
    return success_flag;
}

bool diskLevel::GetMapData(vector<bool>& data) const
{
    if (success_flag)
    {
        data = level_data.maze_pattern;
        return true;
    }

    return false;
}

bool diskLevel::GetSpawnData(vector<spawnData>& data) const
{
    if (success_flag)
    {
        data = level_data.spawns;
        return true;
    }

    return false;
}

bool diskLevel::GetGlyphData(vector<glyphData>&data) const
{
    if(success_flag)
    {
        data = level_data.glyphs;
        return true;
    }
    return false;
}

const unsigned int diskLevel::GetGridWidth() const
{
    return level_data.grid_w;
}

const float diskLevel::GetCellWidth() const
{
    return level_data.cell_w;
}

diskLevel * LevelList::getNextLevel()
{
    // TODO: insert return statement here
    if (completed_lvl < level_v.size())
    {
        load_new = false;
        return &level_v[completed_lvl];
    }
    else
    {
        return nullptr;
    }
}


LevelList::LevelList()
{

}

LevelList::~LevelList()
{
}

void LevelList::markLevelComplete()
{
    if (load_new)
    {
        return;
    }
    completed_lvl++;
    if (completed_lvl >= level_v.size())
    {
        cout << "YOU WIN THE GAME!" << endl;
        completed_lvl = 0;
    }
    load_new = true;
}

diskLevel * LevelList::getHub() const
{
    // TODO: insert return statement here
    if (level_v.size() > 0)
    {
        return &level_v[0];
    }
    return nullptr;
}


#ifdef _MSC_VER
using std::experimental::filesystem::directory_iterator;
#endif
bool LevelList::LoadLevelData()
{
    vector<string> fnames;
    string fname;
    string srch(".map");
#ifdef _MSC_VER
    string folder(".\\Levels\\");
    //fnames.emplace_back("Levels\\hub.map");
    for (auto& dirEntry : directory_iterator(".\\Levels"))
    {
        fname = dirEntry.path().generic_string();
        size_t pos = fname.find(srch);
        if (pos != string::npos && pos == (fname.length() - srch.length()))
        {
            cout << "Found level file: " << fname << endl;
            fnames.push_back(fname);
        }
    }
#else
    DIR *dpdf;
    struct dirent *epdf;
    string folder("./Levels");
    //fnames.emplace_back("./Levels/hub.map");
    dpdf = opendir(folder.c_str());
    if (dpdf != NULL)
    {
       while (epdf = readdir(dpdf))
       {
          //printf("Filename: %s",epdf->d_name);
            std::cout << epdf->d_name << std::endl;
            fname = epdf->d_name;
            size_t pos = fname.find(srch);
            if (pos != string::npos && pos == (fname.length() - srch.length()))
            {
                cout << "Found level file: " << fname << endl;
                string path;
                path.append(folder);
                path.append("/");
                path.append(fname);
                fnames.emplace_back(path);
            }
       }
    }
#endif

    sort(fnames.begin(), fnames.end());

    for (auto& f : fnames)
    {
        level_v.emplace_back(f);
    }
    load_new = true;

    return true;
}

bool LevelList::NewLoadCheck()
{
    return load_new;
}
