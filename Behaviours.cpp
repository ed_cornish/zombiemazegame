//#include <functional>
#include "Behaviours.h"
//#include "StateMachine.h"
#include "Type.h"
#include "GridNode.h"
#include "Ray.h"
#include "Effect.h"
#include "EntityData.h"
#include "Explosion.h"
#include "SoundFX.h"

void RegisterHit(TypeData& data_of_hitter, TypeData& data_of_hitee)
{
    data_of_hitter.has_hit_ptr = &data_of_hitee;
    //data_of_hitee.hit_by_ptr = &data_of_hitter;
}

void RegisterDetection(TypeData& data_of_detecting, TypeData& data_of_detected)
{
    data_of_detecting.detected_ptr = &data_of_detected;
}

bool GetTypeDetected(TypeData& data, node_type_id id)
{
    if (data.detected_ptr && data.detected_ptr->id == id)
    {
        return true;
    }

    return false;
}


bool MoveDataCheck(TypeData& data)
{
    if (data.move_data)
    {
        return true;
    }
    else
    {
        //cout << "ERROR: No movement data defined for " << data.type_string.toAnsiString() << endl;
        return false;
    }
}

bool CastLOSRay(TypeData& data, fec2* target_pos_ptr)
{
    /* This helper function handles casting of a ray from an object. If the target_pos is null, then cast to the data.target_pos.*/
    /* Note that if an orphan ray is required between two points then the Ray API can be used directly */
    if (target_pos_ptr == nullptr)
    {
        target_pos_ptr = &(data.target_pos);
    }
    data.detected_ptr = nullptr;

    fec2 to_target = fec2(*target_pos_ptr - data.world_pos).norm() * data.detect_range;
    fec2 ray_end = data.world_pos + to_target;

    if (data.host_node_ptr)
    {
        Ray world_ray(*(data.host_node_ptr), ray_end, true);
        bool hit_found = world_ray.RayCastGrid();
        //cout << "Ray length " << world_ray.mag() << endl;
        if (hit_found && world_ray.GetHitNode())
        {
            TypeData* data_of_hit_ptr = world_ray.GetHitNode()->GetTypePtr()->GetTypeDataPtr();

            if(data_of_hit_ptr)
                RegisterDetection(data, *data_of_hit_ptr);
        }
        world_ray.draw_me(*Type::GetWindowPtr());
        if (hit_found)
            return true;
    }

    return false;
}

bool CastObstructRay(TypeData & data, fec2 end_point)
{
    /* This helper function handles casting of a ray from an object. If the target_pos is null, then cast to the data.target_pos.*/
    data.detected_ptr = nullptr;

    if (data.host_node_ptr)
    {
        Ray world_ray(*(data.host_node_ptr), end_point, true);
        bool hit_found = world_ray.RayCastGrid();
        //cout << "Ray length " << world_ray.mag() << endl;
        if (hit_found && world_ray.GetHitNode())
        {
            Type * host_hit_ptr = world_ray.GetHitNode()->GetTypePtr();

            if (host_hit_ptr)
                host_hit_ptr->RegisterContact(*data.host_node_ptr->GetTypePtr());

        }
        world_ray.draw_me(*Type::GetWindowPtr());
        if (hit_found)
            return true;
    }

    return false;
}

void FollowPath(TypeData& data)
{
    /* Will follow the path if one is set */
    if (MoveDataCheck(data))
         data.move_data->goal_pos = data.move_data->path.getCurrentWP(data.world_pos);
}

void GetPathToTarget(TypeData& data)
{
    if (MoveDataCheck(data))
        GridNode::FindPath(data.world_pos, data.target_pos, data.move_data->path.reset());
}

void GetPathToGoal(TypeData& data)
{
    if (MoveDataCheck(data))
        GridNode::FindPath(data.world_pos, data.move_data->goal_pos, data.move_data->path.reset());
}

void GetPathToPos(TypeData& data, fec2 pos)
{
    if (MoveDataCheck(data))
        GridNode::FindPath(data.world_pos, pos, data.move_data->path.reset());
}

void UpdateTimer(TypeData & data, float elapsed_s)
{
    data.timer += elapsed_s;
}

bool CheckTimerExpire(TypeData & data, float elapsed_s, float timeout_s)
{
    if (data.timer > timeout_s)
    {
        data.timer = 0.f;
        return true;
    }
    else
    {
        data.timer += elapsed_s;
    }
    return false;
}
/* Return true if a non-system id has been detected */
bool CheckDetectedNonSystem(TypeData & data)
{
    if (data.detected_ptr)
    {
        node_type_id detected_id = data.detected_ptr->id;
        switch (detected_id)
        {
        case PLAYER_TYPE:
        case VIRUS_TYPE:
        case TROJAN_TYPE:
        case WORM_TYPE:
        case LOGICBOMB_TYPE:
            return true;
            break;
        default:
            break;
        }
    }
    return false;
}

bool CheckDetectedTypeId(TypeData & data, node_type_id id_to_check_for)
{
    if(data.detected_ptr)
    {
        if(data.detected_ptr->id == id_to_check_for)
        {
            return true;
        }
    }

    return false;
}

bool CheckDetectedInstance(TypeData & data, unsigned int instance_id)
{
    if(data.detected_ptr && data.detected_ptr->instance_id == instance_id)
    {
        return true;
    }

    return false;
}

node_type_id GetPreferredTarget(TypeData & data)
{
    node_type_id ret_id = PLAYER_TYPE;
    static int wdog_frag_check = 0;
    switch(data.id)
    {
        case GARBAGECOLLECTOR_TYPE:
            ret_id = FRAGMENT_TYPE;
            break;
        case DISINFECTOR_TYPE:
        case INTERRUPTOR_TYPE:
        case WATCHDOG_TYPE:
            wdog_frag_check++;
            ret_id = PLAYER_TYPE;
            if (wdog_frag_check > 3)
            {
                ret_id = FRAGMENT_TYPE;
                wdog_frag_check = 0;
            }
            break;
        default:
            ret_id = NO_TYPE_ID;
            break;
    }
    GAME_DEBUG_LOG("Returned preffered target id: " << ret_id << " for type id " << data.id);
    return ret_id;
}

float GetCurrHPFraction(TypeData & data)
{
    //cout << "HP FRAC " << (data.hp / GetMaxHitPoints(data.id)) << endl;
    return ((float)data.hp / (float)GetMaxHitPoints(data.id));
}

float GetCurrentMaxSpeedPerSecond(TypeData & data)
{
    if (MoveDataCheck(data))
    {
        return max(data.move_data->max_speed, data.move_data->max_speed *GetCurrHPFraction(data));
    }
    return 0.0f;
}

float GetCurrentAttackRate(TypeData & data)
{
    return GetBaseAttackRate(data.id) / GetCurrHPFraction(data);
}

int GetBulletDamage(node_type_id id)
{
    if (id == PLAYER_TYPE)
    {
        return 20;
    }
    else
    {
        return 25;
    }
}

shared_ptr<Type> GetTypeSharedPtr(TypeData & data)
{
    if (data.host_node_ptr)
    {
        Type * type_p = data.host_node_ptr->GetTypePtr();
        if (type_p)
        {
            return type_p->shared_from_this();
        }
    }
    return shared_ptr<Type>();
}

void FireBullet(TypeData & data, int bullet_damage)
{
    fec2 aim_pt = data.target_pos - data.world_pos;
    fec2 offset = data.world_pos + aim_pt.norm() * (data.host_node_ptr->radius * 2.f);
    State * bullet_state = new State(bind(bulletLaunchEnterB, _1, _2, aim_pt, GetCurrHPFraction(data) * GetBulletDamage(data.id) ), bind(bulletLaunchB, _1, _2, data.instance_id, aim_pt));
    SoundFX::play(ENEMY_SHOOT_SND);
    new Type(offset, BULLET_TYPE, bullet_state, &(data.id));// bind(bulletLaunchEnterB, _1, _2, data.target_pos), bind(bulletLaunchB, _1, _2, &data)))
}

State * healthFrameB(TypeData & data, float elapsed_s)
{
    if (data.has_hit_ptr != nullptr)
    {
        data.has_hit_ptr->effs.push_back(GetEffect(HEAL_EFFECT, *data.has_hit_ptr, 25));
        data.hp = 0;
    }
    return nullptr;
}

State* initStateFrameB(TypeData& data, float elapsed_s)
{
    if (data.host_node_ptr)
    {
        data.host_node_ptr->collider_type = GetColliderType(data.id);
    }
    return GetInitState(data.id);
}

void ClearContacts(TypeData & data)
{
    data.contact_list.clear();
}

void npcMoveUpdate(TypeData & data, float elapsed_s, float speed_scaling)
{
    if (MoveDataCheck(data))
    {
        data.move_data->nav.Compute(data, speed_scaling * GetCurrentMaxSpeedPerSecond(data) * elapsed_s);
        //data.move_data->path.debugDraw(*window_ptr);
        if (isnan(data.move_data->vel.x))
        {
            cout << "NaN found!" << endl;
        }
        data.world_pos += data.move_data->vel;
    }
}

State * GetAttackBehaviour(TypeData & data)
{
    State * ret_state_p = nullptr;
    switch (data.id)
    {
    case INTERRUPTOR_TYPE:
        /* Create ranged attack state */
        ret_state_p = new State(nullptr, bind(rangeAttackTargetFrameB, _1, _2));
        break;
    case DISINFECTOR_TYPE:
        ret_state_p = new State(nullptr, bind(closeAttackTargetFrameB, _1, _2));
        /* Create close range attack state */
        break;
    case GARBAGECOLLECTOR_TYPE:
        if(GetCurrHPFraction(data) > 2.f)
        {
            data.hp = GetMaxHitPoints(data.id);
            ret_state_p = new State(nullptr, bind(spawnAllies, _1, _2));
        }
        else
        {
            ret_state_p = new State(bind(collideEnterB, _1, _2), bind(collideFrameB, _1, _2));
        }
        break;
    case WATCHDOG_TYPE:
        /* Create spawn state */
        ret_state_p = new State(nullptr, bind(spawnAllies, _1, _2));
        break;
    default:
        break;

    }
    return ret_state_p;
}

State * portFrameProcess(TypeData & data, float elapsed_s, bool level_exit, fec2 partner_pos)
{

    for (auto contact : data.contact_list)
    {
        if (contact)
        {
            data.detected_ptr = contact->GetTypeDataPtr();
            if (GetTypeDetected(data, PLAYER_TYPE) && level_exit == false)
            {
                data.detected_ptr->world_pos = partner_pos;
            }
        }
    }

    return nullptr;
}

float GetScanInterval(TypeData & data)
{
    float ret_val = 0.4;
    switch (data.id)
    {
    case WATCHDOG_TYPE:
        ret_val = 0.5f;
        break;
    case DISINFECTOR_TYPE:
        ret_val = 0.3f;
        break;
    case INTERRUPTOR_TYPE:
        ret_val = 0.2f;
        break;
    case GARBAGECOLLECTOR_TYPE:
        ret_val = 0.6f;
        break;
    default:
        break;

    }
    return ret_val;
}
