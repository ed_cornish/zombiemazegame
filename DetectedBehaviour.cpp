#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"

class AlertOp : public GridOperator
{
    TypeData& alert_to;
public:
    void operator() (int x, int y)
    {
        //TODO: Get rid of this
    }
    void operator() (GridNode &cell)
    {
        for (auto &n : cell.node_v)
        {
            n.AlertTo(alert_to);
        }
    }
    AlertOp(TypeData& alert_to) : alert_to(alert_to)
    {
    }
};

State * detectedTarget(TypeData & data, float elapsed_s, unsigned int instance_id)
{
    unsigned int alert_grid_width = 5;// (unsigned int)(data.detect_range / GridNode::GetCellWidth() * 2.f);
    if (CheckDetectedInstance(data, instance_id))
    {
        data.target_pos = data.detected_ptr->world_pos;
        CastLOSRay(data);
        /* Must acquire a target continuously for a short time to avoid ray glitching */
        if (CheckDetectedInstance(data, instance_id))//CheckTimerExpire(data, elapsed_s, 0.01f))
        {
            /* Alert nearby nodes based on the detection range */
            AlertOp alert(*data.detected_ptr);
            GridNode::WalkSpiral(data.world_pos, alert_grid_width, alert);
            /* Move to the node's attack state */
            return GetAttackBehaviour(data);
        }
    }
    /* No valid ptr detected, back to idle */
    return new State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, ENEMY_SCAN_INTERVAL));
}
