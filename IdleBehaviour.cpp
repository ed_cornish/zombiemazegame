#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"


void idleEnter(TypeData & data, float elapsed_s)
{
    if (MoveDataCheck(data))
    {
        //Monster should wander aimlessly while still keeping away from walls
        data.move_data->nav.weights = monsterIdleWeights;
        data.move_data->goal_pos = data.world_pos;
        data.move_data->nav.ResetWander();
    }
    data.timer = 0.f;
    data.detected_ptr = nullptr;
}

State * idleWander(TypeData & data, float elapsed_s, float scan_interval_s)
{
    if (CheckTimerExpire(data, elapsed_s, scan_interval_s))
    {
        return new State(nullptr, bind(scanFrameB, _1, _2, GetScanDegsPerSec(data.id), GetPreferredTarget(data)));
    }
    if (data.detected_ptr && CheckDetectedTypeId(data, GetPreferredTarget(data)))
    {
        /* We have been alerted to something, hunt for it */
        return new State(bind(huntEnter, _1, _2, data.detected_ptr->world_pos), bind(huntTarget, _1, _2, 4.f));
    }
    npcMoveUpdate(data, elapsed_s, 0.3f);
    return nullptr;
}
