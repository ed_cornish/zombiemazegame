#include "CollisionShapes.h"
#include "GFXMgr.h"

bool flpts_rough_equivalence(fec2 a, fec2 b, float epsilon)
{
    if (fec2(a - b).mag() < epsilon)
    {
        return true;
    }

    return false;
}
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ---- FEC2 functions ---- */
//Ctor and Dtor inline in h file
//Dot (scalar) product between two vectors
float fec2::dot(fec2 rhs)
{
    return (x * rhs.x) + (y*rhs.y);
}

//Vector magnitude
float fec2::mag()
{
    return sqrtf(x*x + y*y);
}

//Normalised vector
fec2 fec2::norm()
{
    if (mag() == 0)
    {
        return fec2(0, 0);
    }
    return (*this / mag());
}

void fec2::rotate(float angle_in_rads)
{
    float cs = cosf(angle_in_rads);
    float sn = sinf(angle_in_rads);

    float new_x = x * cs - y * sn;
    float new_y = x * sn + y * cs;
    x = new_x;
    y = new_y;
}

fec2 fec2::perp()
{
    return fec2(y, -x);
}

fec2::~fec2()
{
}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ---- CIRCLE functions ---- */
//Ctor and Dtor inline in h file
//Draw the circle collider to the specified render window
void circle::draw_me()
{
    render_shape.setPosition(centre);
    if (debug_render)
    {
        GFXMgr::PushColliderShape(render_shape);
    }
}

//Test for overlap between two circles, and update a vector with the seperation of the radii
bool circle::overlap(circle rhs, Vector2f &seperation)
{
    fec2 delta = centre - rhs.centre;

    float radius_sum = radius + rhs.radius;

    float overlap_distance = radius_sum - delta.mag();

    if (overlap_distance < 0)
    {
        seperation = delta;
        return false;
    }
    else
    {
        seperation = delta.norm() * overlap_distance;
        render_shape.setOutlineColor(Color::Green);
        return true;
    }
}

//Override the debug draw colour
void circle::SetDrawColour(Color draw_color)
{
    render_shape.setOutlineColor(draw_color);
}

void edge::RecalcLine()
{
    slope = (end.y - start.y) / (end.x - start.x);
    if (!isnan(slope))
    {
        yintercept = start.y - slope * start.x;
    }
    else
    {
        yintercept = nanf("");
    }
    length = fec2(end - start).mag();
}
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ---- EDGE functions ---- */
//Ctor and Dtor inline in h file
fec2 edge::getstart()
{
    return start;
}

fec2 edge::getend()
{
    return end;
}

//Get a vector perpendicular to the edge (up wrt to edge)
fec2 edge::perp()
{
    return fec2((end.y - start.y), -(end.x - start.x));
}

//Test for overlap of a circle and the edge
bool edge::overlap_circle(circle rhs, fec2 &seperation)
{
    fec2 near_point = nearest_point(rhs.centre);
    float dist = fec2(near_point - rhs.centre).mag();
    render_vertexes[0].color = Color::Red;
    render_vertexes[1].color = Color::Red;
    seperation = (rhs.radius - dist) * perp().norm();
    if (dist < rhs.radius)
    {
        //Color the edge green if an overlap is found
        render_vertexes[0].color = Color::Green;
        render_vertexes[1].color = Color::Green;
        return true;
    }
    else
    {
        return false;
    }
}

bool edge::overlap_circle_np(circle rhs, fec2 & near_point)
{
    near_point = nearest_point(rhs.centre);
    float dist = fec2(near_point - rhs.centre).mag();
    render_vertexes[0].color = Color::Red;
    render_vertexes[1].color = Color::Red;
    if (dist < rhs.radius)
    {
        //Color the edge green if an overlap is found
        render_vertexes[0].color = Color::Green;
        render_vertexes[1].color = Color::Green;
        return true;
    }
    else
    {
        return false;
    }
}

//Version of the overlap test where we do not care about the seperation
bool edge::overlap_circle(circle rhs)
{
    fec2 near_point = nearest_point(rhs.centre);
    float dist = fec2(near_point - rhs.centre).mag();
    render_vertexes[0].color = Color::Red;
    render_vertexes[1].color = Color::Red;
    if (dist < rhs.radius)
    {
        //Color the edge green if an overlap is found
        render_vertexes[0].color = Color::Green;
        render_vertexes[1].color = Color::Green;
        return true;
    }
    else
    {
        return false;
    }
}

//Perform a coarse check to see if a point is on the line (0.01 precision)
bool edge::point_on_line(fec2 point)
{
    float fudge_f = 0.05f;
    //fec2 np = nearest_point(point);
    if (isinf(slope))
    {
        //Case where start is < end
        if (point.y > start.y && point.y < end.y)
        {
            if (flpts_rough_equivalence(point.x, start.x, fudge_f))
            {
                return true;
            }
        }
        else if (point.y < start.y && point.y > end.y)
        {
            //Case where start > end
            if (flpts_rough_equivalence(point.x, start.x, fudge_f))
            {
                return true;
            }
        }//Case where point == start or end
        else if ((flpts_rough_equivalence(point.y, start.y, fudge_f)) || (flpts_rough_equivalence(point.y, end.y, fudge_f)))
        {
            if (flpts_rough_equivalence(point.x, start.x, fudge_f))
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        fec2 eval_pt = (slope * point.x + yintercept);
        if (point.x > start.x && point.x < end.x)
        {
            if (flpts_rough_equivalence(point.y, eval_pt, fudge_f))
            {
                return true;
            }
        }
        else if (point.x < start.x && point.x > end.x)
        {
            if (flpts_rough_equivalence(point.y, eval_pt, fudge_f))
            {
                return true;
            }
        }
        else if ((flpts_rough_equivalence(point.x, start.x, fudge_f)) && (flpts_rough_equivalence(point.x, end.x, fudge_f)))
        {
            if (flpts_rough_equivalence(point.y, eval_pt, fudge_f))
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    /*
    fec2 dist = np - point;
    if (dist.mag() <0.01)
    {
    return true;
    }
    return false;*/
    return false;
}

//Find the nearest point on the line to another point
fec2 edge::nearest_point(fec2 point)
{
    fec2 start_to_point = point - start;
    

    float scalar_product = (start_to_point.dot(end - start) / (mag()*mag()));

    fec2 np = start + ((end - start) * scalar_product);

    if (fec2(end - start).mag() < fec2(np - start).mag())
    {
        np = end;
    }
    if (fec2(start - end).mag() < fec2(np - end).mag())
    {
        np = start;
    }
    return np;
}

float edge::mag()
{
    return length;
}

//Render the edge to the provided render target
void edge::draw_me(RenderWindow &window)
{
    if (debug_render)
    {
        window.draw(render_vertexes);
        render_vertexes[0].color = Color::Red;
        render_vertexes[1].color = Color::Red;
    }
}

//Determine if a point lies above or below the line (start is assumed to be left-most point by convention - order is important!)
bool edge::above_line(fec2 point)
{
    fec2 near_point = nearest_point(point);
    if (acosf(perp().dot(fec2(point - near_point)) > (MY_PI / 2)))
    {
        return false;
    }
    return true;
}

//Determine if two edges intersect, accounting for their length (all non-parallel lines will intersect somewhere)
bool edge::intersect(edge & rhs, fec2 & intersect_point)
{
    //cout << "Slope: " << slope << " yintercept: " << yintercept << endl;
    //cout << "RHS Slope: " << rhs.slope << " RHS yintercept: " << rhs.yintercept << endl;
    fec2 temp_point;
    if (slope == rhs.slope || (isinf(slope) && isinf(rhs.slope)))
    {
        //Lines are parallel, they will only intersect if they are on top of each other (there will be a range of intersections, return only one point)
        if (point_on_line(rhs.start))
        {
            intersect_point = rhs.start;
            return true;
        }
        else if (point_on_line(rhs.end))
        {
            intersect_point = rhs.end;
            return true;
        }
    }
    else //Lines MUST intersect somewhere in space
    {
        if (isinf(slope))
        {
            //this line is vertical, and since slopes are not equal then rhs will intersect at the point where rhs.x == start.x
            temp_point.x = start.x;
            temp_point.y = rhs.slope * temp_point.x + rhs.yintercept;
        }
        else if (isinf(rhs.slope))
        {
            //rhs line is vertical, and since slopes are not equal then this will intersect at the point where rhs.start.x == this.x
            temp_point.x = rhs.start.x;
            temp_point.y = (slope * temp_point.x) + yintercept;
        }
        else //Neither line is vertical
        {
            temp_point.x = (rhs.yintercept - yintercept) / (slope - rhs.slope);
            temp_point.y = (slope * temp_point.x) + yintercept;
            //cout << "Intersect check x: " << temp_point.x << ", y:" << temp_point.y << endl;
        }
        if (point_on_line(temp_point) && rhs.point_on_line(temp_point))
        {
            intersect_point = temp_point;
            //cout << "Point is on line" << endl;
            return true;
        }
        //else
        //{
        //    cout << "No intersect" << endl;
        //}
    }
    return false;
}

void edge::rotate(float angle_in_degrees)
{
    fec2 vector = end - start;
    fec2 new_vector;
    float angle_in_radians = angle_in_degrees * MY_PI/180;

    new_vector.x = vector.x * cosf(angle_in_radians) - vector.y * sinf(angle_in_radians);
    new_vector.y = vector.x * sinf(angle_in_radians) + vector.y * cosf(angle_in_radians);
    end = start + new_vector;
}

void edge::setrender(bool render_flag)
{
    debug_render = render_flag;
}

bool edge::getrender()
{
    return debug_render;
}

void edge::extend(float amount)
{
    end += (end - start) * amount;

}
