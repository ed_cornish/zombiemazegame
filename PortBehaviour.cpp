#include "Behaviours.h"
#include "Type.h"
#include "MapLoader.h"
#include "EntityData.h"

State * portExitProcess(TypeData & data, float elapsed_s)
{
    LevelList level_access_point;
    data.hp = GetMaxHitPoints(data.id);
    for (auto& contact : data.contact_list)
    {
        TypeData * contact_data_ptr = contact->GetTypeDataPtr();
        if (contact_data_ptr && contact_data_ptr->id == PLAYER_TYPE)
        {
            level_access_point.markLevelComplete();
            return nullptr;
        }
    }
    data.contact_list.clear();

    return nullptr;
}
