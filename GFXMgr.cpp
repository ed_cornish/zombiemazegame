#include "GFXMgr.h"
#include "GridNode.h"
#include "EntityData.h"
#include "Thor/Math.hpp"
#include "Thor/Animations.hpp"
#include "SoundFX.h"
#include "Shadow.h"
#include "LevelData.h"

vector<shared_ptr<Type>> GFXMgr::chars_v;

VertexArray GFXMgr::level_geometry;

Texture * GFXMgr::level_texture_map_p = nullptr;
float GFXMgr::frame_interval = 0.f;
VertexArray GFXMgr::ray_debug_vertexes;
vector<CircleShape> GFXMgr::collider_debug_circles;
vector<Text>        GFXMgr::glyphs;
thor::ParticleSystem GFXMgr::part_mgr;
thor::ParticleSystem GFXMgr::bg_part_mgr;
thor::ParticleSystem GFXMgr::wall_tex_part_mgr;
//RenderTexture GFXMgr::wall_texture;
//RenderTexture GFXMgr::bg_texture;
bool GFXMgr::show_debug_info = true;
int GFXMgr::FPS_lock = 60;

void GFXMgr::InitParticleEmitters()
{
    static bool emit_init = false;

    if (emit_init == false)
    {
        /* Get the grid rect size to use for reference */
        Vector2f half_sz = Vector2f(GridNode::GetGridRect().width / 2.f, GridNode::GetGridRect().height / 2.f);

        /* Set up the background texture and emitter */
        cout << "Creating BG texture " << bg_texture.create(GridNode::GetGridRect().width, GridNode::GetGridRect().height) << endl;
        thor::UniversalEmitter bg_emitter;
        bg_emitter.setEmissionRate(50);
        bg_emitter.setParticleColor(Color(0, 0, 95, 100));
        bg_emitter.setParticleLifetime(thor::Distributions::uniform(seconds(2.f), seconds(5.f)));
        bg_emitter.setParticleScale(thor::Distributions::rect(Vector2f(1.f, 1.f), Vector2f(0.7f, 0.7f)));
        bg_emitter.setParticlePosition(thor::Distributions::rect(half_sz, half_sz));
        bg_part_mgr.addEmitter(bg_emitter);

        /* Set up the wall texture and emitter */
        cout << "Creating Wall texture " << wall_texture.create(half_sz.x * 2, half_sz.y * 2) << endl;
        thor::UniversalEmitter wall_emitter;
        wall_emitter.setEmissionRate(150);
        wall_emitter.setParticleColor(Color(0, 120, 255, 150));
        wall_emitter.setParticleLifetime(thor::Distributions::uniform(seconds(0.5f), seconds(1.2f)));
        wall_emitter.setParticleScale(thor::Distributions::rect(Vector2f(1.f, 1.f), Vector2f(0.1f, 0.1f)));
        wall_emitter.setParticlePosition(thor::Distributions::rect(half_sz, half_sz));
        wall_tex_part_mgr.addEmitter(wall_emitter);

        /* Only run this once */
        emit_init = true;
        cout << endl;
    }

}

void GFXMgr::UpdateParticles(RenderWindow & window, float elapsed_s)
{
    /* Update and render particles */
    Sprite bg;

    /* Clear render textures*/
    bg_texture.clear();
    wall_texture.clear(Color(0, 20, 170, 255));

    /* Update particle managers */
    bg_part_mgr.update(seconds(elapsed_s));
    wall_tex_part_mgr.update(seconds(elapsed_s));

    /* Render particles to the textures */
    bg_texture.draw(bg_part_mgr);
    wall_texture.draw(wall_tex_part_mgr);

    /* Render the background sprite */
    bg.setTexture(bg_texture.getTexture());
    bg.setPosition(fec2());
    window.draw(bg);

    /* Render the wall geometry */
    window.draw(level_geometry, RenderStates(&wall_texture.getTexture()));

    /* Update the particles in the world */
    part_mgr.update(seconds(elapsed_s));
    window.draw(part_mgr);
}

GFXMgr::GFXMgr(int fps_lock, bool debug_mode)
{
    FPS_lock = fps_lock;
    show_debug_info = debug_mode;
}

GFXMgr::~GFXMgr()
{
}

void GFXMgr::Frame(RenderWindow & window)
{
    bool render_now = false;
    Text FPS;
    FPS.setString(GridNode::FrameTick());
    float elapsed_s = Type::GetSecsElapsed();
    frame_interval += elapsed_s;
    /* Are we locking the frame rate? */
    if (FPS_lock > 0)
    {
        /* Has enought time elapsed to tick the renderer? */
        if (frame_interval > (float)(1 / FPS_lock))
        {
            /* Render a frame and reset the interval */
            render_now = true;
            elapsed_s = frame_interval;
            frame_interval = 0.f;
        }
    }
    else
    {
        /* FPS lock disabled, render as fast as possible */
        render_now = true;
    }

    SoundFX::purge(elapsed_s);

    InitParticleEmitters();

    if (render_now)
    {
        window.clear();

        View curr_view = window.getView();

        curr_view.setCenter(Type::GetPlayerPos());

        window.setView(curr_view);

        UpdateParticles(window, elapsed_s);

        Text player_hp;
        Vector2f top_left_of_window = (window.getView().getCenter() - window.getView().getSize() / 2.f);
        bool show_hp = false;

        /* Render characters */
        for (auto & c : chars_v)
        {
            c->Render(window, show_debug_info);
            if (c->isPlayer())
            {
                ostringstream conversion_stream;
                conversion_stream << "STACK: " << c->GetHP() << endl;
                player_hp.setString(conversion_stream.str());
                player_hp.setPosition(top_left_of_window);
                if (font_p)
                {
                    player_hp.setFont(*font_p);
                    show_hp = true;
                }
            }
        }

        cursor_sprite.setPosition(top_left_of_window + Vector2f(Mouse::getPosition(window)));

        //Remove any dead chars from the renderlist
        chars_v.erase(remove_if(chars_v.begin(), chars_v.end(), [](shared_ptr<Type> & a_char_sp) { return a_char_sp->isDead(); }), chars_v.end());

        for (auto & g : glyphs)
        {
            window.draw(g);
        }

        Shadow::Render(window);
        if (show_debug_info)
        {
            ray_debug_vertexes.setPrimitiveType(Lines);
            window.draw(ray_debug_vertexes);
            for (auto & c : collider_debug_circles)
            {
                window.draw(c);
            }
        }
        if (ray_debug_vertexes.getVertexCount() > 0)
        {
            ray_debug_vertexes.clear();
        }
        if (collider_debug_circles.size() > 0)
        {
            collider_debug_circles.clear();
        }
        window.draw(cursor_sprite);

        if (show_hp)
        {
            window.draw(player_hp);
        }

        //TODO: Remove the FPS clock in non-debug mode
        if (1)//show_debug_info && font_p)
        {
            FPS.setPosition(top_left_of_window + Vector2f(0.f, window.getSize().y * 0.9f));
            FPS.setFont(*font_p);
            FPS.setScale(0.5f, 0.5f);
            window.draw(FPS);
        }

        window.display();

        if (show_debug_info)
        {
            cout << flush;
        }
    }
}

void GFXMgr::RegisterChar(Type & char_ref)
{
    chars_v.push_back(char_ref.shared_from_this());
}

void GFXMgr::SetLevelGeometry(VertexArray & geometry)
{
    level_geometry = geometry;
}

void GFXMgr::SetTextureMap(Texture & new_texture)
{
    level_texture_map_p = &new_texture;
}

thor::Connection GFXMgr::RegisterEmitter(thor::UniversalEmitter &  em_ref)
{
    return part_mgr.addEmitter(thor::refEmitter(em_ref));
}

void GFXMgr::RegisterExplosionEmitter(thor::UniversalEmitter & em_ref, float lifetime_s)
{
    part_mgr.addEmitter(em_ref, seconds(lifetime_s));
}

void GFXMgr::SetupReticule(Texture & reticule_texture)
{
    Vector2f size;
    cursor_sprite = Sprite(reticule_texture);
    size = Vector2f(cursor_sprite.getLocalBounds().width, cursor_sprite.getLocalBounds().height);
    cursor_sprite.setOrigin(size / 2.f);
}

void GFXMgr::LevelReset()
{
    level_geometry.clear();
    chars_v.clear();
    glyphs.clear();
    collider_debug_circles.clear();
    part_mgr.clearEmitters();
    part_mgr.clearAffectors();
    part_mgr.clearParticles();
}

void GFXMgr::SetupGUIFont(Font & font)
{
    font_p = &font;
}

void GFXMgr::PushRayVertexes(VertexArray & ray_vertexes)
{
    for (auto i = 0; i < ray_vertexes.getVertexCount(); i++)
    {
        ray_debug_vertexes.append(ray_vertexes[i]);
    }
}

void GFXMgr::PushColliderShape(CircleShape & shape)
{
    collider_debug_circles.push_back(shape);
}

void GFXMgr::InitParticleSys(Texture & particle_texture)
{
    part_mgr.setTexture(particle_texture);
    bg_part_mgr.setTexture(particle_texture);
    wall_tex_part_mgr.setTexture(particle_texture);
    thor::FadeAnimation fade_anim(0.f, 0.9f);
    thor::FadeAnimation bg_fade_anim(0.5f, 0.5f);
    part_mgr.addAffector(thor::AnimationAffector(fade_anim));
    bg_part_mgr.addAffector(thor::AnimationAffector(bg_fade_anim));
    wall_tex_part_mgr.addAffector(thor::AnimationAffector(bg_fade_anim));
}

void GFXMgr::ToggleDebugShow()
{
    show_debug_info = !show_debug_info;
}

bool GFXMgr::GetDebugState()
{
    return show_debug_info;
}

void GFXMgr::AddGlyph(glyphData data, const Font & glyph_font)
{
    int x = (int)data.x;
    int y = (int)data.y;
    if(GridNode::CheckOnGrid(x, y))
    {
        Text txt(data.letter, glyph_font, 20);
        txt.setOrigin(txt.getLocalBounds().width/2, txt.getLocalBounds().height/2);
        txt.setPosition(GridNode::GetCell(data.x, data.y).GetCellCentre());
        txt.setColor(Color(60.f, 60.f, 255.f, 255.f));
        glyphs.push_back(txt);
    }
}
