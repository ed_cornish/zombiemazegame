#include "Explosion.h"
#include "Thor/Math.hpp"
#include "Thor/Input.hpp"
#define BASE_EMIT 130
#define BASE_VEL 350.f
#define BASE_SIZE Vector2f(0.7f, 0.7f)
#define BASE_LIFETIME_MAX 0.5f
#define BASE_LIFETIME_MIN 0.2f

Color RandomColour()
{
    Color ret_colour;
    thor::Distribution<unsigned int> rgb_dist = thor::Distributions::uniform(0, 255);

    ret_colour.r = rgb_dist();
    ret_colour.g = rgb_dist();
    ret_colour.b = rgb_dist();
    ret_colour.a = 130;
    return ret_colour;
}

Explosion::Explosion(fec2 pos, float scale, Color * colour_p) : scale(scale)
{
    thor::UniversalEmitter em;
    em.setEmissionRate(BASE_EMIT * scale);
    em.setParticleLifetime(thor::Distributions::uniform(seconds(BASE_LIFETIME_MIN), seconds(BASE_LIFETIME_MAX)));
    em.setParticleVelocity(thor::Distributions::deflect(BASE_SIZE * BASE_VEL, 360.f));
    em.setParticlePosition(pos);
    if (colour_p)
    {
        colour_p->a = 130;
        em.setParticleColor(*colour_p);
    }
    else
    {
        em.setParticleColor(thor::Distribution<Color>(RandomColour));
    }
    em.setParticleScale(thor::Distributions::rect(BASE_SIZE, fec2(0.3f, 0.3f) * scale));
    GFXMgr::RegisterExplosionEmitter(em, 0.1f);
    //emit_conn.reset(new thor::ScopedConnection(GFXMgr::RegisterEmitter(em)));
}

Explosion::~Explosion()
{
}
