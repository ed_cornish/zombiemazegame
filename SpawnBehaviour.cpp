#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"
#include "Thor/Math.hpp"


State * spawnAllies(TypeData & data, float elapsed_s)
{
    data.timer = 0.f;

    if (data.detected_ptr)
    {
        node_type_id spawn_id;
        State * spawn_state_p = new State(bind(huntEnter, _1, _2, data.detected_ptr->world_pos), bind(huntTarget, _1, _2, 5.f));
        thor::Distribution<Vector2f> offset = thor::Distributions::rect(data.world_pos, fec2(15.f, 15.f));
        if (data.id == WATCHDOG_TYPE)
        {
            if (data.detected_ptr->id == PLAYER_TYPE)
            {
                if (rand() % 2)
                {
                    spawn_id = DISINFECTOR_TYPE;
                }
                else
                {
                    spawn_id = INTERRUPTOR_TYPE;
                }
            }
            else if (data.detected_ptr->id == FRAGMENT_TYPE)
            {
                spawn_id = GARBAGECOLLECTOR_TYPE;
            }
        }
        else
        {
            spawn_id = DISINFECTOR_TYPE;
        }
      

        new Type(offset(), spawn_id, spawn_state_p);

        /* Enter cooldown */
        return new State(nullptr, bind(cooldownState, _1, _2, 3.f));
    }
    /* Couldn't find a detected target, do a scan */
    return new State(nullptr, bind(scanFrameB, _1, _2, GetScanDegsPerSec(data.id), GetPreferredTarget(data)));

}
