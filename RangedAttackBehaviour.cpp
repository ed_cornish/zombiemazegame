#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"

State * rangeAttackTargetFrameB(TypeData & data, float elapsed_s)
{
    CastLOSRay(data);

    if (GetTypeDetected(data, PLAYER_TYPE))
    {
        data.target_pos = data.detected_ptr->world_pos;
        data.move_data->goal_pos = data.target_pos;
        /* Has LOS to player */
        if (CheckTimerExpire(data, elapsed_s, GetCurrentAttackRate(data)))
        {
            FireBullet(data, GetBulletDamage(data.id));
        }
        else if (flpts_rough_equivalence(data.world_pos, data.detected_ptr->world_pos, data.detect_range * 0.6f))
        {
            /* Flee if the player gets too close */
            data.move_data->nav.weights = monsterFleeingWeights;
        }
        else
        {
            data.move_data->nav.weights = monsterAttackingWeights;
        }
    }
    else if (CheckTimerExpire(data, elapsed_s, GetBaseAttackRate(data.id)))
    {
        //Go to hunt target state 
        return new State(bind(huntEnter, _1, _2, data.target_pos), bind(huntTarget, _1, _2, 5.f));
    }
    FollowPath(data);
    npcMoveUpdate(data, elapsed_s);

    return nullptr;
}
