#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/5c0/Behaviours.o \
	${OBJECTDIR}/_ext/5c0/BulletBehaviour.o \
	${OBJECTDIR}/_ext/5c0/CloseAttackBehaviour.o \
	${OBJECTDIR}/_ext/5c0/CollisionShapes.o \
	${OBJECTDIR}/_ext/5c0/CooldownBehaviour.o \
	${OBJECTDIR}/_ext/5c0/DetectedBehaviour.o \
	${OBJECTDIR}/_ext/5c0/Effect.o \
	${OBJECTDIR}/_ext/5c0/EntityData.o \
	${OBJECTDIR}/_ext/5c0/Explosion.o \
	${OBJECTDIR}/_ext/5c0/GFXMgr.o \
	${OBJECTDIR}/_ext/5c0/GridNode.o \
	${OBJECTDIR}/_ext/5c0/HuntBehaviour.o \
	${OBJECTDIR}/_ext/5c0/IdleBehaviour.o \
	${OBJECTDIR}/_ext/5c0/MapLoader.o \
	${OBJECTDIR}/_ext/5c0/Maze.o \
	${OBJECTDIR}/_ext/5c0/MovementData.o \
	${OBJECTDIR}/_ext/5c0/Node.o \
	${OBJECTDIR}/_ext/5c0/PlayerBehaviour.o \
	${OBJECTDIR}/_ext/5c0/PortBehaviour.o \
	${OBJECTDIR}/_ext/5c0/RangedAttackBehaviour.o \
	${OBJECTDIR}/_ext/5c0/Ray.o \
	${OBJECTDIR}/_ext/5c0/ScanBehaviour.o \
	${OBJECTDIR}/_ext/5c0/Shadow.o \
	${OBJECTDIR}/_ext/5c0/SpawnBehaviour.o \
	${OBJECTDIR}/_ext/5c0/StateMachine.o \
	${OBJECTDIR}/_ext/5c0/Steering.o \
	${OBJECTDIR}/_ext/5c0/Type.o \
	${OBJECTDIR}/_ext/5c0/main.o \
	${OBJECTDIR}/_ext/5c0/micropather.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/dungeongame

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/dungeongame: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/dungeongame ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/5c0/Behaviours.o: ../Behaviours.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Behaviours.o ../Behaviours.cpp

${OBJECTDIR}/_ext/5c0/BulletBehaviour.o: ../BulletBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/BulletBehaviour.o ../BulletBehaviour.cpp

${OBJECTDIR}/_ext/5c0/CloseAttackBehaviour.o: ../CloseAttackBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/CloseAttackBehaviour.o ../CloseAttackBehaviour.cpp

${OBJECTDIR}/_ext/5c0/CollisionShapes.o: ../CollisionShapes.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/CollisionShapes.o ../CollisionShapes.cpp

${OBJECTDIR}/_ext/5c0/CooldownBehaviour.o: ../CooldownBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/CooldownBehaviour.o ../CooldownBehaviour.cpp

${OBJECTDIR}/_ext/5c0/DetectedBehaviour.o: ../DetectedBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/DetectedBehaviour.o ../DetectedBehaviour.cpp

${OBJECTDIR}/_ext/5c0/Effect.o: ../Effect.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Effect.o ../Effect.cpp

${OBJECTDIR}/_ext/5c0/EntityData.o: ../EntityData.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/EntityData.o ../EntityData.cpp

${OBJECTDIR}/_ext/5c0/Explosion.o: ../Explosion.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Explosion.o ../Explosion.cpp

${OBJECTDIR}/_ext/5c0/GFXMgr.o: ../GFXMgr.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/GFXMgr.o ../GFXMgr.cpp

${OBJECTDIR}/_ext/5c0/GridNode.o: ../GridNode.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/GridNode.o ../GridNode.cpp

${OBJECTDIR}/_ext/5c0/HuntBehaviour.o: ../HuntBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/HuntBehaviour.o ../HuntBehaviour.cpp

${OBJECTDIR}/_ext/5c0/IdleBehaviour.o: ../IdleBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/IdleBehaviour.o ../IdleBehaviour.cpp

${OBJECTDIR}/_ext/5c0/MapLoader.o: ../MapLoader.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/MapLoader.o ../MapLoader.cpp

${OBJECTDIR}/_ext/5c0/Maze.o: ../Maze.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Maze.o ../Maze.cpp

${OBJECTDIR}/_ext/5c0/MovementData.o: ../MovementData.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/MovementData.o ../MovementData.cpp

${OBJECTDIR}/_ext/5c0/Node.o: ../Node.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Node.o ../Node.cpp

${OBJECTDIR}/_ext/5c0/PlayerBehaviour.o: ../PlayerBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/PlayerBehaviour.o ../PlayerBehaviour.cpp

${OBJECTDIR}/_ext/5c0/PortBehaviour.o: ../PortBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/PortBehaviour.o ../PortBehaviour.cpp

${OBJECTDIR}/_ext/5c0/RangedAttackBehaviour.o: ../RangedAttackBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/RangedAttackBehaviour.o ../RangedAttackBehaviour.cpp

${OBJECTDIR}/_ext/5c0/Ray.o: ../Ray.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Ray.o ../Ray.cpp

${OBJECTDIR}/_ext/5c0/ScanBehaviour.o: ../ScanBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/ScanBehaviour.o ../ScanBehaviour.cpp

${OBJECTDIR}/_ext/5c0/Shadow.o: ../Shadow.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Shadow.o ../Shadow.cpp

${OBJECTDIR}/_ext/5c0/SpawnBehaviour.o: ../SpawnBehaviour.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/SpawnBehaviour.o ../SpawnBehaviour.cpp

${OBJECTDIR}/_ext/5c0/StateMachine.o: ../StateMachine.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/StateMachine.o ../StateMachine.cpp

${OBJECTDIR}/_ext/5c0/Steering.o: ../Steering.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Steering.o ../Steering.cpp

${OBJECTDIR}/_ext/5c0/Type.o: ../Type.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/Type.o ../Type.cpp

${OBJECTDIR}/_ext/5c0/main.o: ../main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/main.o ../main.cpp

${OBJECTDIR}/_ext/5c0/micropather.o: ../micropather.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5c0/micropather.o ../micropather.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/dungeongame

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
