#include "Steering.h"

#include "GridNode.h"
#include "Type.h"


/* Vector to target */
void Steering::ComputePursue(TypeData & data)
{
    vectors.pursue = (data.target_pos - data.world_pos) * weights.pursue;
}
/* Vector away from target */
void Steering::ComputeFlee(TypeData & data)
{
    vectors.flee = (data.world_pos - data.target_pos) * weights.flee;
}
/* Vector to flank target */
void Steering::ComputeFlank(TypeData & data)
{
    vectors.flank = fec2();// data.vel.perp() * weights.flank;
}
/* Vector to meander on course */
void Steering::ComputeWander(TypeData & data)
{
    //vectors.wander = fec2(data.vel + fec2((float)(rand() % 10)-5.f, (float)(rand() % 10)-5.f)) * weights.wander;

}
/* Vector to avoid walls */
void Steering::ComputeNavigate(TypeData & data)
{
    vectors.navigate = (data.move_data->goal_pos - data.world_pos) * weights.navigate;
}

void Steering::ComputeAvoid(TypeData & data)
{
    float cell_width = GridNode::GetCellWidth();
    GridNode &cell = GridNode::GetCell(data.world_pos);
    GridNode &corner_cell = cell;
    fec2 wall_vec = fec2(0.f, 0.f);
    for (auto &wall : cell.walls)
    {
        fec2 dist = fec2(data.world_pos - wall.nearest_point(data.world_pos));
        //float dot_prod = wall.perp().dot(dist);
        wall_vec += wall.perp() * (1.f / (dist.mag() + 1));
    }

    vectors.avoid = wall_vec * weights.avoid;
}


Steering::Steering()
{
    /* Set some default weights */
    weights.pursue = 0.f;
    weights.flee = 0.f;
    weights.flank = 0.f;
    weights.wander = 0.f;
    weights.navigate = 1.f;
    weights.avoid = 1.f;
    weights.damping = 0.15f;
}

/* Construct a Steering object from a given set of weights */
Steering::Steering(SteeringWeights weights) : weights(weights)
{
}


Steering::~Steering()
{
}

/* Calculate the resultant of the various steering vectors and clamp to the max length given */
void Steering::Compute(TypeData & data, float clamp_max)
{
    MoverData* move_data_ptr = (data.move_data.get());
    if (move_data_ptr)
    {
        fec2 desired;
        ComputePursue(data);
        ComputeFlee(data);
        ComputeFlank(data);
        //ComputeWander(data);
        ComputeNavigate(data);
        ComputeAvoid(data);
        desired = (vectors.pursue + vectors.flee + vectors.flank + vectors.wander + vectors.avoid + vectors.navigate);
        desired = desired * weights.damping;
        data.move_data->vel += desired;// -data.vel;
                                       //data.vel += data.vel + vectors.wander;
        if (data.move_data->vel.mag() > clamp_max)
        {
            data.move_data->vel = data.move_data->vel.norm() * clamp_max;
        }
    }
}

void Steering::ResetWander()
{
    vectors.wander += fec2((float)((rand() % 10))-5.f, (float)((rand() % 10) - 5.f)) * weights.wander;
    //cout << "Wander vector - x: " << vectors.wander.x << ", y: " << vectors.wander.y << endl;
    if (vectors.wander.mag() > 10.f)
    {
        /* Reset the wander vector if it gets excessively large*/
        //cout << "Resetting wander" << endl;
        vectors.wander = fec2();
    }
}

Path::Path()
{
    path_idx = 0;
}

vector<fec2>& Path::reset()
{
    wp.clear();
    path_idx = 0;
    return wp;
}

fec2 Path::getCurrentWP(fec2 curr_pos)
{
    //cout << wp.size() << " nodes in path" << endl;
    fec2 ret_pos;
    float dist_to_wp;

    if (path_idx >= wp.size())
    {
        /* The path_idx is > the wp vec size, reset the path and abort */
        /* We may have reached the end of the path */
        reset();
        ret_pos = curr_pos;
    }
    else
    {
        dist_to_wp = fec2(wp[path_idx] - curr_pos).mag();

        if (dist_to_wp < (GridNode::GetCellWidth() / 2.f))
        {
            path_idx++;
            ret_pos = curr_pos;
        }
        else
        {
            ret_pos = wp[path_idx];
        }
    }
    
    return ret_pos;
}

void Path::debugDraw(RenderWindow & window)
{
    CircleShape waypoint = CircleShape(15.f, 12);
    waypoint.setOutlineColor(Color(0, 255, 0, 80));
    waypoint.setOutlineThickness(3.f);
    waypoint.setOrigin(fec2(15.f, 15.f));
    for (auto node : wp)
    {
        waypoint.setPosition(node);
        if (path_idx == 0)
        {
            waypoint.setFillColor(Color(255, 0, 0, 80));
        }
        else
        {
            waypoint.setFillColor(Color::Transparent);
        }
        window.draw(waypoint);
    }
    waypoint.setRadius(5.f);
    waypoint.setFillColor(Color::Blue);
    if (path_idx < wp.size())
    {
        waypoint.setPosition(wp[path_idx]);
        window.draw(waypoint);
    }
}
