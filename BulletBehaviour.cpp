#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"
const SteeringWeights bulletTravelWeights =
{
    0.2f, //pursue
    0.f, //flee
    0.f, //flank
    0.f, //wander
    1.f, //navigate
    0.f, //avoid
    0.5f, //damping
};
State * bulletLaunchB(TypeData & data, float elapsed_s, unsigned int launcher_instance, fec2 aim_point)
{
    for (auto contact : data.contact_list)
    {
        TypeData * contact_data_ptr = contact->GetTypeDataPtr();
        if (contact_data_ptr &&  
            contact_data_ptr->instance_id != launcher_instance && 
            contact_data_ptr->instance_id != data.instance_id &&
            !contact->IsTypeID(FRAGMENT_TYPE))
        {
            contact_data_ptr->hp -= data.hp;
            data.hp = 0;
            GAME_DEBUG_LOG(data.type_string.toAnsiString() << " has hit " << contact_data_ptr->type_string.toAnsiString() << ", now at " << contact_data_ptr->hp << " hp")
            if (contact_data_ptr->id == BULLET_TYPE)
            {
                GAME_DEBUG_LOG("Hit Bullet!")
                contact_data_ptr->hp = 0;
            }
            break;
        }
    }

    for (auto econt : data.geom_contact_list)
    {
        data.hp = 0;
        break;
    }
    UpdateTimer(data, elapsed_s);
    if (data.hp == 0)
    {
        GAME_DEBUG_LOG("Bullet dead, timer is @ " << data.timer << " seconds")
        //if (data.timer < 0.02f && data.contact_list.size() == 0 && data.geom_contact_list.size() == 0)
        //{
        //     cout << "Very short time, check this!" << endl;
        //}
    }

    ClearContacts(data);

    /* No hit, move to target position and recalculate */
    data.world_pos = data.target_pos;
    data.target_pos = data.world_pos + aim_point.norm() * data.detect_range * elapsed_s;

    data.render_text.setPosition(data.world_pos);
    return nullptr;
}

void bulletLaunchEnterB(TypeData & data, float elapsed_s, fec2 aim_point, int dmg)
{

    data.target_pos = data.world_pos + aim_point.norm() * data.detect_range * elapsed_s;
    data.hp = dmg;
    GAME_DEBUG_LOG(data.hp << "HP assigned to bullet");
    data.detect_range = GetBaseDetectRange(data.id) * GetCurrHPFraction(data);
    ///* In the case where the parents hp fraction has resulted in a hp of < 1, clamp it to 1 as a lower bound */
    //if (data.hp < 1)
    //{
    //    data.hp = 1;
    //    cout << "Correcting bullet hp!" << endl;
    //}
    if (GridNode::GetCell(data.world_pos).CanWalk() == false)
    {
        /* Bullet cannot be created inside a wall, destroy immediately */
        data.hp = 0;
        GAME_DEBUG_LOG("Cannot spawn bullet here!")
    }
}
