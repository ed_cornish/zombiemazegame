cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
set(CMAKE_LEGACY_CYGWIN_WIN32 0)

project("DungeonGame")

add_executable(DungeonGame Behaviours.cpp
BulletBehaviour.cpp
CloseAttackBehaviour.cpp
CollisionShapes.cpp
CooldownBehaviour.cpp
DetectedBehaviour.cpp
Effect.cpp
EntityData.cpp
Explosion.cpp
GFXMgr.cpp
GridNode.cpp
HuntBehaviour.cpp
IdleBehaviour.cpp
main.cpp
main.cpp
MapLoader.cpp
Maze.cpp
micropather.cpp
MovementData.cpp
Node.cpp
PlayerBehaviour.cpp
PortBehaviour.cpp
RangedAttackBehaviour.cpp
Ray.cpp
ScanBehaviour.cpp
Shadow.cpp
SpawnBehaviour.cpp
StateMachine.cpp
Steering.cpp
Type.cpp)


