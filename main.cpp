#include "Node.h"
#include "GridNode.h"
#include "Type.h"
#include "SoundFX.h"

//const char *rpg_string = " A\n V\n_]\n ]\n/]";
const char *rpg_string =  "______.\n7~[]--\"";

int main()
{
    /* Specify 24 bits depth buffer */
    ContextSettings context_settings;
    context_settings.depthBits = 24;

    /* Create window */
    RenderWindow window(VideoMode(1280, 1024),"M A L", Style::Default, context_settings);
    Node::AssignWindow(window);
    context_settings = window.getSettings();
    GAME_DEBUG_LOG("OpenGL version is " << context_settings.majorVersion << ":" << context_settings.minorVersion);
    //window.setVerticalSyncEnabled(true);

    View render_view = window.getView();
    Text FPS_text;
    Text Score_text;
    Text Game_over_text;
    Font my_font;
    Font char_font;

    Texture floor_texture;
    Texture ret_texture;
    Texture part_tex;
    part_tex.loadFromFile("particle.png");
    ret_texture.loadFromFile("reticule.png");
    Image wall_image;
    // Create a clock for measuring the time elapsed
    Clock clock;
    LevelList levels;
    srand((unsigned int)time(NULL));

    if (!levels.LoadLevelData())
    {
        cout << "ERROR loading level files!" << endl;
    }
//    diskLevel test_level("test2.map");

    floor_texture.loadFromFile("texture_sheet.png");
    wall_image.loadFromFile("stone1512.jpg");
    my_font.loadFromFile("lucon.ttf");
    char_font.loadFromFile("joystix monospace.ttf");

    //cout << "Max texture size is " << wall_texture.getMaximumSize() << endl;
    window.setActive();
    /*render_view.setCenter(200, 200);
    window.setView(render_view);*/
    Node::SetFont(char_font);

    SoundFX sounds;


    Type::InitWindow(window);

    Node::ConfigDraw(true);
    //GLWalls wall_renderer(window, wall_image);
    Type::SetFont(char_font);
    fec2 win_pos = window.getView().getCenter();

    fec2 win_delta;

    GFXMgr renderer(90, false);

    renderer.SetupGUIFont(char_font);
    renderer.SetupReticule(ret_texture);

    bool type_enter_mode = false;

    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        if (levels.NewLoadCheck())
        {
            renderer.LevelReset();
            renderer.InitParticleSys(part_tex);
            GridNode::ClearGrid();
            GridNode::CreateGrid(GRID_WIDTH, GRID_CELL_WIDTH, &window, &floor_texture, levels.getNextLevel());
            GridNode::ConfigDraw(true, true, false, false);
            GridNode::SupplyVertices();// wall_renderer);
            vector<spawnData> spawn_list;
            if (levels.getNextLevel() && levels.getNextLevel()->GetSpawnData(spawn_list))
            {
                for (auto s : spawn_list)
                {
                    new Type(GridNode::GetCell(s.x, s.y).GetCellCentre(), s.id);
                }
            }
            vector<glyphData> glyph_list;
            if (levels.getNextLevel() && levels.getNextLevel()->GetGlyphData(glyph_list))
            {
                for (auto g : glyph_list)
                {
                    GFXMgr::AddGlyph(g, char_font);
                }
            }

            SoundFX::play(NEW_LEVEL_SND);
        }
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
            // Adjust the viewport when the window is resized

            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Return)
                {
                    if (type_enter_mode)
                    {
                        //game_gui_ptr->CastText();
                    }
                    type_enter_mode = !type_enter_mode;
                }
                else if (event.key.code == sf::Keyboard::Tab)
                {
                    levels.markLevelComplete();
                }
                else if (event.key.code == sf::Keyboard::I)
                {
                    renderer.ToggleDebugShow();
                }
                else if (event.key.code == sf::Keyboard::P)
                {
                    SoundFX::play(WARP_SND);
                }
                //std::cout << event.key.code << std::endl;
            }
            else if (type_enter_mode && event.type == sf::Event::TextEntered)
            {
                //std::cout << event.text.unicode << std::endl;
            }
        }


        /* Update all nodes in the game*/
        GridNode::WalkGrid(window);
        //GridNode::FrameTick();
        renderer.Frame(window);

    }


    GridNode::ClearGrid();

    return 0;
}
