#pragma once
#include <malloc.h>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <vector>
#include "micropather.h"
#include "SFML/Graphics/Rect.hpp"
#include "Game.h"

#define MAX_DIM 512
enum
{
    FLOOR_BIT_IDX,
    CEILI_BIT_IDX,
    NWALL_BIT_IDX,
    EWALL_BIT_IDX,
    SWALL_BIT_IDX,
    WWALL_BIT_IDX,
    MAX_BIT_IDX,
};

#define CELL_WIDTH 1.0f
#define CELL_HEIGHT 1.0f

#define LEVEL_ROWS	32
#define LEVEL_COLS	LEVEL_ROWS
#define LEVEL_CENTRE_X (LEVEL_COLS * CELL_WIDTH /2)
#define LEVEL_CENTRE_Z (LEVEL_ROWS * CELL_WIDTH /2)
#define PLAYER_VIEW_HEIGHT	(CELL_HEIGHT * 0.6)
#define PLAYER_START_X	CELL_WIDTH/2
#define PLAYER_START_Z	CELL_WIDTH/2

#define PLAYER_AABB_DIM	0.2
#define PLAYER_COLL_RADIUS 0.15


#define FLOOR_BIT_MASK 0x1 << FLOOR_BIT_IDX
#define CEILI_BIT_MASK 0x1 << CEILI_BIT_IDX
#define NWALL_BIT_MASK 0x1 << NWALL_BIT_IDX
#define EWALL_BIT_MASK 0x1 << EWALL_BIT_IDX
#define SWALL_BIT_MASK 0x1 << SWALL_BIT_IDX
#define WWALL_BIT_MASK 0x1 << WWALL_BIT_IDX

#define MAP_ACCESS(x,y) map_v[(y) * maze_cols + (x)]

using namespace micropather;
class Maze : public Graph
{
public:
    Maze();// unsigned int rows = 1, unsigned int cols = 1, unsigned int res = 1);
	~Maze();

    void Config(unsigned int rows, unsigned int cols, unsigned int res);

	void WallGen(unsigned int top, unsigned int left, unsigned int bottom, unsigned int right, unsigned int last_door);
	void Generate();

	void PutBlock(sf::Rect<unsigned int> dims, bool walkable = false);
	void PutBorders();
	void RandomBlock(bool walkable);
	void Strew(unsigned int iterations);

	void PlacePlayer();
	void PlaceExit();
	void PlaceEnemies();

    bool LoadPattern(std::vector<bool>& pattern, unsigned int pattern_width);

    void Path(MPVector<void*> &path, unsigned int start_idx, unsigned int end_idx);

    unsigned int map_access(unsigned int x, unsigned int y);
    unsigned int map_idx_from_cart(unsigned int x, unsigned int y);
	//unsigned int **map;
private:
    std::vector<unsigned int> map_v;
    MicroPather *path_p;
	unsigned int min_span;
	unsigned int maze_rows;
	unsigned int maze_cols;
	bool calc_mask();
	int recursion;
	unsigned int walkable_count;

    void map_index( unsigned int idx, unsigned int &x, unsigned int &y);
    virtual float LeastCostEstimate(void* nodeStart, void* nodeEnd);
    virtual void AdjacentCost(void* node, MPVector< StateCost > *neighbours);
    virtual void PrintStateInfo(void* node);
};

