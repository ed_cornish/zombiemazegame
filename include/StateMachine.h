#pragma once
#include <functional>
#include <memory>
#include <vector>
#include <iostream>
//#include "Behaviours.h"
#include "Game.h"

using namespace std;

/* Fwd Decl */
struct TypeData;
class State;

/* functions to modify typedata and/or to indicate a new state */
typedef function<void(TypeData&, float)> typedatafunc;
typedef function<State*(TypeData&, float)> statechangefunc;

typedef enum 
{
    INIT_STATE, //Used when node first created 
    IDLE_STATE,
    SEARCHING_STATE,
    ATTACKING_STATE,
    FLEEING_STATE,
    NAVIGATING_STATE,
    ALERTING_STATE,
    HACKING_STATE,
    DEAD_STATE, //Draw the entity's corpse/debris
    MAX_STATE_ID,
} state_id;


/* Each state has up to three functions bound to it, one to execute when it enters, one to execute when it exits, and one per frame */
struct StateFuncSet
{
    typedatafunc onEntry;
    statechangefunc onFrame; // This can trigger a state change 
   // typedatafunc onExit;
};
/* Helper function to get the appropriate state funcs from an enum code */
State* GetInitState(node_type_id id);
/* The FSM class for controlling states and behaviours */
class FSM
{
    /* The current state */
    unique_ptr<State> currState;
public:
    FSM(State& initial_state); //An FSM can be initialised with a current state 
    FSM(); // Until an initial state is populated nothing will be done
    ~FSM();
    void runUpdate(TypeData& data, float elapsed_s); // Calls the appropriate functions on the current state, switches state if required
    void initState(State* new_state); //Set to a new state (e.g. for initialisation)
};

// The state class, which has a number of functions bound to it and can return a new state pointer to indicate a transition 
class State
{
    bool active; //Indicates if the state should call it's onEntry method, if applicable 
    StateFuncSet funcs; // The bound functions 
    float timer_s; // This is not really used, the data.timer is used instead
    //TODO: assess relevance of the timer_s variable
public:
    //Construct a state from a set of bound functions
    State(const typedatafunc& entry, const statechangefunc& frame);
    ~State();
    // Run the appropriate functions 
    State* runUpdate(TypeData& data, float elapsed_s);
};


