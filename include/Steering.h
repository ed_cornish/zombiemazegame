#pragma once
#include "CollisionShapes.h"

/* Fwd declaration of TypeData */
struct TypeData;

/* Define a bunch of weighting factors for steering */
struct SteeringWeights
{
    float pursue;
    float flee;
    float flank;
    float wander;
    float navigate;
    float avoid;
    float damping;
};

/* Hold the information for pathfinding via A* algorithm */
struct Path
{
    vector<fec2> wp; //waypoints vector
    unsigned int path_idx; /* The index of the current waypoint */
public:
    Path();
    vector<fec2>& reset(); /* Clear the path and return a reference for population by the algorithm */
    fec2 getCurrentWP(fec2 curr_pos); /* Get the current waypoint, increment the index if needed */
    void debugDraw(RenderWindow& window); /* Draw the waypoints for debug */
};

/* The vectors acting on the object */
struct SteeringVectors
{
    fec2 pursue;
    fec2 flee;
    fec2 flank;
    fec2 wander;
    fec2 navigate;
    fec2 avoid;
};

/* Compute and store the steering vectors each frame based on their current weightings */
class Steering
{
    SteeringVectors vectors;
    void ComputePursue(TypeData &data);
    void ComputeFlee(TypeData &data);
    void ComputeFlank(TypeData &data);
    void ComputeWander(TypeData &data);
    void ComputeNavigate(TypeData &data);
    void ComputeAvoid(TypeData &data);
public:
    /* Weights is made publicly accessible to avoid Setter function quagmire */
    SteeringWeights weights;
    /* The object can take an initial set of weights or use defaults */
    Steering();
    Steering(SteeringWeights weights);

    ~Steering();
    /* Compute new vectors for this frame, clamp the velocity to a maximum value */
    void Compute(TypeData &data, float clamp_max);
    /* Reset the random wander vector */
    void ResetWander();
};
