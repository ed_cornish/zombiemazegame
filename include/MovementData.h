#pragma once
#include <memory>
#include "Game.h"

/* This file handles the initialisation of movement data parameters for each entity in the game */

struct MoverData;
struct TypeData;
/* Master function that will take a data structure and initialise the movement data appropriately */
bool InitMoverData(TypeData& data);