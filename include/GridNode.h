#pragma once
#include "Node.h"
#include "MapLoader.h"


using namespace std;
using namespace sf;
class Node;

enum
{
    LINE_DIR_RIGHT,
    LINE_DIR_DOWN,
    LINE_DIR_LEFT,
    LINE_DIR_UP,
    LINE_DIR_MAX,
};

struct GridLine
{
    int start_x;
    int start_y;

    unsigned int num_cells;

    int dir;

    int end_x;
    int end_y;
};

class GridNode;

class GridOperator
{
public:
    ~GridOperator()
    {
    }
    virtual void operator() (GridNode &cell) = 0;
    GridOperator()
    {
    };
};

class GridNode
{
    friend class Ray;
    friend class ShadowOp;
    friend class Steering;
    friend class AddOp;
    friend class AlertOp;
    ///* Static vars */

    static unsigned int cell_width;

    static vector<GridNode> grid;

    static unsigned int grid_width;

    static Maze grid_maze;

    static RenderWindow *window_ptr;

    static Clock frame_clock;

    static Time elapsed;

    static vector<edge> grid_borders;

    static VertexArray floor_sheet;

    static Texture * floor_texture_p;

    static bool wallDrawOn;
    static bool floorDrawOn;
    static bool shadowDrawOn;
    static bool debugDrawOn;
    static int live_cells;

    static shared_ptr<MicroPather> pather_ptr;

    ///* Private vars per instance */
    int x;
    int y;
    fec2 cell_pos;
    vector<Node> append_v;
    vector<Node> node_v;
    vector<edge> walls;
    unsigned int activity;
    bool shadowed;

    Text glyph;

    void DecrementActivity();

    unsigned int GetActivity();

    void Render();

    bool CheckIntersect(fec2 position);

    void CheckNodeCollisions(Node &node_to_test, int x, int y);

    void CheckWallCollisions(Node & node_to_test, int x, int y);

    void WalkNodes(RenderWindow &window);
protected:

public:
    GridNode();

    ~GridNode()
    {

    }

    bool CanWalk();

    void AddWallEdges();

    fec2 GetCellCentre();

    ///* --- static member functions --- */
    static void CreateGrid(unsigned int grid_width_cells = DEFAULT_GRID_WIDTH,
        unsigned int cell_width_val = 25.f,
        RenderWindow * render_target_p = nullptr,
        Texture * floor_texture_ptr = nullptr,
        diskLevel * map_data_ptr = nullptr);

    static FloatRect GetGridRect();

    static GridNode &GetCell(fec2 pos);

    static GridNode &GetCell(int x, int y);

    static GridNode &GetCell(int grid_index);

    static Node& AddNode(Node &node_to_add);

    static void WalkGrid(RenderWindow &window);

    static bool LOSCheck(fec2 start, fec2 end, float range, RenderWindow & window, bool debug_render = false);

    static fec2 BoundsCheck(fec2 pos);

    static bool BoundsCheck(int &x, int &y);

    static bool CheckOnGrid(fec2 pos);

    static String FrameTick();

    static fec2 ClampEdgeEnd(edge &edge_to_clamp);

    static void SupplyVertices();// GLWalls & wall_renderer);

    static float GetCellWidth();

    static void WalkSpiral(fec2 start_point, unsigned int width, GridOperator& op);

    static bool CheckOnGrid(int x, int y);

    static void ConfigDraw(bool draw_walls = true, bool draw_floor = true, bool draw_shadows = true, bool debug_draw = false);

    static void ClearGrid();

    static void WalkLine(GridLine &line_def, GridOperator& op);

    static void FindPath(fec2 start_pos, fec2 end_pos, vector<fec2>& path);

    void IncrementActivity();

    static void SetGlyph(glyphData data, Font * font_ptr);

    void RenderGlyph(RenderWindow & window);
};
