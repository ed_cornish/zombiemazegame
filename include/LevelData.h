#pragma once
#include <vector>
#include "Game.h"
using namespace std;

struct spawnData
{
    //TODO: Replace with appropriate node_type_id enum
    node_type_id id;
    unsigned int x;
    unsigned int y;
};

struct glyphData
{
    char letter;
    unsigned int x;
    unsigned int y;
};

struct mapData
{
    vector<bool>        maze_pattern;
    unsigned int        grid_w;
    unsigned int        cell_w;
    vector<spawnData>   spawns;
    vector<glyphData>   glyphs;
};
