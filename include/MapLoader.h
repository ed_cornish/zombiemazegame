#pragma once
#include "LevelData.h"
#include <iostream>
#include <fstream>


class diskLevel
{
    mapData level_data;
    bool success_flag;
    fstream * fs_p;

    /* The top-level function for iterating over the filestream and passing the data to appropriate functions */
    void Process();

    bool ProcessInt(unsigned int& val, char& buff);

    /* Deal with each line */
    bool ProcessLine();
    /* Handle node spawns */
    void ProcessNode(unsigned int node_id, unsigned int pattern_pos);

    void ProcessGlyph(char letter, unsigned int patter_pos);

public:
    diskLevel(string fname);
    ~diskLevel();
    /* Check if the level loaded successfully */
    bool GetSuccess() const;

    bool GetMapData(vector<bool> & data) const;
    bool GetSpawnData(vector<spawnData> & data) const;
    bool GetGlyphData(vector<glyphData> & data) const;

    const unsigned int GetGridWidth() const;
    const float GetCellWidth() const;
};

class LevelList
{
    static vector<diskLevel> level_v;
    static int completed_lvl;
    static bool load_new;
public:
    diskLevel * getNextLevel();
    LevelList();
    ~LevelList();
    void markLevelComplete();
    diskLevel * getHub() const;
    bool LoadLevelData();
    bool NewLoadCheck();
};
