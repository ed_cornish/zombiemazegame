#pragma once
#include <vector>
#include <memory>
#include "SFML/Graphics.hpp"
#include "Type.h"
#include "Thor/Particles.hpp"

struct glyphData;

class GFXMgr
{
    static vector<shared_ptr<Type>> chars_v;
    static VertexArray level_geometry;
    static Texture * level_texture_map_p;
    static float frame_interval;
    static VertexArray ray_debug_vertexes;
    static vector<CircleShape> collider_debug_circles;
    static vector<Text> glyphs;
    static thor::ParticleSystem part_mgr;
    static thor::ParticleSystem bg_part_mgr;
    static thor::ParticleSystem wall_tex_part_mgr;
    static bool show_debug_info;
    static int FPS_lock;

    RenderTexture wall_texture;
    RenderTexture bg_texture;
    Sprite cursor_sprite;
    Font * font_p;

    void InitParticleEmitters();
    void UpdateParticles(RenderWindow & window, float elapsed_s);

public:
    GFXMgr(int fps_lock = -1, bool debug_mode = false);
    ~GFXMgr();
    void Frame(RenderWindow& window);
    static void RegisterChar(Type & char_ref);
    static void SetLevelGeometry(VertexArray & geometry);
    static void SetTextureMap(Texture & new_texture);
    static thor::Connection RegisterEmitter(thor::UniversalEmitter &  em_ref);
    static void RegisterExplosionEmitter(thor::UniversalEmitter & em_ref, float lifetime_s);
    void SetupReticule(Texture & reticule_texture);
    void LevelReset();
    void SetupGUIFont(Font & font);
    static void PushRayVertexes(VertexArray & ray_vertexes);
    static void PushColliderShape(CircleShape & shape);
    static void InitParticleSys(Texture & particle_texture);
    void ToggleDebugShow();
    static bool GetDebugState();
    static void AddGlyph(glyphData data, const Font & glyph_font);
};

