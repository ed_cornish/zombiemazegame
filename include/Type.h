#pragma once
#include <memory>
#include "Game.h"
#include "Node.h"
#include "CollisionShapes.h"
#include "Effect.h"
#include "Steering.h"
#include "micropather.h"
#include "StateMachine.h"
#include "GFXMgr.h"
#include "Thor/Input.hpp"
#include "Thor/Particles.hpp"

/* Forward declarations */
class Type;



struct MoverData
{
    /* Where the node wants to go */
    fec2 goal_pos;
    /* 2d velocity of the entity */
    fec2 vel;
    /* The speed to clamp to per second */
    float max_speed;
    /* Object to control pathfinding for the entity. As with the steering controller, this should be moved to the behaviour object so that it does
    not waste memory in stationary entities */
    Path path;
    /* Object to handle the steering behaviours for the entity. This should be moved to the behaviour handler so it is not setup for stationary objects */
    Steering nav;

    MoverData();
};

struct TypeData
{
private:
    static unsigned int instance_count;
public:
    node_type_id id;

    unsigned int instance_id;

    /* The entities health ("STACK") */
    int hp;

    /* A descriptive string indicating what the entity is i.e. "Zombie", "Health Potion", "Grue" */
    String type_string;
    /* The current position of the entity in the world */
    fec2 world_pos;
    /* The position of whatever the entity wants to kill/touch/talk to */
    fec2 target_pos;
    /* Text to render the entity in lieu of pretty artwork */
    Text render_text;
    /* Any effects active on the entity*/
    vector<Effect> effs;
    /* utility timer variable */
    float timer;
    /* flag to indicate if a world space ray is needed */
    bool ray_to_target;
    /* Object to handle the steering behaviours for the entity. This should be moved to the behaviour handler so it is not setup for stationary objects */
    //Steering nav;
    //If a ray to target is being cast, any node that was hit will be pointed to here
    Type *hit_node;
    /* A pointer to an entity we have detected */
    TypeData* detected_ptr;
    /* Ptr to the last typedata we hit with a contact*/
    TypeData* has_hit_ptr;

    /* Store all the collision contacts from each frame here */
    vector<shared_ptr<Type>> contact_list;

    vector<edge*> geom_contact_list;

    Node* host_node_ptr;

    float detect_range;

    shared_ptr<MoverData> move_data;

    TypeData();

    Color render_colour;
};

/* This class wraps the control code and state for each entity in the game. Shared_ptrs are used to prevent leaks and avoid copying data around */
class Type : public std::enable_shared_from_this<Type>
{
    /* Allow effects to access protected data */
    friend Effect;
private:
    /* Static font ptr to allow rendering of character text */
    static Font *font_ptr;
    /* Static variable to hold the time elapsed this frame, updated manually */
    static float time_elapsed_s;
    /* Flag to draw debug stuff */
    static bool debugDraw;

    static fec2 player_pos;

    /* Scoped connection for automatic destruction of particle emitters*/
    shared_ptr<thor::ScopedConnection> auto_conn_p;
    thor::UniversalEmitter emitter;
protected:
    static RenderWindow *window_ptr; //Used for mapping mouse coords to world, debug draw, rendering

    //Called at init to centre the origin of the render text
    void CentreText();

    //Handle a collision with another type
    void handleHit();

    /* Apply the effects to this entity */
    void ApplyEffects();

    /* This data structure holds the entity specific state and behaviour */
    TypeData data;

    FSM state_m;

    void InitEmitter();
public:
    /* Synchronise the entity with it's parent node */
    void SyncNode(Node* node_ptr);

    TypeData* GetTypeDataPtr();


    Type(fec2 pos, node_type_id id, State * initial_state = nullptr, node_type_id * parent_id_p = nullptr);

    Type(const Type &);

    virtual ~Type();

    /* Entity logic is run from witin this method */
    void runUpdate();
    
    void hitBy(TypeData& data_that_hit);

    void RegisterContact(Type& contacting_object);

    void RegisterContact(edge & contacting_edge);

    // Indicate that another node has 'hit' this
    void registerHit(Type &node_hit);

    // Check if the node should be removed
    bool isDead();

    bool isPlayer();

    int GetHP();

    // Get descriptive string
    String GetTypeString();

    // Immediately destroy the entity
    void Kill();

    // Increase the entities hp
    void Heal(unsigned int heal_amount);

    // Add an effect to the entity
    void AddEffect(Effect eff);

    // Get the world position of the entity
    fec2 GetPos();

    static fec2 GetPlayerPos();

    static void UpdatePlayerPos(fec2 pos);

    // Set the world position of the entity
    void SetPos(fec2 pos);

    // Set a goal position
    void SetGoalPos(fec2 pos);

    // Return the target position of the entity
    fec2 GetTargetPos();

    // Handle the rendering of the entity
    void Render();

    void Render(RenderWindow & window, bool debug_mode = false);

    bool IsTypeID(node_type_id id);

    void SpawnFragments();

    // Alert the entity to another entities prescence, to start it showing
    void AlertTo(TypeData& alert_to_type);

    /*=== STATIC methods ===*/
    static void SetFont(Font &font);

    static Font *GetFontPtr();

    static void TickFrame(float secs_elapsed);

    static float GetSecsElapsed();

    static void InitWindow(RenderWindow &window);

    static RenderWindow* GetWindowPtr();

    static void ConfigDraw(bool debug_draw = false);
};

#if 0
class PlayerType : public Type
{
    friend class GUI;
    float secs_since_last_shot;
    static fec2 player_position;
    Text GUI_text;
    unsigned int score;
public:
    PlayerType(fec2 pos);
    ~PlayerType();
    void runUpdate();
    void hitBy(Type &type_that_hit);
    static fec2 GetPlayerPos();
    void DrawPlayerGUI();
};

class ZombieType : public Type
{
    unsigned int notice_counter;
    float attention_clock;
    bool aware_of_player;
public:
    ZombieType(fec2 pos);
    ~ZombieType();
    void runUpdate();
    void hitBy(Type &type_that_hit);
};

class MonsterType : public Type
{
    unsigned int notice_counter;
    float attention_clock;
    bool aware_of_target;
    Steering nav;
    vector<fec2> path; //for pathfinding
    unsigned int path_idx;
    bool search_for_player;
    FSM state_m;
public:
    MonsterType(fec2 pos, TypeData &data);
    ~MonsterType();
    void runUpdate();
    void hitBy(Type &type_that_hit);
};

class HealthType : public Type
{
public:
    HealthType(fec2 pos);
    ~HealthType();
    void runUpdate();
    void hitBy(Type &type_that_hit);
};


class ZombieSpawner : public Type
{
    float spawn_interval;
    float elapsed;
    bool spawn;
    unsigned int num_spawns;
    int max_spawns;
public:
    ZombieSpawner(fec2 pos, float interval);
    ZombieSpawner(fec2 pos, float interval, unsigned int spawn_limit);
    ~ZombieSpawner();
    void runUpdate();
    void hitBy(Type &type_that_hit);
    Type * spawnNew();
};

struct text_blocks_s
{
    Text fps;
    Text score;
    Text hp;
    Text game_over;
    Text central_pop_up;
    Text spell;

    text_blocks_s()
    {
        fps.setColor(Color::White);
        score.setColor(Color::White);
        hp.setColor(Color::White);
        game_over.setColor(Color::White);
        central_pop_up.setColor(Color::White);
        spell.setColor(Color::White);

        fps.setCharacterSize(15);
        score.setCharacterSize(25);
        hp.setCharacterSize(25);
        game_over.setCharacterSize(15);
        central_pop_up.setCharacterSize(15);
        spell.setCharacterSize(30);

    }


    void Render(RenderWindow &window)
    {
        window.draw(fps);
        window.draw(score);
        window.draw(hp);
        if (game_over.getString().isEmpty() == false)
        {
            window.draw(game_over);
        }

        if (central_pop_up.getString().isEmpty() == false)
        {
            window.draw(central_pop_up);
        }

        if (spell.getString().isEmpty() == false)
        {
            window.draw(spell);
        }
    }

    void SetFont(Font &font_r)
    {
        fps.setFont(font_r);
        score.setFont(font_r);
        hp.setFont(font_r);
        game_over.setFont(font_r);
        central_pop_up.setFont(font_r);
        spell.setFont(font_r);
    }
};


class GUI : public Type
{
    text_blocks_s text_blocks;
    PlayerType *player_ptr;
    Font *gui_font_ptr;
public:
    GUI(PlayerType &player, Font *aux_font_ptr = nullptr);
    ~GUI();
    void runUpdate();
    void hitBy(Type &type_that_hit);
    void EnterText(sf::Uint32 text_char);
    void CastText();
};


void monstRangedStrafe(TypeData &data);
#endif
