#pragma once
#include "GridNode.h"
#include "StateMachine.h"
#include "Steering.h"
#include <functional>
/* This file defines the component behaviours that are used to populate a State class */
using namespace std;
using namespace std::placeholders;

/*Forward declarations of needed classes */
struct TypeData;
class PortRegistry;
//class State;
//struct StateFuncSet;

/* Basic monster behaviours */
void monstIdleEnterB(TypeData& data, float elapsed_s);
void monstSearchEnterB(TypeData& data, float elapsed_s);
void monstAttackingEnterB(TypeData& data, float elapsed_s);
void monstFleeingEnterB(TypeData& data, float elapsed_s);

State* monstIdleFrameB(TypeData& data, float elapsed_s);
State* monstSearchFrameB(TypeData& data, float elapsed_s);
State* monstAttackingFrameB(TypeData& data, float elapsed_s);
State* monstFleeingFrameB(TypeData& data, float elapsed_s);

State* playerFrameB(TypeData& data, float elapsed_s);

State* healthFrameB(TypeData& data, float elapsed_s);
State * initStateFrameB(TypeData & data, float elapsed_s);

const SteeringWeights monsterIdleWeights = {
    0.f, //pursue
    0.f, //flee
    0.f, //flank
    0.3f, //wander
    0.1f, //navigate
    1.f, //avoid
    0.15f, //damping
};

const SteeringWeights monsterSearchWeights = {
    0.f, //pursue
    0.f, //flee
    0.f, //flank
    0.2f, //wander
    0.9f,//9f, //navigate
    0.7f, //avoid
    0.15f, //damping
};
const SteeringWeights monsterAttackingWeights = {
    1.f, //pursue
    0.f, //flee
    0.f, //flank
    0.5f, //wander
    0.6f, //navigate
    0.9f, //avoid
    0.15f, //damping
};

const SteeringWeights monsterCollidingWeights = {
    1.f, //pursue
    0.f, //flee
    0.f, //flank
    0.f, //wander
    0.f, //navigate
    0.f, //avoid
    0.5f, //damping
};

const SteeringWeights monsterFleeingWeights = {
    0.f, //pursue
    1.f, //flee
    0.f, //flank
    0.8f, //wander
    0.f, //navigate
    0.9f, //avoid
    0.15f, //damping
};


//State* GetInitialState(node_type_id id);

State& GetPlayerInitialState();

void ClearContacts(TypeData & data);

void npcMoveUpdate(TypeData& data, float elapsed_s, float speed_scaling = 1.f);

void RegisterHit(TypeData & data_of_hitter, TypeData & data_of_hitee);

void RegisterDetection(TypeData & data_of_detecting, TypeData & data_of_detected);

bool GetTypeDetected(TypeData & data, node_type_id id);

bool MoveDataCheck(TypeData & data);

bool CastLOSRay(TypeData & data, fec2 * target_pos_ptr = nullptr);

bool CastObstructRay(TypeData & data, fec2 end_point);

void FollowPath(TypeData & data);

void GetPathToTarget(TypeData & data);

void GetPathToGoal(TypeData & data);

void GetPathToPos(TypeData & data, fec2 pos);

void UpdateTimer(TypeData& data, float elapsed_s);

bool CheckTimerExpire(TypeData& data, float elapsed_s, float timeout_s);

bool CheckDetectedNonSystem(TypeData& data);

float GetCurrHPFraction(TypeData & data);

float GetCurrentMaxSpeedPerSecond(TypeData& data);

float GetCurrentAttackRate(TypeData & data);

int GetBulletDamage(node_type_id id);

bool CheckDetectedTypeId(TypeData & data, node_type_id id_to_check_for);

bool CheckDetectedInstance(TypeData & data, unsigned int instance_id);

node_type_id GetPreferredTarget(TypeData & data);

shared_ptr<Type> GetTypeSharedPtr(TypeData & data);

void FireBullet(TypeData & data, int bullet_damage);

void bulletLaunchEnterB(TypeData & data, float elapsed_s, fec2 aim_point, int dmg);
State * bulletLaunchB(TypeData & data, float elapsed_s, unsigned int launcher_instance, fec2 aim_point);

void interruptorEnterB(TypeData & data, float elapsed_s);
State * interruptorFrameB(TypeData & data, float elapsed_s);

void loopbackEnterB(TypeData & data, float elapsed_s);
State * loopbackFrameB(TypeData & data, float elapsed_s);

void fraggerEnterB(TypeData & data, float elapsed_s);
State * fraggerFrameB(TypeData & data, float elapsed_s);

State * closeAttackTargetFrameB(TypeData & data, float elapsed_s);
State * rangeAttackTargetFrameB(TypeData & data, float elapsed_s);
State * huntTargetFrameB(TypeData & data, float elapsed_s);

State * GetAttackBehaviour(TypeData & data);

void idleEnter(TypeData & data, float elapsed_s);
State * idleWander(TypeData & data, float elapsed_s, float scan_interval_s);
State * scanArea(TypeData & data, float elapsed_s, float rads_s);
void huntEnter(TypeData & data, float elapsed_s, fec2 last_known);
State * huntTarget(TypeData & data, float elapsed_s, float timeout_s);
State * detectedTarget(TypeData & data, float elapsed_s, unsigned int instance_id);
State * spawnAllies(TypeData & data, float elapsed_s);
State * cooldownState(TypeData & data, float elapsed_s, float cooldown_s);
void scanEnterB(TypeData& data, float elapsed_s);
State * scanFrameB(TypeData & data, float elapsed_s, float degs_per_sec, node_type_id id_to_scan_for);

void collideEnterB(TypeData & data, float elapsed_s);
State * collideFrameB(TypeData & data, float elapsed_s);

State * portExitProcess(TypeData & data, float elapsed_s);

void fragEnterB(TypeData & data, float elapsed_s);
State * fragFrameB(TypeData & data, float elapsed_s);

node_type_id GetAllyToSpawn(TypeData & data);
