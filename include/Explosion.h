#pragma once

#include "GFXMgr.h"

class Explosion
{
    float scale;
public:
    Explosion(fec2 pos, float scale, Color * colour_p = nullptr);
    ~Explosion();
};

