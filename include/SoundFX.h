#ifndef SOUNDFX_H
#define SOUNDFX_H

#include <SFML/Audio.hpp>
#include <deque>
enum
{
    PLAYER_HIT_SND,
    PLAYER_SHOOT_SND,
    PLAYER_DIE_SND,
    ENEMY_HIT_SND,
    ENEMY_SHOOT_SND,
    ENEMY_DIE_SND,
    PICKUP_SND,
    ALERT_SND,
    WARP_SND,
    NEW_LEVEL_SND,
    SPAWN_FWALL_SND,
    SPAWN_ENEMY_SND,
    MAX_SND_ID,
};
using namespace sf;
using namespace std;

//This class is to manage one-shot sound effects for shooting, hits, etcetera
class SoundFX
{
    public:
        SoundFX();
        virtual ~SoundFX();
        static void play(int sound_id);
        static void purge(float elapsed_s);

    protected:

    private:
        static SoundBuffer sfx_buffers[MAX_SND_ID];
        static std::deque<Sound> sfx;
};

#endif // SOUNDFX_H
