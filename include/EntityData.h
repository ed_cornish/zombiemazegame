#pragma once
#include "Game.h"
#include "MovementData.h"

struct TypeData;

bool InitTypeData(TypeData& data);

float GetBaseDetectRange(node_type_id id);

int GetMaxHitPoints(node_type_id id);

float GetBaseAttackRate(node_type_id id);

float GetScanDegsPerSec(node_type_id id);

float GetColliderRadius(TypeData & data);

Color GetColor(node_type_id id, node_type_id * parent_id_p = nullptr);
