#pragma once
#include <memory>
#include "Game.h"
#include "CollisionShapes.h"
#include "GridNode.h"

#define SHADOW_COLOR Color::Black

class Shadow
{
    static vector<unique_ptr<Shadow>> shadow_v;
    static VertexArray shadow_mask;
    /* How far the edges should extend from the ends of the shadow face*/
    static float extents;
    /* A Shadow region is composed of 3 edges, two sides and a face which is orientated with the normal toward the light source */
    /* The sides can be constructed from the source and end points of the face */
    fec2 source;
    /* Construct CCW point first! */
    edge face;
    edge ccw_edge;
    edge cw_edge;
public:
    Shadow(fec2 origin, fec2 ccw_vertex, fec2 cw_vertex);
    ~Shadow();
    bool ColliderCheck(circle &collider);
    static void Render(RenderWindow &window);
    static bool ShadowCast(fec2 source, edge &wall);
    static bool InShadowTest(edge wall);
    static bool InShadowTest(fec2 point);
    static bool ShadowIntersect(edge wall, fec2 &intersect_point, fec2 &outside_point);
    static void Reset();
};


class ShadowOp : public GridOperator
{
    fec2 origin_pos;

public:
    ~ShadowOp()
    {
    }

    ShadowOp(fec2 origin);

    void operator() (int x, int y);

    void operator() (GridNode &cell);
};