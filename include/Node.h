#pragma once
#pragma once

#include <memory>
#include "Game.h"
#include "CollisionShapes.h"
#include "Maze.h"

enum
{
    NO_INTERSECT,
    COMPLETE_INTERSECT,
    HORIZONTAL_COLLIDE,
    VERTICAL_COLLIDE,
};

enum
{
    NOT_CONTAINED = 0,
    CONTAINED_INSIDE = 1 << COMPLETE_INTERSECT,
    HORIZONTAL_BORDER_CROSS = 1 << HORIZONTAL_COLLIDE,
    VERTICAL_BORDER_CROSS = 1 << VERTICAL_COLLIDE,
};

typedef enum
{
    STATIC_BLOCK_NODE,
    MOVER_NODE,
    PLAYER_NODE,
    TRIGGER_NODE,
    MAX_NODE_TYPE,
} collider_type_e;

collider_type_e GetColliderType(node_type_id id);

class Type;
struct TypeData;

// A basic node class to be held in the Grid
class Node : public Text
{
//public:
    //static vector<Node> nodes_v;
    static int nodeCount;
    static fec2 player_pos;
    static int player_score;
    static float player_last_shot;
    static Font *font_ptr;
    static bool debugDrawOn;
    static RenderWindow *window_ptr;

    //Shared pointer as we copy nodes to move to a new cell
    shared_ptr<Type> type_ptr;

    void RecalcBounds();

    float DistanceSquared(Node &rh_node);

public:

    Node(collider_type_e collider = STATIC_BLOCK_NODE, Vector2f new_pos = Vector2f(0.f, 0.f), float new_radius = 10.0f, Vector2f vel = Vector2f(0.f, 0.f));

    Node(String node_char, collider_type_e collider = STATIC_BLOCK_NODE, Vector2f new_pos = Vector2f(0.f, 0.f), float new_radius = 10.0f, Vector2f vel = Vector2f(0.f, 0.f));
    /* Deprecate previous constructors once new type system is sorted */
    Node(Type * node_type_ptr, collider_type_e collider = STATIC_BLOCK_NODE, Vector2f new_pos = Vector2f(0.f, 0.f), float new_radius = 10.0f);
    ~Node();

    int id;
    bool dead;
    fec2 position;
    fec2 resultant_pos;   
    circle collider;
    collider_type_e collider_type;
    FloatRect bounds;
    float radius;
    float radius_squared;

    void Render();
    /* Check if two nodes are overlapping by radius (Mover to Mover collisions)*/
    bool RadiusOverlap(Node &rh_node, Vector2f &seperation_vector);

    bool Update(float secs_since_last);

    bool HandleTriggerCollision(Node & rhs, fec2 &resultant);

    bool HandleStaticCollisions(Node & rhs, fec2 &resultant);

    bool HandleMoverCollision(Node & rhs, fec2 &resultant);

    bool HandlePlayerCollision(Node & rhs, fec2 &resultant);

    void HasHitWall(edge & wall);

    void AlertTo(TypeData& type_detected);

    Type *GetTypePtr();

    virtual void Trigger(Node &triggered, fec2 &collide_resultant);

    static String GetScoreString();

    static void SetFont(Font &font);

    static fec2 GetPlayerPos();

    static void ConfigDraw(bool debug_draw = false);

    static void AssignWindow(RenderWindow &window);
};
