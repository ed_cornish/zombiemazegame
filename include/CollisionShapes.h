#pragma once
#include "Game.h"

/* Extends SFML Vector2f class with common vector operations */
struct fec2 : public Vector2f
{
    /* Construct from floats */
    fec2(float x_val = 0.f, float y_val = 0.f) : Vector2f(x_val, y_val)
    {
    }
    /* Construct from unsigned ints */
    fec2(unsigned int x_val, unsigned int y_val) : Vector2f((float)x_val, (float)y_val)
    {
    }
    /* Construct from signed ints */
    fec2(int x_val, int y_val) : Vector2f((float)x_val, (float)y_val)
    {
    }
    /* Construct from an unsigned int and a signed int */
    fec2(unsigned int x_val, int y_val) : Vector2f((float)x_val, (float)y_val)
    {
    }
    /* Construct from a signed int and a signed int */
    fec2(int x_val, unsigned int y_val) : Vector2f((float)x_val, (float)y_val)
    {
    }
    /* Construct from a sf::Vector2f */
    fec2(Vector2f vector_val) : Vector2f(vector_val)
    {
    }

    ~fec2();

    /* Dot product against another fec2 */
    float dot(fec2 rhs);

    /* Returns the length of the vector */
    float mag();

    /* Returns a vector normalised to length 1 */
    fec2 norm();

    /* Rotate the vector by an angle in radians (clockwise) */
    void rotate(float angle_in_rads);

    /* Return a vector perpendicular to the vector (will point up if the origin is on the left ) */
    fec2 perp();
};

/* A 2d circle for collision testing */
struct circle
{
private:
    /* Debug render shape */
    CircleShape render_shape;
    bool debug_render;
public:
    /* The centre point in 2d space */
    fec2 centre;
    /* Radius in 2d space */
    float radius;

    /* Construct from a centre, a radius, and determine if debug rendering should happen */
    circle(fec2 pos, float rad, bool render = false) : centre(pos), radius(rad), debug_render(render)
    {
        if (debug_render)
        {
            render_shape = CircleShape(radius);
            render_shape.setOrigin(render_shape.getLocalBounds().width / 2, render_shape.getLocalBounds().height / 2);
            render_shape.setOutlineThickness(-1.0f);
            render_shape.setFillColor(Color::Transparent);
            render_shape.setPointCount(6);
            render_shape.setOutlineColor(Color::Red);
        }
    }

    ~circle()
    {

    }

    /* Draw the debug shape to a render window */
    void draw_me();

    /* Check for overlap with another circle */
    bool overlap(circle rhs, Vector2f &seperation);

    /* Set the colour for debug drawing */
    void SetDrawColour(Color draw_color);
};

/* A collision shape in the form of a line between 2 points i.e. a wall */
struct edge
{
protected:
    float length;
    fec2 start;
    fec2 end;
    bool debug_render;
    /* The slope or gradient of the line. Used along with the y-crossing point to determine intersections */
    float slope;
    /* The y-crossing point in 2d space. Used along with the slope to determine intersections */
    float yintercept;
    /* Internal helper to recalculate the y-crossing and\or the slope if the start or end changes */
    void RecalcLine();
public:
    /* Debug vertices for drawing to the screen */
    VertexArray render_vertexes;

    /* Construct from a start and end point and set debug drawing on or off */
    edge(fec2 start_point, fec2 end_point, bool render = false) : start(start_point), end(end_point), debug_render(render)
    {
        length = fec2(end - start).mag();
        render_vertexes.append(Vertex(start, Color::Red));
        render_vertexes.append(Vertex(end, Color::Red));
        render_vertexes.setPrimitiveType(Lines);
        slope = (end.y - start.y) / (end.x - start.x);
        if (!isnan(slope))
        {
            yintercept = start.y - slope * start.x;
        }
        else
        {
            yintercept = nanf("");
        }
    }

    ~edge()
    {
    }
    /* Helper to get the start point */
    fec2 getstart();
    /* Helper to get the end point */
    fec2 getend();

    fec2 perp();

    /* Determine if a circle overlaps the edge, updates a seperation vector for collision resolution */
    bool overlap_circle(circle rhs, fec2 & seperation);

    /* Determine if a circle overlaps and gives the nearest_point on the edge in either case */
    bool overlap_circle_np(circle rhs, fec2 & near_point);

    /* Determines if a circle overlaps the edge without updating seperation or near point information */
    bool overlap_circle(circle rhs);

    /* Determines if a point is on the line */
    bool point_on_line(fec2 point);

    /* Returns the nearest point on the line to a point in space */
    fec2 nearest_point(fec2 point);
    /* Returns the length of the line */
    float mag();
    /* Draw debug vertices to a window */
    void draw_me(RenderWindow &window);
    /* Determine if a point is above the line (assuming that start is on the left) */
    bool above_line(fec2 point);
    /* Determine if two lines intersect, and updates the point of intersection */
    bool intersect(edge &rhs, fec2 &intersect_point);
    /* Rotate the line by an amount in degrees (clockwise) */
    void rotate(float angle_in_degrees);
    /* Set the debug draw flag */
    void setrender(bool render_flag);
    /* Determine if the debug draw flag is set */
    bool getrender();
    /* Stretch the line in the direction of the end by a fraction of it's current length */
    void extend(float amount);
};

/* This is used to check if floating point vectors are near enough to be considered equal*/
bool flpts_rough_equivalence(fec2 a, fec2 b, float epsilon);
