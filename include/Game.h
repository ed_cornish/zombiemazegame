#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <sstream>
#include <cmath>


#define MY_PI 3.1415926535897932384626433832795f

#define INVALID_NODE_IDX    -1

#define MIN_BRANCH_WIDTH 2.0f
#define MAX_UNBRANCHED_LEAVES 1
#define DEFAULT_GRID_WIDTH 8

#define CELL_POS(x,y,w) fec2((float)((x)*w), (float)((y)*w))

#define PLAYER_MOVE_SPEED 140.f
#define MONSTER_MOVE_SPEED 90.f
#define MONSTER_IDLE_SPEED 20.f
#define MONSTER_TURN_LIMIT 360.f

#define PLAYER_SHOOT_INTERVAL 0.2f
#define PLAYER_WARP_INTERVAL 0.8f
#define PLAYER_FWALL_INTERVAL 3.f
#define PLAYER_VIEW_RANGE 2000.f
#define PLAYER_LIGHT_RADIUS_CELLS 8
#define COLOR_MAX   255
#define ZOMBIE_HP 40
#define PLAYER_HP 100
#define HEALTH_PACK_HP 50

#define ZOMBIE_ATTACK_RANGE 40.f
#define ZOMBIE_ATTENTION_THRESHOLD 10
#define ZOMBIE_COLLIDER_RADIUS 10.f
#define ZOMBIE_AQCUIRE_TIME 0.2f
#define ZOMBIE_ATTENTION_SPAN 2.f
#define ZOMBIE_VIEW_RANGE 600.f

#define IMP_VIEW_RANGE 200.f
#define IMP_ENGAGEMENT_RANGE 120.f

#define RANDOM_NODES_TO_ADD 16
#define GRID_CELL_WIDTH 60
#define GRID_WIDTH  8
#define PLAYER_START_CLEARANCE GRID_CELL_WIDTH
#define PLAYER_START fec2(PLAYER_START_CLEARANCE/2,PLAYER_START_CLEARANCE/2)
#define RANDOM_MOVER_POS fec2((rand() % GRID_WIDTH * GRID_CELL_WIDTH) + PLAYER_START_CLEARANCE, (rand() % GRID_WIDTH * GRID_CELL_WIDTH) + PLAYER_START_CLEARANCE)
#define RANDOM_NODE_POS fec2((rand() % GRID_WIDTH * GRID_CELL_WIDTH), (rand() % GRID_WIDTH * GRID_CELL_WIDTH))

#define GL_FLOATS_PER_VERTEX 5
#define GL_WALLS_Z_OFFSET -6.5f
#define GL_POSITION_VERTEX_NUM_FLOATS 3
#define GL_UV_VERTEX_NUM_FLOATS 2

#define ENEMY_SCAN_INTERVAL 0.1f

class GFXMgr;

#define GAME_DEBUG_LOG(s) {if(GFXMgr::GetDebugState()) cout << s << "\n"; }

typedef enum
{
    PLAYER_TYPE,            //The player
    DISINFECTOR_TYPE,       //Hunts the player down and tries to delete them
    INTERRUPTOR_TYPE,       //Fires projectiles at the player to lock them
    PORT_TYPE,              //Entry\Exit points from level
    GARBAGECOLLECTOR_TYPE,  //Clears up memory leaks\dropped packets
    WATCHDOG_TYPE,          //Scans for bad threads and summons AV threads if required
    FRAGMENT_TYPE,           //Generic system process, moves around level and ignores player
    FIREWALL_TYPE,          //Blocks access from an area
    LOGFILE_TYPE,           //Contains info, possible fragments
    EXEFILE_TYPE,           //Produces threads, possibly contains passcode fragments
    SOURCEFILE_TYPE,        //Contains FlOps (xp), possible passcode fragments
    BUFFER_TYPE,            //Can be used as a hiding place, contains XP, possible fragments
    VIRUS_TYPE,             //Deletes system threads it touches. Destroyed by disinfector
    WORM_TYPE,              //Not spotted by Watchdogs, when it gets to it's destination, spawns a virus
    TROJAN_TYPE,            //Latches onto a system thread and allows the player to warp to it
    LOGICBOMB_TYPE,         //'Detonates', locking every thread nearby
    BULLET_TYPE,           //'Crashes' a process, causing it to lose 'stack' (health)
    NO_TYPE_ID,             //NULL type
    MAX_TYPE_ID,
} node_type_id;

using namespace std;
using namespace sf;
