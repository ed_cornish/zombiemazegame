#pragma once
#include <functional>
#include <iostream>

using namespace std;

/* Forward declaration for classes */
struct TypeData;

/* All the possible effects should be represented in this enum so that effects 
   can effectively filter one another*/
typedef enum
{
    IMPACT_EFFECT,
    FIRE_EFFECT,
    ICE_EFFECT,
    CONFUSION_EFFECT,
    STASIS_EFFECT,
    STUN_EFFECT,
    SHOCK_EFFECT,
    TELEPORT_EFFECT,
    POLYMORPH_EFFECT,
    HEAL_EFFECT,
    MAX_EFFECT_ID,
} effect_type_id;

struct Effect
{
private:
    /* Define a function to filter new effects added to the owner
       For example, a fire resistance effect would reduce the amount of any fire effects added */
    function<void(Effect&, effect_type_id, int)> filt_f;
    /* Define a function to take effect each frame (regenerate health, confuse the creature, render sparkles, etcetera) */
    function<void(TypeData&, int)> exec_f;
    /* A ptr to the target data, registered previously.*/
    TypeData *target_ptr;
    /* Flag to indicate the effect has worn off and should be removed */
    bool timed_out;    
    /* The magnitude of the effect to apply */
    int amount;
    /* The time for this effect to wear off */
    float time_out_seconds;
    /* Type indicator for comparison/filtering purposes */
    effect_type_id type;
public:
    /* Construct an effect, default times out after 1 frame */
    Effect(effect_type_id type, int amount, float time_out = 0.f);
    /* Register some target data */
    void RegisterTarget(TypeData &target);
    /* Register the per-frame function */
    void RegisterExec(function<void(TypeData&, int)> func);
    /* Register the filter function (if any) */
    void RegisterFilt(function<void(Effect&, effect_type_id, int)> func);
    /* Apply the per-frame function */
    void Apply(float elapsed_secs);
    /* Filter an effect */
    void Filter(Effect &eff);
    /* Indicate if the effect has worn off and should be removed by the host */
    bool TimedOut();
    /* Retrieve the type of the effect */
    effect_type_id GetType();
    /* Retrieve the amount of the effect to apply */
    int GetAmount();
    /* Modify the amount of the effect to apply */
    void ChangeAmount(int delta);

    ~Effect();
};

/* Declarations of effect functions (acting on Type class) */
void effOnFire(TypeData &data, int amount);

void effRegenHealth(TypeData &data, int amount);

void effHeal(TypeData& data, int amount);

void effResistEffect(Effect &eff, effect_type_id type, int amount);

void effWeaknessEffect(Effect &eff, effect_type_id type, int amount);

Effect GetEffect(effect_type_id id, TypeData& target, int amount, float timeout = 0.f);