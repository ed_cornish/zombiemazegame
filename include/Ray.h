#pragma once
#include "Node.h"
#include "GridNode.h"
#include "Shadow.h"

/* Ray class inherits from edge collision shape */
class Ray :
    public edge
{
private:
    Node *node_hit;
    edge *edge_hit;
    fec2 hit_point;
    Node *start_node;
    bool CheckGridNodeCollideWalls(GridNode &cell);
    bool CheckGridNodeCollideNodes(GridNode &cell);
    /* Unused - deprecated in favour of Shadow class */
    bool ShadowCastGrid();
    /* Unused - deprecated in favour of Shadow class */
    bool CheckGridNodeShadowWalls(GridNode & cell);
    /* Unused - deprecated internally */
    bool HitEnd();
public:
    Ray(fec2 start_pos, fec2 end_pos, bool render = false);
    Ray(Node &originator, fec2 end_pos, bool render = false);
    ~Ray();

    bool CheckGridNodeCollideBlocked(GridNode & cell);

    /*This is the entry point for a ray cast operation */
    bool RayCastGrid();

    /* Render for debug */
    void draw_me(RenderWindow &window);

    fec2 GetTerminationPoint();

    /* Retrieve the node hit by the ray, if any */
    Node * GetHitNode();
};