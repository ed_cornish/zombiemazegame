#include "GridNode.h"
#include "Ray.h"
#include "Node.h"
#include "Type.h"
#include "Shadow.h"
#include "GFXMgr.h"
#include "Thor/Math.hpp"

vector<GridNode> GridNode::grid;

unsigned int GridNode::grid_width;
unsigned int GridNode::cell_width;
Maze GridNode::grid_maze;
RenderWindow * GridNode::window_ptr = nullptr;
Clock GridNode::frame_clock;
Time GridNode::elapsed = Time::Zero;
int GridNode::live_cells = 0;
vector<edge> GridNode::grid_borders;
VertexArray GridNode::floor_sheet(Quads);
Texture * GridNode::floor_texture_p = nullptr;
shared_ptr<MicroPather> GridNode::pather_ptr;

bool GridNode::wallDrawOn = true;
bool GridNode::floorDrawOn = true;
bool GridNode::shadowDrawOn = true;
bool GridNode::debugDrawOn = false;

//Constructor for gridnode, called once when the Grid is generated.
GridNode::GridNode()
{
    y = grid.size() / grid_width;
    x = grid.size() % grid_width;
    //cell_width = 50.f;
    AddWallEdges();
    activity = 10;
    cell_pos = fec2((x * cell_width), (y * cell_width));
    shadowed = false;
}

bool GridNode::CanWalk()
{
    if (grid_maze.map_access(x, y) == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void GridNode::IncrementActivity()
{
    if (activity < 20 && node_v.size() > 0)
    {
        activity += 20;
    }
}

void GridNode::DecrementActivity()
{
    if (activity > 0)
    {
        activity--;
    }
}

unsigned int GridNode::GetActivity()
{
    return activity;
}

//Render any nodes in the cell, as well as highlighting the cell if activity > 0
void GridNode::Render()
{
    RectangleShape cell_shape;

    if (node_v.size() > 0)
    {
        //Set transparency based on activity
        //if (GetActivity() > 0)
        //{
        //    cell_shape.setFillColor(Color(255, 0, 0, 80));
        //    cell_shape.setPosition((float)(x*cell_width), (float)(y*cell_width));
        //    cell_shape.setSize(fec2(cell_width, cell_width));
        //    window_ptr->draw(cell_shape);
        //}
        for (auto &node : node_v)
        {
            node.Render();
        }
    }
}

//Check if a position lies within the cell's boundary
bool GridNode::CheckIntersect(fec2 position)
{
    FloatRect bounds = FloatRect(fec2(x*cell_width, y*cell_width), fec2(cell_width, cell_width));
    //cout << "Position : " << position.x << ", " << position.y << endl;
    if (bounds.contains(position))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Check for collisions between a node and all nodes in a specified grid location
void GridNode::CheckNodeCollisions(Node &node_to_test, int x, int y)
{
    fec2 collide_resultant;
    GridNode & grid_to_check = GetCell(x, y);
    if (node_to_test.dead)
    {
        //Do not check collisions for dead nodes
        return;
    }
    for (auto &node_in_cell : grid_to_check.node_v)
    {
        // Collide each node (Static block nodes are passive and do not adjust their position)
        if (node_in_cell.id != node_to_test.id && node_in_cell.dead == false)
        {
            if (node_to_test.RadiusOverlap(node_in_cell, collide_resultant))
            {
                if (!node_to_test.HandleStaticCollisions(node_in_cell, collide_resultant))
                {
                    if (!node_to_test.HandleTriggerCollision(node_in_cell, collide_resultant))
                    {
                        if (!node_to_test.HandleMoverCollision(node_in_cell, collide_resultant))
                        {
                            //One of the following should have executed if a radius overlap has occured
                        }
                    }
                }
            }
            if (isnan(node_in_cell.resultant_pos.x) || isnan(node_in_cell.resultant_pos.y))
            {
                //cout << "nan found!" << endl;
            }
            else
            {
                node_in_cell.position = node_in_cell.resultant_pos;
            }
        }
    }
    if (isnan(node_to_test.resultant_pos.x) || isnan(node_to_test.resultant_pos.y))
    {
        cout << "nan found!" << endl;
    }
    else
    {
        node_to_test.position = node_to_test.resultant_pos;
    }
}

//Test for collisions against all walls in the cell
void GridNode::CheckWallCollisions(Node &node_to_test, int x, int y)
{
    fec2 collide_resultant;

    for (auto &wall : GetCell(x, y).walls)
    {
        if (node_to_test.collider_type != STATIC_BLOCK_NODE && wall.overlap_circle(node_to_test.collider, collide_resultant))
        {
            node_to_test.resultant_pos += collide_resultant;
            node_to_test.HasHitWall(wall);
        }
    }
    if (isnan(node_to_test.resultant_pos.x) || isnan(node_to_test.resultant_pos.y))
    {
        cout << "nan found!" << endl;
    }
    else
    {
        node_to_test.position = node_to_test.resultant_pos;
    }
}

//Walk over the nodes in the cell, run their update methods, check for collisions, and render them.
//If a node leaves the cell, remove it from this cell and add it back to the Grid so that it will find the appropriate new cell
void GridNode::WalkNodes(RenderWindow &window)
{
    vector<Node> collision_candidates;
    vector<Node> nodes_to_move;
    fec2 collide_resultant;
    fec2 old_pos;

    for (auto &node : node_v)
    {
        if (node.dead)
        {
            //Do nothing with dead nodes
        }
        else
        {
            node.position = BoundsCheck(node.position);
            //if (isnan(node.position.x))
            //{
            //    cout << "nan found!" << endl;
            //}

            //Clamp to grid
            //if (isnan(node.position.x))
            //{
            //    cout << "nan found!" << endl;
            //}

            //if (grid_maze.map_access(cell.x, cell.y) == 0)
            //{
            //    node.position += fec2(node.position - fec2(GridNode::grid_width * GridNode::cell_width, GridNode::grid_width * GridNode::cell_width)).norm() * (float)GridNode::cell_width;
            //    //node.goal_pos = node.position;
            //    //if (isnan(node.position.x))
            //    //{
            //    //    cout << "nan found!" << endl;
            //    //}
            //}
            //Check collisions against walls (neighbours are used to stop from clipping through corners)
            CheckWallCollisions(node, x, y);
            //if (isnan(node.position.x))
            //{
            //    cout << "nan found!" << endl;
            //}
            CheckWallCollisions(node, x + 1, y);
            CheckWallCollisions(node, x - 1, y);
            CheckWallCollisions(node, x, y - 1);
            CheckWallCollisions(node, x, y + 1);

            //Update will return true if the node is doing anything
            if (node.Update(elapsed.asSeconds()))
            {
                IncrementActivity();
            }
            else
            {
                DecrementActivity();
            }

            GridNode &cell = GetCell(node.position);

            if (node.collider_type == PLAYER_NODE && CanWalk() == false)// && (node.collider_type == MOVER_NODE || node.collider_type == PLAYER_NODE))
            {
                // GAME_DEBUG_LOG("Player currently on walkable: " << cell.CanWalk());
                /* Mover node has glitched into an unwalkable cell...*/
                node.position.x += ((cell_width / 2.f) - (node.position.x - GetCellCentre().x));
                node.position.y += ((cell_width / 2.f) - (node.position.y - GetCellCentre().y));
            }


            CheckNodeCollisions(node, x, y);
            CheckNodeCollisions(node, x - 1, y);
            CheckNodeCollisions(node, x - 1, y - 1);
            CheckNodeCollisions(node, x, y - 1);
            CheckNodeCollisions(node, x + 1, y - 1);

            //Activate the cells around the player so that any sleeping cells will wake up
            if (node.collider_type == PLAYER_NODE)
            {
                GetCell(x + 1, y).IncrementActivity();
                GetCell(x - 1, y).IncrementActivity();

                GetCell(x + 1, y + 1).IncrementActivity();
                GetCell(x - 1, y + 1).IncrementActivity();

                GetCell(x + 1, y - 1).IncrementActivity();
                GetCell(x - 1, y - 1).IncrementActivity();

                GetCell(x, y + 1).IncrementActivity();
                GetCell(x, y - 1).IncrementActivity();
            }



            //If the node leaves the cell, remove it and push a copy onto the grid so that it waterfalls down the new correct cell
            if (!CheckIntersect(node.position))
            {
                nodes_to_move.push_back(node);
                node.dead = true;
            }


            //cout << node.type_ptr.use_count() << " references in use in update" << endl;
        }
    }

    for (auto &node : nodes_to_move)
    {
        //cout << node.type_ptr.use_count() << " references in use before move" << endl;
        //if (isnan(node.position.x))
        //{
        //    cout << "nan found!" << endl;
        //}
        GridNode::AddNode(node);
        //cout << node.type_ptr.use_count() << " references in use after move" << endl;
    }
    //Remove any dead nodes from the node_v
    node_v.erase(remove_if(node_v.begin(), node_v.end(), [](Node & a_node) { return a_node.dead; }), node_v.end());

    for (auto &node : append_v)
    {
        node_v.push_back(node);
    }
    append_v.clear();

    //If there are no nodes in this cell, decrement the activity so that the cell will not be updated
    if (node_v.size() == 0)
    {
        DecrementActivity();
    }
}


/* This function adds wall edges to a cell based on the bitmask defined in the level map */

void GridNode::AddWallEdges()
{
    float wall_fudge = 0.1f;
    fec2 wall_start;
    fec2 wall_end;
    unsigned int bmask = grid_maze.map_access(x, y);
    if (bmask & NWALL_BIT_MASK)
    {
        wall_start = CELL_POS(x + 1, y, cell_width);
        wall_end = CELL_POS(x, y, cell_width);
        wall_start.y += wall_fudge;
        wall_end.y += wall_fudge;

        walls.emplace_back(edge(wall_start, wall_end, debugDrawOn));
    }
    if (bmask & EWALL_BIT_MASK)
    {
        wall_start = CELL_POS(x + 1, y + 1, cell_width);
        wall_end = CELL_POS(x + 1, y, cell_width);
        wall_start.x -= wall_fudge;
        wall_end.x -= wall_fudge;
        walls.emplace_back(edge(wall_start, wall_end, debugDrawOn));
    }
    if (bmask & SWALL_BIT_MASK)
    {
        wall_start = CELL_POS(x, y + 1, cell_width);
        wall_end = CELL_POS(x + 1, y + 1, cell_width);
        wall_start.y -= wall_fudge;
        wall_end.y -= wall_fudge;
        walls.emplace_back(edge(wall_start, wall_end, debugDrawOn));
    }
    if (bmask & WWALL_BIT_MASK)
    {
        wall_start = CELL_POS(x, y, cell_width);
        wall_end = CELL_POS(x, y + 1, cell_width);
        wall_start.x += wall_fudge;
        wall_end.x += wall_fudge;
        walls.emplace_back(edge(wall_start, wall_end, debugDrawOn));
    }
    for (auto &wall : walls)
    {
        /* Extend the walls slightly*/
        wall.extend(0.01f);
    }
}

//Generate the GridNodes for the grid sequentially, after generating the bitmask for the Maze (random each time)
void GridNode::CreateGrid(unsigned int grid_width_cells, unsigned int cell_width_val, RenderWindow * render_target_p, Texture * floor_texture_ptr, diskLevel * map_data_ptr)
{
    if (map_data_ptr)
    {
        vector<bool> mapData;
        grid_width = map_data_ptr->GetGridWidth();
        cell_width = map_data_ptr->GetCellWidth();
        if (!map_data_ptr->GetMapData(mapData))
        {
            cout << "ERROR getting map data from level loader!" << endl;
        }
        else
        {
            /* Load the maze from the mapData */
            grid_maze.LoadPattern(mapData, grid_width);
            GAME_DEBUG_LOG("Loaded new pattern!");
        }
    }
    else
    {
        grid_width = grid_width_cells;
        cell_width = cell_width_val;
        /* RNG the maze */
        grid_maze.Config(grid_width, grid_width, 2);
    }
    for (unsigned int i = 0; i < (grid_width*grid_width); i++)
    {
        //Auto gen the grid
        grid.push_back(GridNode());
    }
    if (render_target_p != nullptr)
    {
        window_ptr = render_target_p;
    }
    floor_texture_p = floor_texture_ptr;

    grid_borders.emplace_back(fec2(grid_width * cell_width, 0), fec2(0, 0), true);
    grid_borders.back().render_vertexes[0].color = Color::Yellow;
    grid_borders.emplace_back(fec2(0, 0), fec2(0, grid_width * cell_width), true);
    grid_borders.back().render_vertexes[0].color = Color::Yellow;
    grid_borders.emplace_back(fec2(grid_width * cell_width, grid_width * cell_width), fec2(grid_width * cell_width, 0), true);
    grid_borders.back().render_vertexes[0].color = Color::Yellow;
    grid_borders.emplace_back(fec2(0, grid_width * cell_width), fec2(grid_width * cell_width, grid_width * cell_width), true);
    grid_borders.back().render_vertexes[0].color = Color::Yellow;
}

FloatRect GridNode::GetGridRect()
{
    FloatRect ret_rect;
    ret_rect.width = grid_width * cell_width;
    ret_rect.height = ret_rect.width;
    return ret_rect;
}

//Getter to return a reference to the Cell containing the supplied position
GridNode & GridNode::GetCell(fec2 pos)
{
    fec2 clamped = BoundsCheck(pos);
    unsigned int index = (unsigned int)(floor(clamped.y / cell_width)*grid_width + floor(clamped.x / cell_width));
    if (index >= grid.size())
    {
        index = (grid.size() - 1);
    }
    if (index < 0)
    {
        index = 0;
    }
    return grid[index];
}

//Getter to return a reference to the cell at the specified grid coordinates
GridNode & GridNode::GetCell(int x, int y)
{
    int clamped_x, clamped_y;
    if (y > (int)grid_width)
    {
        clamped_y = (int)grid_width - 1;
    }
    else if(y < 0)
    {
        clamped_y = 0;
    }
    else
    {
        clamped_y = y;
    }
    if (x > (int)grid_width)
    {
        clamped_x = (int)grid_width - 1;
    }
    else if (x < 0)
    {
        clamped_x = 0;
    }
    else
    {
        clamped_x = y;
    }
    unsigned int index = y * grid_width + x;
    if (index >= grid.size())
    {
        index = grid.size() - 1;
    }
    if (index < 0)
    {
        index = 0;
    }
    return grid[index];
}

GridNode & GridNode::GetCell(int grid_index)
{
    if (grid_index >= (int)grid.size())
    {
        grid_index = grid.size() - 1;
    }
    if (grid_index < 0)
    {
        grid_index = 0;
    }
    return grid[grid_index];
}

class TestOp : public GridOperator
{
    void operator() (int x, int y)
    {
        //& cell = GridNode::GetCell(x, y);
        cout << "Cell " << x << ", " << y << endl;
    }

    void operator() (GridNode &cell)
    {
        //cout << cell.GetCellCentre().x << ", " << cell.GetCellCentre().y << endl;
        cell.IncrementActivity();
    }
};

class AddOp : public GridOperator
{
    Node* node_p;
    bool added;

public:
    void operator() (int x, int y)
    {
    }

    void operator() (GridNode &cell)
    {
        //cout << cell.GetCellCentre().x << ", " << cell.GetCellCentre().y << endl;

        if (added == false && cell.CanWalk() && node_p != nullptr)
        {
            cell.append_v.push_back(*node_p);
            cell.IncrementActivity();
            added = true;
            node_p = &cell.append_v.back();
            //node_p->position = cell.GetCellCentre();
        }
    }
    AddOp(Node &node_to_add)
    {
        node_p = &node_to_add;
        added = false;
    }

    Node* getNode()
    {
        if (added == true && node_p != nullptr)
        {
            return node_p;
        }
        else
        {
            return nullptr;
        }
    }
};

//Add a node to the grid, it will be added to the node_v in the appropriate cell
Node& GridNode::AddNode(Node &node_to_add)
{
    if (isnan(node_to_add.position.x))
    {
        cout << "nan found!" << endl;
    }

    if (node_to_add.position.x > grid_width*cell_width)
    {
        node_to_add.position.x = (float)(grid_width*cell_width - 1);
    }
    if (node_to_add.position.y > grid_width*cell_width)
    {
        node_to_add.position.y = (float)(grid_width*cell_width - 1);
    }

    GridNode & cell = GridNode::GetCell(node_to_add.position);
    //AddOp add_f(node_to_add);
    if (cell.CanWalk() == false)
    {
        node_to_add.position.x += ((cell_width / 2.f) - (node_to_add.position.x - cell.GetCellCentre().x));
        node_to_add.position.y += ((cell_width / 2.f) - (node_to_add.position.y - cell.GetCellCentre().y));
        cell = GridNode::GetCell(node_to_add.position);
    }
    //WalkSpiral(node_to_add.position, 3, add_f);
    ////Add the node to the appropriate cell
    cell.append_v.push_back(node_to_add);
    ////Wake up the cell to ensure the new node is processed
    cell.IncrementActivity();
    //Return a reference to the new node
    //Node* returned = &node_to_add;

    return node_to_add;
}

//This is the function to walk the grid and update the nodes in any awake cells (activity > 0)
//It also renders the wall edges to the screen
void GridNode::WalkGrid(RenderWindow &window)
{
    //This contains the vertices to render the walls for each cell
    VertexArray edge_verts;
    vector<GridNode *> render_ptrs;
    //cout << "Grid size :" << grid.size() << endl;
    //Keep track of the number of live cells
    live_cells = 0;
    //Walk cells in the grid

    //WalkSpiral(PlayerType::GetPlayerPos());
    //fec2 fov_vector(0, 1000.f);

    if (window_ptr != nullptr && floor_texture_p != nullptr)
    {
        //window_ptr->draw(floor_sheet, RenderStates(floor_texture_p));
    }

    for (auto &cell : grid)
    {
        //Do not update cells with no activity
        if ((cell.node_v.size() + cell.append_v.size()) > 0)
        {
            //Do not update any cells with activity == 0
            if (cell.GetActivity() > 0)
            {
                cell.WalkNodes(window);
                live_cells++;
            }
            //Render all nodes even if they are sleeping
            //render_ptrs.push_back(&cell);
            cell.Render();
        }
    }

    if (shadowDrawOn)
    {
        ShadowOp my_op(Type::GetPlayerPos());
        Shadow::Reset();
        for (auto &cell : grid)
        {
            //cell.Render();
            for (auto &e : cell.walls)
            {
                Shadow::ShadowCast(Type::GetPlayerPos(), e);
            }
        }
        WalkSpiral(Type::GetPlayerPos(), 8, my_op);
    }

    //PlotRay(window, Node::player_pos, window.mapPixelToCoords(Mouse::getPosition(window)));
    // LOSCheck(Node::player_pos, window.mapPixelToCoords(Mouse::getPosition(window)), 200.f, window);
    edge_verts.setPrimitiveType(Lines);

    //Draw the walls
    if (window_ptr != nullptr && debugDrawOn)
    {
        window_ptr->draw(edge_verts);
        for (auto &border : grid_borders)
        {
           border.draw_me(*window_ptr);
        }
    }
    //cout << "Live Cells " << live_cells << endl;
}

//This performs a raycast between two points to see if there is Line-Of-Sight (no walls in the way). If a window ref is provided, the ray is drawn to the screen
bool GridNode::LOSCheck(fec2 start, fec2 end, float range, RenderWindow & window, bool debug_render)
{
    //Boundscheck both start and end points
    start = BoundsCheck(start);
    end = BoundsCheck(end);

    if (isnan(start.x) || isnan(start.y))
    {
        exit(1);
    }

    //Total vector delta between start and end
    fec2 delta = end - start;
    //Tunable search increment, needs to be set so that each search iteration will reliably fall within every possible cell on the grid
    fec2 delta_inc = delta.norm() * (float)cell_width / 10.0f;
    float slope = (delta.x == 0 ? nanf("") : delta.y / delta.x);
    float yintercept = (delta.x == 0 ? nanf("") : start.y - slope*start.x);
    fec2 stop_normal;
    //Begin our search at the start point
    fec2 curr_pos = start;
    fec2 stop_point;
    fec2 near_point;
    edge los_edge(start, end);
    bool searching = true;
    //Assume LOS until blocker is found
    bool los = true;

    VertexArray plot_array;



    stop_point = end;
    //Search for obstructions
    while (searching)
    {
        //If the end point is in the same cell as the current position, there can be no walls in the way, so stop searching
        if (fec2(curr_pos - BoundsCheck(start)).mag() > delta.mag())
        {
            //We have overshot, stop searching
            searching = false;
        }
        else
        {
            //Increment the current position
            curr_pos += delta_inc;

            //Get the cell
            GridNode & curr_cell = GetCell(curr_pos);
            //Wakeup the cell
            curr_cell.IncrementActivity();
            //If we have landed in an unwalkable cell (a solid wall) then there cannot be LOS and return false
            if (grid_maze.map_access(curr_cell.x, curr_cell.y) == 0)
            {
                searching = false;
                los = false;
                stop_point = curr_pos;
            }
            else
            {
                //Check each wall in the cell to see if any lie between the start and the end point
                for (auto &wall : curr_cell.walls)
                {
                    //Create an edge from the start point to each end of the wall - if the end point lies between these two edges
                    //then it must be between the start and end points and so LOS must be blocked
                    edge to_start(BoundsCheck(start), wall.getstart());
                    edge to_end(BoundsCheck(start), wall.getend());
                    //Check if end point is between the two edges
                    if ((to_start.above_line(end) != to_end.above_line(end))
                        && (wall.above_line(start) != wall.above_line(end)))
                    {
                        searching = false;
                        los = false;
                        if (wall.getstart().x == wall.getend().x && !(delta.x == 0))
                        {
                            //...wall is vertical
                            near_point = fec2(wall.getstart().x, slope * wall.getstart().x + yintercept);
                        }
                        else if (!(delta.x == 0))
                        {
                            //...wall is horizontal
                            near_point = fec2((wall.getstart().y - yintercept) / slope, wall.getstart().y);
                        }
                        else
                        {
                            //...line is vertical, wall must be horizontal
                            near_point = fec2(start.x, wall.getstart().y);

                        }
                        if (fec2(near_point - start).mag() < fec2(stop_point - start).mag())
                        {
                            stop_point = near_point;
                            stop_normal = wall.perp();
                        }
                    }
                }
                //Check each node in the cell to see if it blocks
                for (auto &node : curr_cell.node_v)
                {
                    fec2 derp;
                    node.collider.SetDrawColour(Color::Magenta);
                    plot_array.append(Vertex(los_edge.nearest_point(node.position) + fec2(10, 0), Color::Cyan));
                    plot_array.append(Vertex(los_edge.nearest_point(node.position) - fec2(10, 0), Color::Cyan));
                    if (los_edge.overlap_circle(node.collider, derp) && node.collider.centre != start)
                    {
                        near_point = los_edge.nearest_point(node.collider.centre);
                        los = false;
                        searching = false;
                    }
                    if (fec2(near_point - start).mag() < fec2(stop_point - start).mag())
                    {
                        stop_point = near_point;
                        stop_normal = fec2(stop_point - node.collider.centre);
                    }
                }
            }
        }
    }
    //If end point is out of range, return false and do not search for obstructions
    if (delta.mag() > range)
    {
        searching = false;
        los = false;
        //end = start + delta.norm() * range;
    }

    //Append the end point, coloured based on LOS value
    plot_array.append(Vertex(start, (los ? Color(0, 255, 0, 100) : Color(255, 0, 0, 80))));
    plot_array.append(Vertex(stop_point, (los ? Color(0, 255, 0, 100) : Color(255, 0, 0, 80))));
    if (stop_point != end)
    {
        plot_array.append(Vertex(stop_point, Color::Yellow));
        plot_array.append(Vertex(stop_point + stop_normal.norm() * 10.f, Color::Yellow));
    }
    if (los)
    {
        plot_array[0].color = Color::Green;
    }
    plot_array.setPrimitiveType(Lines);
    if (window_ptr != nullptr && debug_render)
    {
        window_ptr->draw(plot_array);
    }
    return los;
}

float ClampFloatToLimits(float val, float lower_lim, float upper_lim)
{
    if (val >= upper_lim)
    {
        return upper_lim - 0.001f;
    }
    else if (val <= lower_lim)
    {
        return lower_lim + 0.001f;
    }
    else
    {
        return val;
    }
}

//Helper function to clamp a position to the bounds of the grid
fec2 GridNode::BoundsCheck(fec2 pos)
{
    fec2 clamped_pos;
    clamped_pos.x = ClampFloatToLimits(pos.x, 0.f, (float)(cell_width * grid_width));
    clamped_pos.y = ClampFloatToLimits(pos.y, 0.f, (float)(cell_width * grid_width));


    //clamped_pos.x = (pos.x < 0 ? 0 : (pos.x >(cell_width * grid_width) ? ((cell_width * grid_width) - 0.1) : pos.x));
    //clamped_pos.y = (pos.y < 0 ? 0 : (pos.y >(cell_width * grid_width) ? ((cell_width * grid_width) - 0.1) : pos.y));

    return clamped_pos;
}

bool GridNode::BoundsCheck(int & x, int & y)
{
    if (x < 0)
    {
        x = 0;
    }
    if (y < 0)
    {
        y = 0;
    }
    if (y >= (int)grid_width)
    {
        y = grid_width - 1;
    }
    if (x >= (int)grid_width)
    {
        x = grid_width - 1;
    }

    return false;
}

bool GridNode::CheckOnGrid(fec2 pos)
{
    if (BoundsCheck(pos) == pos)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Update the FPS and number of live cells
String GridNode::FrameTick()
{
    static float max_fps = 0.f;
    static float min_fps = 999.f;
    static float avg_fps = 0.f;
    static float accumulated_time = 0.f;
    static unsigned int calls = 0;
    float secs_elapsed;
    String ret_string;
    ostringstream stream;
    calls++;

    elapsed = frame_clock.restart();
    secs_elapsed = elapsed.asSeconds();
    Type::TickFrame(secs_elapsed);

    accumulated_time += secs_elapsed;
    if ((1 / secs_elapsed) > max_fps)
    {
        max_fps = 1 / secs_elapsed;
    }

    if (avg_fps > 0.f && (1 / secs_elapsed) < min_fps)
    {
        min_fps = 1 / secs_elapsed;
    }

    if (calls >= 60)
    {
        calls = 0;
        avg_fps = 60/accumulated_time;
        accumulated_time = 0.f;
    }

    stream << "FPS: " << avg_fps << " (mean), " << max_fps << " (max), " << min_fps << " (min)";
    stream << " - Live Cells :" << live_cells;
    ret_string = stream.str();
    return ret_string;
}

fec2 GridNode::ClampEdgeEnd(edge & edge_to_clamp)
{
    if (isnan(edge_to_clamp.getend().x) || isnan(edge_to_clamp.getend().y))
    {
        cout << "Nan found!" << endl;
    }
    fec2 cross_point = edge_to_clamp.getend();
    for (auto &border : grid_borders)
    {
        if (border.intersect(edge_to_clamp, cross_point))
        {
            //cout << "Wall hit!" << endl;
            //return cross_point;
        }
    }
    return cross_point;
}

fec2 GridNode::GetCellCentre()
{
    return cell_pos + fec2(cell_width / 2.f, cell_width / 2.f);
}

void GridNode::SupplyVertices()// GLWalls &wall_renderer)
{
    fec2 quad_start;
    fec2 quad_end;
    fec2 intersects_at;
    fec2 outside;
    Vector2u texture_size;
    float uv_x, uv_y_start, uv_y_end;
    unsigned int walls_in_shadow;
    floor_sheet.clear();
    FloatRect gridrect = GetGridRect();
    thor::Distribution<Vector2f> uv_rand = thor::Distributions::rect(fec2(gridrect.width/2.f, gridrect.height/2.f), fec2(gridrect.width / 2.f, gridrect.height / 2.f));
    thor::Distribution<float> uv_dim_dist = thor::Distributions::uniform(50.f, 100.f);
    for (auto &cell : grid)
    {
        walls_in_shadow = 0;
        //for (auto &e : cell.walls)
        //{
        //    wall_renderer.AddWall(e.getstart() / (float)cell_width, e.getend() / (float)cell_width, 1.0f);
        //}
        if (cell.CanWalk() && floorDrawOn)// && cell.shadowed == false)
        {
            /*texture_size = floor_texture_p->getSize();
            uv_x = (float)texture_size.x;
            uv_y_start = (float)(texture_size.y >> 2) * 2;
            uv_y_end = (float)((texture_size.y >> 2) * 3)-1;*/
            //floor_sheet.append(Vertex(cell.cell_pos, Color::Cyan));
            //floor_sheet.append(Vertex(cell.cell_pos + fec2(cell_width, 0), Color::Cyan));
            //floor_sheet.append(Vertex(cell.cell_pos + fec2(cell_width, cell_width), Color::Cyan));
            //floor_sheet.append(Vertex(cell.cell_pos + fec2(0, cell_width), Color::Cyan));

        }
        else if(wallDrawOn)
        {
            texture_size = floor_texture_p->getSize();
            uv_x = 1.f;// (float)texture_size.x;
            uv_y_start = 0;
            uv_y_end = (float)(texture_size.y >> 2);
            float uv_dim = cell_width;
            fec2 top_left = uv_rand();
            fec2 btm_right = top_left + fec2(uv_dim, uv_dim);
            fec2 btm_left = btm_right - fec2(uv_dim, 0.f);
            fec2 top_right = top_left + fec2(uv_dim, 0.f);

            floor_sheet.append(Vertex(cell.cell_pos, top_left));
            floor_sheet.append(Vertex(cell.cell_pos + fec2(cell_width, 0), top_right));
            floor_sheet.append(Vertex(cell.cell_pos + fec2(cell_width, cell_width), btm_right));
            floor_sheet.append(Vertex(cell.cell_pos + fec2(0, cell_width), btm_left));
            //floor_sheet.append(Vertex(cell.cell_pos, fec2(0.f, uv_y_start)));
            //floor_sheet.append(Vertex(cell.cell_pos + fec2(cell_width, 0), fec2(0.f, uv_y_end)));
            //floor_sheet.append(Vertex(cell.cell_pos + fec2(cell_width, cell_width), fec2(uv_x, uv_y_end)));
            //floor_sheet.append(Vertex(cell.cell_pos + fec2(0, cell_width), fec2(uv_x, uv_y_start)));
            //texture_size = floor_texture_p->getSize();
            //uv_x = texture_size.x;
            //uv_y_start = 0;
            //uv_y_end = (texture_size.y >> 2)-1;

            //target_v.push_back(cell.cell_pos.x / cell_width);
            //target_v.push_back(-cell.cell_pos.y / cell_width);
            //target_v.push_back(0.55f);
            //target_v.push_back(0.f);
            //target_v.push_back(0);
            ////1
            //target_v.push_back((cell.cell_pos.x / cell_width) + 1);
            //target_v.push_back(-cell.cell_pos.y / cell_width);
            //target_v.push_back(0.55f);
            //target_v.push_back(1.f);
            //target_v.push_back(0);
            ////2
            //target_v.push_back((cell.cell_pos.x / cell_width));
            //target_v.push_back((-cell.cell_pos.y / cell_width) -1);
            //target_v.push_back(0.55f);
            //target_v.push_back(0.f);
            //target_v.push_back(1);
            ////3
            //target_v.push_back((cell.cell_pos.x / cell_width));
            //target_v.push_back((-cell.cell_pos.y / cell_width) - 1);
            //target_v.push_back(0.55f);
            //target_v.push_back(0.f);
            //target_v.push_back(1);
            ////4
            //target_v.push_back((cell.cell_pos.x / cell_width) + 1);
            //target_v.push_back(-cell.cell_pos.y / cell_width);
            //target_v.push_back(0.55f);
            //target_v.push_back(1.f);
            //target_v.push_back(0);
            ////5
            //target_v.push_back((cell.cell_pos.x / cell_width) + 1);
            //target_v.push_back((-cell.cell_pos.y / cell_width) - 1);
            //target_v.push_back(0.55f);
            //target_v.push_back(1.f);
            //target_v.push_back(1);
        }
    }

    GFXMgr::SetLevelGeometry(floor_sheet);
    if (floor_texture_p)
    {
        GFXMgr::SetTextureMap(*floor_texture_p);
    }
    //target_v.clear();
}

float GridNode::GetCellWidth()
{
    return (float)cell_width;
}

enum
{
    WALKING_LEFT,
    WALKING_UP,
    WALKING_RIGHT,
    WALKING_DOWN,
    WALKING_MAX,
};

void GridNode::WalkSpiral(fec2 start_point, unsigned int width, GridOperator &op)
{
    GridLine line_def;
    unsigned int total_cells = width * width;
    int start_x, start_y;
    GridNode current_cell = GetCell(start_point);
    bool done = false;

    line_def.dir = LINE_DIR_RIGHT;
    line_def.num_cells = 1;
    line_def.start_x = current_cell.x;
    line_def.start_y = current_cell.y;
    start_x = line_def.start_x;
    start_y = line_def.start_y;

    for (unsigned int num_cells = 0; num_cells < total_cells; num_cells++)
    {
        /* Reset the line_dir if we have completed a spiral loop */


        WalkLine(line_def, op);

        /* Change line direction */
        if (line_def.num_cells != 1)
        {
            line_def.dir++;
        }
        else
        {
            line_def.dir = LINE_DIR_MAX;
        }

        if (line_def.dir >= LINE_DIR_MAX)
        {
            line_def.dir = LINE_DIR_RIGHT;
            start_x--;
            start_y--;
            line_def.start_x = start_x;
            line_def.start_y = start_y;

            if (line_def.num_cells == 1)
            {
                line_def.num_cells += 1;
            }
            else
            {
                line_def.num_cells += 2;
            }
        }
        else
        {
            line_def.start_x = line_def.end_x;
            line_def.start_y = line_def.end_y;
        }

    }

   // cout << "Done!" << endl;
}

bool GridNode::CheckOnGrid(int x, int y)
{
    if (x < 0)
    {
        return false;
    }
    else if (x >= (int)grid_width)
    {
        return false;
    }
    else if (y < 0)
    {
        return false;
    }
    else if (y >= (int)grid_width)
    {
        return false;
    }
    return true;
}

void GridNode::ConfigDraw(bool draw_walls, bool draw_floor, bool draw_shadows, bool debug_draw)
{
    wallDrawOn = draw_walls;
    floorDrawOn = draw_floor;
    shadowDrawOn = draw_shadows;
    debugDrawOn = debug_draw;
}

void GridNode::ClearGrid()
{
    grid.clear();

}

void GridNode::WalkLine(GridLine & line_def, GridOperator& op)
{
    int x, y;

    //BoundsCheck(line_def.start_x, line_def.start_y);

    x = line_def.start_x;
    y = line_def.start_y;

    line_def.end_x = x;
    line_def.end_y = y;

    //cout <<  " - " << line_def.dir << endl;
    for (unsigned int n = 0; n < line_def.num_cells; n++)
    {
        if (CheckOnGrid(x, y))
        {
            GridNode & cell = GetCell(x, y);
            //cout << "Cell > x: " << cell.x << ", y: " << cell.y << endl;
            op(cell);
        }


        switch (line_def.dir)
        {
        case LINE_DIR_RIGHT:
            x++;
            break;
        case LINE_DIR_DOWN:
            y++;
            break;
        case LINE_DIR_LEFT:
            x--;
            break;
        case LINE_DIR_UP:
            y--;
            break;
        default:
            std::cout << "ERROR: Unknown direction in WalkLine function: " << line_def.dir << std::endl;

        }

        line_def.end_x = x;
        line_def.end_y = y;

    }

}

void GridNode::FindPath(fec2 start_pos, fec2 end_pos, vector<fec2>& path)
{
    MPVector<void*> mpPath;
    GridNode &start = GetCell(start_pos);
    GridNode &end = GetCell(end_pos);
    if (start.cell_pos == end.cell_pos)
    {
        /* No point pathfinding within the same cell! */
        return;
    }
    grid_maze.Path(mpPath, grid_maze.map_idx_from_cart(start.x, start.y), grid_maze.map_idx_from_cart(end.x, end.y));
    /* Turn void* representations of indexes into positions in space */
    for (unsigned int n = 0; n < mpPath.size(); ++n)
    {
        GridNode& cell = GetCell((unsigned int)(MP_UPTR)mpPath[n]);
        if (cell.CanWalk() == false)
        {
            /* If we have somehow pathed into an unwalkable cell, abort and try again next time */
            path.clear();
            return;
        }
        path.push_back(cell.GetCellCentre());
    }
}

