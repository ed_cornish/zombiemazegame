#include "Behaviours.h"
#include "Type.h"
/* This behaviour is for scanning the surroundings for non-system thread. The scan ray will rotate like a radar */

void scanEnterB(TypeData& data, float elapsed_s)
{
    data.timer = 0.f;
}

State * scanFrameB(TypeData & data, float elapsed_s, float degs_per_sec, node_type_id id_to_scan_for)
{
    fec2 ray_vector = fec2(0.f, data.detect_range);
    float degrees = data.timer * degs_per_sec;

    ray_vector.rotate(degrees * (MY_PI/180));
    data.target_pos = data.world_pos + ray_vector;

    if (CheckTimerExpire(data, elapsed_s, 360.f / degs_per_sec))
    {
        return new State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, ENEMY_SCAN_INTERVAL));
    }
    CastLOSRay(data);
    if (CheckDetectedTypeId(data, id_to_scan_for ))
    {
        return new State(nullptr, bind(detectedTarget, _1, _2, data.detected_ptr->instance_id));
    }
    npcMoveUpdate(data, elapsed_s, 0.2f);
    return nullptr;
}


