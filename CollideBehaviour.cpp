#include "Behaviours.h"
#include "Type.h"
#include "EntityData.h"

void collideEnterB(TypeData & data, float elapsed_s)
{
    if(MoveDataCheck(data))
    {
        data.move_data->nav.weights = monsterCollidingWeights;
        GetPathToTarget(data);
    }
}

State * collideFrameB(TypeData & data, float elapsed_s)
{
    TypeData * last_detected = data.detected_ptr;
    CastLOSRay(data);
    if(CheckDetectedTypeId(data, GetPreferredTarget(data)) && data.detected_ptr == last_detected)
    {
        /* We still have los to the original target, keep moving */
        data.timer = 0.f;
    }
    else
    {
        FollowPath(data);
        /* LOS condition has changed, rescan */
    }
    if(CheckTimerExpire(data, elapsed_s, 0.2f))
    {
        return new State(nullptr, bind(scanFrameB, _1, _2, GetScanDegsPerSec(data.id), GetPreferredTarget(data)));
    }
    npcMoveUpdate(data, elapsed_s, 1.f);
    return nullptr;
}
