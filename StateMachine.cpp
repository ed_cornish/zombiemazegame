#include "StateMachine.h"
#include "Behaviours.h"

/* Define const initial states for all types */
const State InitialStates[MAX_TYPE_ID] = {
    State(nullptr, bind(playerFrameB, _1, _2)), //    PLAYER_TYPE,
    State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, 0.6f)), //    DISINFECTOR_TYPE,
    State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, 0.6f)),//    INTERRUPTOR_TYPE,
    State(nullptr, bind(portExitProcess, _1, _2)),//    PORT_TYPE,
    State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, 0.3f)),//    GARBAGECOLLECTOR_TYPE,
    State(bind(idleEnter, _1, _2), bind(idleWander, _1, _2, 0.f)),// WATCHDOG_TYPE,
    State(bind(fragEnterB, _1, _2), bind(fragFrameB, _1, _2)),//    FRAGMENT_TYPE,
    State(nullptr, nullptr),//    FIREWALL_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    LOGFILE_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    EXEFILE_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    SOURCEFILE_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    BUFFER_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    VIRUS_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    WORM_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    TROJAN_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    LOGICBOMB_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    BULLET_TYPE,
    State(nullptr, bind(playerFrameB, _1, _2)),//    NO_TYPE_ID,
};

State::State(const typedatafunc & entry, const statechangefunc & frame): active(false), timer_s(0.f)
{
    funcs.onEntry = entry;
    funcs.onFrame = frame;
}

State::~State()
{
}

State * State::runUpdate(TypeData & data, float elapsed_s)
{
    State* next_state = nullptr;

    timer_s += elapsed_s;

    if (active == false)
    {
        if (funcs.onEntry)
        {
            funcs.onEntry(data, elapsed_s);
        }
        active = true;
    }
    if (funcs.onFrame)
    {
        next_state = funcs.onFrame(data, elapsed_s);

    }

    /*if (next_state != nullptr && funcs.onExit)
    {
        funcs.onExit(data, elapsed_s);
    }*/


    return next_state;
}

FSM::FSM(State & initial_state)
{
}

FSM::FSM()
{
}

FSM::~FSM()
{
}

void FSM::runUpdate(TypeData & data, float elapsed_s)
{
    if (currState)
    {
        State* next_state = currState->runUpdate(data, elapsed_s);
        if (next_state != nullptr)
        {
            currState.reset(next_state);
        }
    }
}

void FSM::initState(State* new_state)
{
    currState.reset(new_state);
}

StateFuncSet GetStateFSet(state_id state, node_type_id type)
{
    return StateFuncSet();
}

State* GetInitState(node_type_id id)
{
    State* init_state_ptr = new State(InitialStates[id]);
    return init_state_ptr;
}
