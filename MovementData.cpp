#include "MovementData.h"
#include "Type.h"

#define NULL_SPEED_VAL -1.f
const float max_speed_table[MAX_TYPE_ID] = {
    200.f, //    PLAYER_TYPE, 
    160.f,   //    DISINFECTOR_TYPE, 
    170.f,   //    INTERRUPTOR_TYPE, 
    0.f,   //    PORT_TYPE, 
    170.f,   //    GARBAGECOLLECTOR_TYPE, 
    0.f,   //    WATCHDOG_TYPE,         
    0.f,   //    FRAGMENT_TYPE,
    0.f,   //    FIREWALL_TYPE,
    0.f,   //    LOGFILE_TYPE,
    0.f,   //    EXEFILE_TYPE,
    0.f,   //    SOURCEFILE_TYPE,
    0.f,   //    BUFFER_TYPE,
    100.f,   //    VIRUS_TYPE,
    100.f,   //    WORM_TYPE,
    100.f,   //    TROJAN_TYPE,
    0.f,   //    LOGICBOMB_TYPE,
    100.f,   //    BITFLIP_TYPE,
    0.f,   //    NO_TYPE_ID,
};


/* Apply the correct max speed */
bool InitMoverData(TypeData & data)
{
    if (max_speed_table[data.id] > 0.f)
    {
        data.move_data.reset(new MoverData());
        data.move_data->max_speed = max_speed_table[data.id];
        GAME_DEBUG_LOG("Initialised max speed of " << data.type_string.toAnsiString() << " to " << data.move_data->max_speed)
        return true;
    }
    return false;
}
