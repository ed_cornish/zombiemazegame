#include "Shadow.h"

vector<unique_ptr<Shadow>> Shadow::shadow_v;
VertexArray Shadow::shadow_mask(Quads);
float Shadow::extents = 1000.f;

Shadow::Shadow(fec2 origin, fec2 ccw_vertex, fec2 cw_vertex) : 
    source(origin), face(ccw_vertex, cw_vertex, true), 
    ccw_edge(ccw_vertex + fec2(ccw_vertex - origin).norm() * extents, ccw_vertex),
    cw_edge(cw_vertex, cw_vertex + fec2(cw_vertex-origin).norm() * extents)
{
    shadow_mask.append(Vertex(ccw_edge.getstart(), SHADOW_COLOR));
    shadow_mask.append(Vertex(ccw_edge.getend(), SHADOW_COLOR));
    shadow_mask.append(Vertex(cw_edge.getstart(), SHADOW_COLOR));
    shadow_mask.append(Vertex(cw_edge.getend(), SHADOW_COLOR));
}

Shadow::~Shadow()
{
}

bool Shadow::ColliderCheck(circle & collider)
{
    /* Not used */
    return false;
}

void Shadow::Render(RenderWindow & window)
{

    //shadow_mask.setPrimitiveType(Lines);
    window.draw(shadow_mask);
}

bool Shadow::ShadowCast(fec2 source, edge & wall)
{
    fec2 ccw_point;
    fec2 cw_point;

    if (!wall.above_line(source))
    {
        ccw_point = wall.getstart();
        cw_point = wall.getend();
        /*ccw_point = ccw_point - wall.perp().norm() * 2.f;
        cw_point = cw_point - wall.perp().norm() * 2.f;*/
        shadow_v.emplace_back(new Shadow(source, ccw_point, cw_point));
    }
    else
    {
        return false;
    }
    return true;
}

bool Shadow::InShadowTest(edge wall)
{
    unsigned int shadow_count = 0;
    fec2 cand_start = wall.getstart();
    fec2 cand_end = wall.getend();
    fec2 intersect_point;
    for (auto &shad : shadow_v)
    {
        if (shad->ccw_edge.above_line(cand_start)
            && shad->cw_edge.above_line(cand_start)
            && shad->face.above_line(cand_start)
            && shad->ccw_edge.above_line(cand_end)
            && shad->cw_edge.above_line(cand_end)
            && shad->face.above_line(cand_end))
        {
            return true;
        }
        else if (shad->ccw_edge.intersect(wall, intersect_point) || shad->cw_edge.intersect(wall, intersect_point) || shad->face.intersect(wall, intersect_point))
        {
            if (shad->face.getstart() == wall.getend() || shad->face.getend() == wall.getstart())
            {
                return true;
            }
            shadow_count++;
        }            

        if (shadow_count > 1)
        {
            return true;
        }
    }
    return false;
}

bool Shadow::InShadowTest(fec2 point)
{
    for (auto &shad : shadow_v)
    {
        if (shad->ccw_edge.above_line(point)
            && shad->cw_edge.above_line(point)
            && shad->face.above_line(point))
        {
            return true;
        }
    }
    return false;
}

bool Shadow::ShadowIntersect(edge wall, fec2 & intersect_point, fec2 & outside_point)
{
    for (auto &shad : shadow_v)
    {
        if (shad->ccw_edge.intersect(wall, intersect_point) || shad->cw_edge.intersect(wall, intersect_point))
        {
            if (InShadowTest(wall.getend()))
            {
                outside_point = wall.getstart();
            }
            else
            {
                outside_point = wall.getend();
            }
            return true;
        }
    }
    return false;
}

void Shadow::Reset()
{

    shadow_v.clear();
    shadow_mask.clear();
}

ShadowOp::ShadowOp(fec2 origin) : GridOperator()
{
    origin_pos = GridNode::BoundsCheck(origin);
}

void ShadowOp::operator()(int x, int y)
{
    GridNode & cell = GridNode::GetCell(x, y);
    for (auto &wall : cell.walls)
    {
        Shadow::ShadowCast(origin_pos, wall);
    }
}

void ShadowOp::operator()(GridNode & cell)
{
    for (auto &wall : cell.walls)
    {
        Shadow::ShadowCast(origin_pos, wall);
    }
}
