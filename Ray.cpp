#include "Ray.h"
#include "GFXMgr.h"

Ray::Ray(fec2 start_pos, fec2 end_pos, bool render) :
    edge(start_pos, end_pos, render),
    hit_point(end_pos),
    node_hit(nullptr),
    start_node(nullptr),
    edge_hit(nullptr)
{
    fec2 bounded_end = GridNode::ClampEdgeEnd(*this);
    fec2 delta = end - start;
    fec2 x_delta = fec2(bounded_end.x - start.x, 0.f);
    fec2 y_delta = fec2(0.f, bounded_end.y - start.y);
    //float scalar_prod_x = delta.dot(x_delta)/(delta.mag() * delta.mag());
    //float scalar_prod_y = delta.dot(y_delta)/(delta.mag() * delta.mag());
    if (bounded_end != end)
    {
       // fec2 clamped = (delta * (scalar_prod_y > scalar_prod_x ? scalar_prod_x : scalar_prod_y));
        end = bounded_end;
        render_vertexes.append(Vertex(bounded_end + fec2(10, 0), Color::White));
        render_vertexes.append(Vertex(bounded_end - fec2(10, 0), Color::White));
        render_vertexes.append(Vertex(bounded_end - fec2(0, 10), Color::White));
        render_vertexes.append(Vertex(bounded_end + fec2(0, 10), Color::White));
        ////clamped = clamped.dot(y_delta) / (clamped.mag() * clamped.mag());
        ////end = clamped;
        RecalcLine();
        hit_point = end;

        //render_vertexes.append(Vertex(start, Color::White));
        //render_vertexes.append(Vertex(start + x_delta, Color::White));
        //render_vertexes.append(Vertex(start, Color::White));
        //render_vertexes.append(Vertex(start + y_delta, Color::White));
    }

}

Ray::Ray(Node & originator, fec2 end_pos, bool render):
    edge(originator.position, end_pos, render),
    hit_point(end_pos),
    node_hit(nullptr),
    start_node(&originator),
    edge_hit(nullptr)
{
    fec2 bounded_end = GridNode::ClampEdgeEnd(*this);
    fec2 delta = end - start;
    fec2 x_delta = fec2(bounded_end.x - start.x, 0.f);
    fec2 y_delta = fec2(0.f, bounded_end.y - start.y);

    if (bounded_end != end)
    {

        end = bounded_end;
        render_vertexes.append(Vertex(bounded_end + fec2(10, 0), Color::White));
        render_vertexes.append(Vertex(bounded_end - fec2(10, 0), Color::White));
        render_vertexes.append(Vertex(bounded_end - fec2(0, 10), Color::White));
        render_vertexes.append(Vertex(bounded_end + fec2(0, 10), Color::White));

        RecalcLine();
        hit_point = end;

    }

}

Ray::~Ray()
{
}

bool Ray::CheckGridNodeCollideBlocked(GridNode & cell)
{
    if (cell.CanWalk() == false)
    {
        fec2 hit_candidate = nearest_point(cell.GetCellCentre());
        if (fec2(hit_candidate - start).mag() < fec2(hit_point - start).mag())
        {
            // Count as a hit if the cell is blocked
            // Set the hit point to the nearest point on the ray to the cell centre
            hit_point = hit_candidate;
            node_hit = nullptr;
            // TODO: Check why sometimes when a node is close to a wall the ray seems to stop short
            render_vertexes[0].color = Color::Yellow;
            render_vertexes[1].color = Color::Yellow;
        }
        return true;
    }
    return false;
}

bool Ray::CheckGridNodeCollideWalls(GridNode & cell)
{
    fec2 intersect_point = end;
    bool hit_wall = false;

    if (cell.CanWalk())
    {
        for (auto &wall : cell.walls)
        {
            //wall.render_vertexes[0].color = Color::Magenta;
            // wall.render_vertexes[1].color = Color::Magenta;
            //Does the wall intersect the ray?
            if (
                //(above_line(wall.getstart()) && !above_line(wall.getend()))
                //||
                //(!above_line(wall.getstart()) && above_line(wall.getend()))
                //||
                (intersect(wall, intersect_point))
                )
            {
                hit_wall |= true;
                wall.render_vertexes[0].color = Color::Blue;
                wall.render_vertexes[1].color = Color::Blue;

                //intersect(wall, intersect_point);
                /*
                                                            render_vertexes.append(Vertex(wall.getstart(), Color::White));
                                                            render_vertexes.append(Vertex(wall.getend(), Color::White));*/
                                                            //Is the point of intersection nearer than the last recorded hit_point?
                if (fec2(intersect_point - start).mag() < fec2(hit_point - start).mag())
                {
                    hit_point = intersect_point;
                    node_hit = nullptr;
                }
            }
        }
        //if (cell.walls.size() > 1 && hit_wall)
        //{
        //    cout << " " << endl;
        //}
    }
    else
    {
        //Check for an intersection with a blocked cell
        fec2 hit_candidate = nearest_point(cell.GetCellCentre());
        if(fec2(hit_candidate - start).mag() < fec2(hit_point - start).mag())
        {
            // Count as a hit if the cell is blocked
            hit_wall = true;
            // Set the hit point to the nearest point on the ray to the cell centre
            hit_point = hit_candidate;
            node_hit = nullptr;
            // TODO: Check why sometimes when a node is close to a wall the ray seems to stop short
            render_vertexes[0].color = Color::Yellow;
            render_vertexes[1].color = Color::Yellow;
        }

    }

    return hit_wall;
}

bool Ray::CheckGridNodeShadowWalls(GridNode & cell)
{
    fec2 intersect_point = end;
    bool hit_wall = !cell.CanWalk();
    if (cell.CanWalk() == false)
    {
        //hit_point = cell.GetCellCentre();
        //Shadow::ShadowCast(start, edge(cell.GetCellCentre() - perp().norm() * cell.GetCellWidth(), cell.GetCellCentre() + perp().norm() * cell.GetCellWidth()));
        edge_hit = this;
        hit_point = end;
        return true;
    }
    for (auto &wall : cell.walls)
    {
        ////wall.render_vertexes[0].color = Color::Magenta;
        //// wall.render_vertexes[1].color = Color::Magenta;
        ////Does the wall intersect the ray?
        //if (hit_point == end)
        //{
        //    //wall.setrender(true);
        //}
        //if (intersect(wall, intersect_point))
        //{

        //    if (fec2(intersect_point - start).mag() < fec2(hit_point - start).mag())
        //    {
        //        hit_point = intersect_point;
        //        hit_wall |= true;
        //        node_hit = nullptr;
        //        edge_hit = &wall;
        //        edge_hit->setrender(true);
        //    }

        //}
        ////if (edge_hit == nullptr)
        ////{
        ////    wall.setrender(true);
        ////}

        Shadow::ShadowCast(start, wall);
    }

    return hit_wall;
}

bool Ray::CheckGridNodeCollideNodes(GridNode & cell)
{
    bool hit_node = false;
    fec2 near_point;

    for (auto &node : cell.node_v)
    {
        //node.collider.SetDrawColour(Color::Cyan);
        //Check that the node did not cast the ray by means of comparing it's position with the ray start
        if (start_node != nullptr && node.id == start_node->id)
        {

        }
        else if (overlap_circle_np(node.collider, near_point) && node.collider_type != TRIGGER_NODE)
        {
            node.collider.SetDrawColour(Color::Cyan);
            //if (node.position == end)
            //{
            //    //node_hit = &node;
            //    //hit_node |= true;
            //    //hit_point = near_point;
            //}

            if (fec2(near_point - start).mag() < fec2(hit_point - start).mag())
            {
                hit_point = near_point;
                hit_node |= true;

                node_hit = &node;
                //node_hit->collider.SetDrawColour(Color::White);
            }
        }
    }
    return hit_node;
}

//Check against the grid and check for hits
bool Ray::RayCastGrid()
{
    if (!GridNode::CheckOnGrid(start))
    {
        return false;
    }
    GridNode &start_cell = GridNode::GetCell(start);
    GridNode &end_cell = GridNode::GetCell(end);
    bool hit = false;
    int cell_delta_x = end_cell.x - start_cell.x;
    int cell_delta_y = end_cell.y - start_cell.y;
   // cout << "Ray start x: " << start_cell.x << ", y: " << start_cell.y << "; end x: " << end_cell.x << ", y: " << end_cell.y << endl;
    int x = start_cell.x;
    int y = start_cell.y;
   // cout << "Start Cell x: " << end_cell.x << ", y: " << end_cell.y << " End Cell x: " << end_cell.x << ", y: " << end_cell.y << endl;
    bool searching = true;

    do
    {
        GridNode & curr_cell = GridNode::GetCell(x, y);
        fec2 curr_cell_centre = curr_cell.GetCellCentre();
        if (fec2(curr_cell_centre - nearest_point(curr_cell_centre)).mag() < (GridNode::GetCellWidth() * 1.6f /2.f))
        {
            bool wall_hit;
            render_vertexes.append(Vertex(nearest_point(curr_cell_centre), Color(0, 255, 100, 80)));
            render_vertexes.append(Vertex(curr_cell_centre, Color(0, 255, 100, 80)));
            curr_cell.IncrementActivity();
            wall_hit = CheckGridNodeCollideWalls(curr_cell);
            hit |= CheckGridNodeCollideNodes(curr_cell);
            hit |= wall_hit;
            //hit |= CheckGridNodeCollideWalls(curr_cell); //BRUTE FORCE THE DAMN THING
            //cout << "Raycasting cell x: " << x << ", y: " << y << " hit status: " << wall_hit << endl;
        }
        if (x == end_cell.x)
        {
            if (y == end_cell.y)
            {
                //We have searched the entire grid
                searching = false;
            }
            else
            {
                //Reset x and increment y
                x = start_cell.x;
                (cell_delta_y > 0 ? y++ : y--);
            }
        }
        else
        {
            //Increment x
            (cell_delta_x > 0 ? x++ : x--);
        }

    } while (searching);

   /* render_vertexes.append(Vertex(start, Color(255,255,255,80)));
    render_vertexes.append(Vertex(GridNode::GetCell(hit_point).GetCellCentre(), Color(255, 255, 255, 80)));*/

    return hit;
}

bool Ray::ShadowCastGrid()
{
    if (!GridNode::CheckOnGrid(start))
    {
        return false;
    }
    GridNode &start_cell = GridNode::GetCell(start);
    GridNode &end_cell = GridNode::GetCell(end);
    bool hit = false;
    unsigned int hit_count = 0;
    int cell_delta_x = end_cell.x - start_cell.x;
    int cell_delta_y = end_cell.y - start_cell.y;
    int x = start_cell.x;
    int y = start_cell.y;
    // cout << "Start Cell x: " << start_cell.x << ", y: " << start_cell.y << " End Cell x: " << end_cell.x << ", y: " << end_cell.y << endl;
    bool searching = true;

    GridNode & prev_cell = start_cell;
    do
    {
        GridNode & curr_cell = GridNode::GetCell(x, y);
        fec2 curr_cell_centre = curr_cell.GetCellCentre();
        if (fec2(curr_cell_centre - nearest_point(curr_cell_centre)).mag() < GridNode::GetCellWidth())
        {
            curr_cell.shadowed = false;
            //if (hit_point != end)
            //{
            //    //render_vertexes.append(Vertex(nearest_point(curr_cell_centre), Color(255, 255, 100, 150)));
            //    //render_vertexes.append(Vertex(curr_cell_centre, Color(255, 255, 100, 150)));
            //}

            if (hit)
            {
                for (auto &wall : curr_cell.walls)
                {
                    Shadow::ShadowCast(start, wall);
                    searching = false;
                }
            }
            else
            {
                for (auto &wall : curr_cell.walls)
                {
                    wall.setrender(debug_render);
                }
            }

            hit |= CheckGridNodeShadowWalls(curr_cell);

        }
        if (x == end_cell.x)
        {
            if (y == end_cell.y)
            {
                //We have searched the entire grid
                searching = false;
            }
            else
            {
                //Reset x and increment y
                x = start_cell.x;
                (cell_delta_y > 0 ? y++ : y--);
            }
        }
        else
        {
            //Increment x
            (cell_delta_x > 0 ? x++ : x--);
        }

        //prev_cell = curr_cell;
    } while (searching);
    return hit;
}

bool Ray::HitEnd()
{
    if (hit_point != end)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void Ray::draw_me(RenderWindow & window)
{
    render_vertexes[1].position = hit_point;
    if (hit_point == end)
    {
        render_vertexes[0].color = Color::Green;
        render_vertexes[1].color = Color::Green;
    }
    else
    {
        render_vertexes[0].color = Color::Red;
        render_vertexes[1].color = Color::Red;
    }
    if (node_hit != nullptr)
    {
        node_hit->collider.SetDrawColour(Color::Green);
    }
    render_vertexes.append(Vertex(hit_point + fec2(10, 0), Color::Yellow));
    render_vertexes.append(Vertex(hit_point - fec2(10, 0), Color::Yellow));
    render_vertexes.append(Vertex(hit_point + fec2(0, 10), Color::Yellow));
    render_vertexes.append(Vertex(hit_point - fec2(0, 10), Color::Yellow));
    if (debug_render)
    {
        GFXMgr::PushRayVertexes(render_vertexes);
        //window.draw(render_vertexes);
       /* float cell_width = GridNode::GetCellWidth();
        RectangleShape end_sq(fec2(cell_width, cell_width));
        end_sq.setPosition(GridNode::GetCell(hit_point).cell_pos);
        end_sq.setFillColor(Color::Green);

        window.draw(end_sq);*/
        //cout << "Drawing!" << endl;
    }
    //GridNode& endc = GridNode::GetCell(hit_point);
    //if (endc.CanWalk() == false)
    //{
    //    cout << "RAY HAS DONE A GOOF!" << endl;
    //}
    //else
    //{
    //    cout <<  " " << endl;

    //}
    render_vertexes.clear();
}

fec2 Ray::GetTerminationPoint()
{
    return hit_point;
}

Node * Ray::GetHitNode()
{
    return node_hit;
}
