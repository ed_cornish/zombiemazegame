#include "Behaviours.h"
#include "Type.h"
#include "Thor/Math.hpp"
#include "SoundFX.h"


void fragEnterB(TypeData & data, float elapsed_s)
{
    static thor::Distribution<int> hp_dist = thor::Distributions::uniform(10, 25);

    data.hp = hp_dist();
}

State * fragFrameB(TypeData & data, float elapsed_s)
{
    for (auto c : data.contact_list)
    {
        if (c &&
            (c->IsTypeID(PLAYER_TYPE) || c->IsTypeID(GARBAGECOLLECTOR_TYPE)))
        {
            c->Heal(data.hp);
            data.hp = 0;
            SoundFX::play(PICKUP_SND);
            break;
        }
    }

    return nullptr;
}

node_type_id GetAllyToSpawn(TypeData & data)
{
    node_type_id ret_id = NO_TYPE_ID;
    if (data.detected_ptr)
    {
        if (data.detected_ptr->id == PLAYER_TYPE)
        {
            if (fec2(data.detected_ptr->world_pos - data.world_pos).mag() > data.detect_range)
            {
                ret_id = INTERRUPTOR_TYPE;
            }
            else
            {
                ret_id = DISINFECTOR_TYPE;
            }
        }
        else if (data.detected_ptr->id == FRAGMENT_TYPE)
        {
            ret_id = GARBAGECOLLECTOR_TYPE;
        }
    }
    return ret_id;
}
