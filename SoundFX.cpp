#include "SoundFX.h"
#include "Game.h"
#include "GFXMgr.h"

SoundBuffer SoundFX::sfx_buffers[MAX_SND_ID];
std::deque<Sound> SoundFX::sfx;

SoundFX::SoundFX()
{
    if(sfx_buffers[PLAYER_HIT_SND].loadFromFile("Windows Ding.wav") &&
       sfx_buffers[PLAYER_SHOOT_SND].loadFromFile("Windows Hardware Fail.wav") &&
       sfx_buffers[WARP_SND].loadFromFile("Windows Battery Low.wav") &&
       sfx_buffers[ENEMY_SHOOT_SND].loadFromFile("Windows Default.wav") &&
       sfx_buffers[NEW_LEVEL_SND].loadFromFile("tada.wav") &&
       sfx_buffers[ENEMY_DIE_SND].loadFromFile("Windows Hardware Remove.wav") &&
       sfx_buffers[PICKUP_SND].loadFromFile("Windows Notify.wav"))
    {
        GAME_DEBUG_LOG("Loaded all sounds successfully!");
    }
    else
    {
        GAME_DEBUG_LOG("Failed to load all sound files!");
    }
//    sfx[PLAYER_SHOOT_SND] =
//    sfx[PLAYER_DIE_SND] =
//    sfx[ENEMY_HIT_SND] =
//    sfx[ENEMY_SHOOT_SND] =
//    sfx[ENEMY_DIE_SND] =
//    sfx[PICKUP_SND] =
//    sfx[ALERT_SND] =
//    sfx[WARP_SND] =
}

SoundFX::~SoundFX()
{
    //dtor
}

void SoundFX::play(int sound_id)
{
    if(sound_id >= MAX_SND_ID || sound_id < 0)
    {
        GAME_DEBUG_LOG("ERROR: invalid sound id passed to SoundFX: " << sound_id);
        return;
    }
    sfx.push_back(Sound(sfx_buffers[sound_id]));
    sfx.back().play();
    if(sfx.size() > 0 && sfx.front().getStatus() == sf::Sound::Stopped)
    {
        sfx.pop_front();
    }
    GAME_DEBUG_LOG("Playing sound id: " << sound_id);
}

void SoundFX::purge(float elapsed_s)
{
    static float timer_s = 0.f;
    timer_s += elapsed_s;
    if(timer_s > 1.f && sfx.size() > 0)
    {
        GAME_DEBUG_LOG(sfx.size() << " sounds currently playing before");

        timer_s = 0.f;
        if(sfx.front().getStatus() == sf::Sound::Stopped)
        {
            sfx.pop_front();
        }
        //sfx.erase(remove_if(sfx.begin(), sfx.end(), [](Sound & snd) { auto status = snd.getStatus(); GAME_DEBUG_LOG(status); return (status == sf::Sound::Stopped); }), sfx.end());
        GAME_DEBUG_LOG(sfx.size() << " sounds currently playing after");
    }

}
