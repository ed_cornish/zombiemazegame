# README #

This is a top down 2D game inspired by roguelikes and dungeon crawlers, with a significant nod towards twin stick shooters.

### Current features ###
- 2D rendering using SFML
- Collision detection using grid and RYO collision shapes (edges, circles)
- Raycasting against the grid.
- Grid optimisation to minimise cycles
- Enemies with state controlled behaviour using the Strategy pattern, using A* algorithm in conjunction with steering behaviour to navigate.
- Trigger nodes which can affect the player (score/health etcetera)
- FOV\Shadowing computed using an outward spiral on the level grid. A feature was tested where the walls were checked against the existing shadow to avoid overdraw but this didn't help performance.
- Player can 'shoot' the enemies using the mouse and a raycast which will reduce their health and remove them from the game when their health reaches 0
- Bullets are now implemented
- Basic HUD showing health\score attached to the player - needs re-implementing
- Generic state based behaviours are implemented for enemy types. There are enemies that move toward the player and fire from close range, and enemies that will maintain their distance and shoot from afar. A third type sits in one place and spawns in new enemies if it detects the player.
- Enemy behaviours include wandering, scanning for the player, hunting for the player if LOS is lost, attacking, and entering a cool down after spawning.
- Effect system in place to allow for damage over time, resistances\weaknesses, etcetera
- Dead enemies should drop 'code fragments' that the player can pickup to boost their health
- Health is tied directly to movement speed and fire rate.
- Player can teleport a short distance within line of sight. This is intended to be the primary defensive ability.
- Particle effects using Thor lib, controlled by Graphics manager class. This adds visual 'pop', and allows things like explosions and teleport effects.

### Features to be implemented (no particular priority implied!) ###
- Tidy up class interfaces
- Animations using Thor lib - TBC
- Sounds
- Story\flavour text via on screen text windows
- Level transitions
- Menu system
- In game GUI\HUD
- Different flavours of monster AI - now largely implemented thanks to runtime defined behaviours

### Canned features
- 2.5D walls in OGL (need to get shadow mapping working using programmable pipeline)
- Multithreading to optimize for multi-core systems
- Spell casting via keyboard command interface - will likely leave this as not conducive to smooth gameplay
- More efficient shadowing algorithm (see point about OGL shadow mapping below)

### Dependencies ###
- SFML 2+
- Thor library 2.0
- Micropather library

### Contribution guidelines ###

- This is a solo project at the moment, please get in touch if you wish to contribute!

### Who do I talk to? ###

* ed.j.cornish@gmail.com - Ed Cornish (that's me!)