#include "EntityData.h"
#include "Type.h"
#include "Behaviours.h"

const float max_detect_range_table[MAX_TYPE_ID] = {
    800.f,//PLAYER_TYPE,
    500.f, //DISINFECTOR_TYPE,
    700.f,//INTERRUPTOR_TYPE,
    0.f,  //    PORT_TYPE,
    900.f,  //    GARBAGECOLLECTOR_TYPE,
    900.f,  //    WATCHDOG_TYPE,
    0.f,  //    PROCESS_TYPE,
    0.f,  //    fIREWALL_TYPE,
    0.f,  //    LOGfILE_TYPE,
    0.f,  //    EXEfILE_TYPE,
    0.f,  //    SOURCEfILE_TYPE,
    0.f,  //    BUffER_TYPE,
    0.f,  //    VIRUS_TYPE,
    0.f,  //    WORM_TYPE,
    0.f,  //    TROJAN_TYPE,
    0.f,  //    LOGICBOMB_TYPE,
    425.f,  //    BULLET_TYPE,
    0.f,  //    NO_TYPE_ID,
};

const char* type_string_table[MAX_TYPE_ID] = {
    "Player",//    PLAYER_TYPE,
    "Disinfector",  //    DISINFECTOR_TYPE,
    "Interruptor",  //    INTERRUPTOR_TYPE,
    "Port",  //    PORT_TYPE,
    "Garbage Collector",  //    GARBAGECOLLECTOR_TYPE,
    "Watchdog",  //    WATCHDOG_TYPE,
    "Process", //    PROCESS_TYPE,
    "Firewall", //    FIREWALL_TYPE,
    ".log File", //    LOGFILE_TYPE,
    ".exe file ", //    EXEFILE_TYPE,
    ".src file", //    SOURCEFILE_TYPE,
    "Buffer", //    BUFFER_TYPE,
    "Virus", //    VIRUS_TYPE,
    "Worm", //    WORM_TYPE,
    "Trojan", //    TROJAN_TYPE,
    "Logic Bomb", //    LOGICBOMB_TYPE,
    "Bullet", //    BULLET_TYPE,
    "NULL TYPE", //    NO_TYPE_ID,
};

const char render_char_table[MAX_TYPE_ID] = {
    '@',//    PLAYER_TYPE,
    'D',  //    DISINFECTOR_TYPE,
    'I',  //    INTERRUPTOR_TYPE,
    'P',  //    PORT_TYPE,
    'G',  //    GARBAGECOLLECTOR_TYPE,
    'W',  //    WATCHDOG_TYPE,
    '>',  //    FRAGMENT_TYPE,
    '|',  //    FIREWALL_TYPE,
    'L',  //    LOGFILE_TYPE,
    'x',  //    EXEFILE_TYPE,
    '#',  //    SOURCEFILE_TYPE,
    'B',  //    BUFFER_TYPE,
    'V',  //    VIRUS_TYPE,
    'W',  //    WORM_TYPE,
    'T',  //    TROJAN_TYPE,
    'o',  //    LOGICBOMB_TYPE,
    '*',  //    BULLET_TYPE,
    '!',  //    NO_TYPE_ID,
};

const int hit_points_table[MAX_TYPE_ID] = {
    100, //    PLAYER_TYPE,
    40,   //    DISINFECTOR_TYPE,
    50,   //    INTERRUPTOR_TYPE,
    1000000,   //    PORT_TYPE,
    60,   //    GARBAGECOLLECTOR_TYPE,
    100,   //    WATCHDOG_TYPE,
    15,   //    FRAGMENT_TYPE,
    100,   //    FIREWALL_TYPE,
    100,   //    LOGFILE_TYPE,
    100,   //    EXEFILE_TYPE,
    100,   //    SOURCEFILE_TYPE,
    100,   //    BUFFER_TYPE,
    100,   //    VIRUS_TYPE,
    100,   //    WORM_TYPE,
    100,   //    TROJAN_TYPE,
    100,   //    LOGICBOMB_TYPE,
    20,   //    BULLET_TYPE,
    100,   //    NO_TYPE_ID,
};

const float attack_interval_table[MAX_TYPE_ID] = {
    0.3f, //    PLAYER_TYPE,
    0.4f,   //    DISINFECTOR_TYPE,
    0.4f,   //    INTERRUPTOR_TYPE,
    0.75f,   //    PORT_TYPE,
    0.75f,   //    GARBAGECOLLECTOR_TYPE,
    0.75f,   //    WATCHDOG_TYPE,
    0.75f,   //    FRAGMENT_TYPE,
    0.75f,   //    FIREWALL_TYPE,
    0.75f,   //    LOGFILE_TYPE,
    0.75f,   //    EXEFILE_TYPE,
    0.75f,   //    SOURCEFILE_TYPE,
    0.75f,   //    BUFFER_TYPE,
    0.75f,   //    VIRUS_TYPE,
    0.75f,   //    WORM_TYPE,
    0.75f,   //    TROJAN_TYPE,
    0.75f,   //    LOGICBOMB_TYPE,
    0.75f,   //    BULLET_TYPE,
    0.75f,   //    NO_TYPE_ID,
};

const float collider_radius_table[MAX_TYPE_ID] = {
    10.f, //    PLAYER_TYPE,
    10.f,   //    DISINFECTOR_TYPE,
    10.f,   //    INTERRUPTOR_TYPE,
    15.f,   //    PORT_TYPE,
    10.f,   //    GARBAGECOLLECTOR_TYPE,
    10.f,   //    WATCHDOG_TYPE,
    7.f,   //    FRAGMENT_TYPE,
    15.f,   //    FIREWALL_TYPE,
    10.f,   //    LOGFILE_TYPE,
    10.f,   //    EXEFILE_TYPE,
    10.f,   //    SOURCEFILE_TYPE,
    10.f,   //    BUFFER_TYPE,
    10.f,   //    VIRUS_TYPE,
    10.f,   //    WORM_TYPE,
    10.f,   //    TROJAN_TYPE,
    10.f,   //    LOGICBOMB_TYPE,
    6.f,   //    BULLET_TYPE,
    10.f,   //    NO_TYPE_ID,
};

bool InitTypeData(TypeData & data)
{
    data.detect_range = max_detect_range_table[data.id];
    data.type_string = String(type_string_table[data.id]);
    data.render_text = Text(String(render_char_table[data.id]), *Type::GetFontPtr());
    data.hp = hit_points_table[data.id];
    if(InitMoverData(data))
    {
        return true;
    }
    return false;
}


float GetBaseDetectRange(node_type_id id)
{
    return max_detect_range_table[id];
}

int GetMaxHitPoints(node_type_id id)
{
    return hit_points_table[id];
}

float GetBaseAttackRate(node_type_id id)
{
    return attack_interval_table[id];
}

float GetScanDegsPerSec(node_type_id id)
{
    if (id == WATCHDOG_TYPE)
    {
        return 360.f;
    }
    return 720.f;
}

float GetColliderRadius(TypeData & data)
{
    float ret_rad = collider_radius_table[data.id];

    if (data.id == BULLET_TYPE || data.id == FRAGMENT_TYPE)
    {
        ret_rad *= GetCurrHPFraction(data);
    }

    return ret_rad;
}

Color GetColor(node_type_id id, node_type_id * parent_id_p)
{
    Color ret_colour;
    if(parent_id_p)
    {
        ret_colour = GetColor(*parent_id_p);
    }
    else
    {
        switch(id)
        {
            case PLAYER_TYPE:
                ret_colour = Color::Red;
                break;
            case BULLET_TYPE:
                ret_colour = Color::Cyan;
                break;
            case PORT_TYPE:
                ret_colour = Color::Magenta;
                break;
            case DISINFECTOR_TYPE:
                ret_colour = Color(255,128,0,255);
                break;
            case INTERRUPTOR_TYPE:
                ret_colour = Color::Cyan;
                break;
            case WATCHDOG_TYPE:
                ret_colour = Color::Blue;
                break;
            case FRAGMENT_TYPE:
                ret_colour = Color::Green;
                break;
            case GARBAGECOLLECTOR_TYPE:
                ret_colour = Color::Yellow;
                break;
            case FIREWALL_TYPE:
                ret_colour = Color(255, 190, 190, 255);
                break;
            default:
                ret_colour = Color::White;
                break;
        }
    }

    return ret_colour;
}

